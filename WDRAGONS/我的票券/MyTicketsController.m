//
//  MyTicketsController.m
//  brothers
//
//  Created by 陳威宇 on 2020/1/13.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import "MyTicketsController.h"
#import <QuartzCore/QuartzCore.h>

@interface MyTicketsController ()

@end

@implementation MyTicketsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BackToObtainedListView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backToObtainedListView) name:@"BackToObtainedListView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BackToNotObtainedListView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backToNotObtainedListView) name:@"BackToNotObtainedListView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SendTicket" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendTicket:) name:@"SendTicket" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RetrieveTicket" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retrieveTicket:) name:@"RetrieveTicket" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GoBackTicket" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBackTicket:) name:@"GoBackTicket" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AcceptTicket" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptTicket:) name:@"AcceptTicket" object:nil];
    self.view.backgroundColor = [UIColor colorWithRed:(248/255.0) green:(248/255.0) blue:(248/255.0) alpha:1.0f];
    [self appendNavigationBarItem];
    [self initView];
    obtained_ticket_Entries = [[NSMutableArray alloc] init];
    notObtainYet_ticket_Entries = [[NSMutableArray alloc] init];
    [AppDelegate sharedAppDelegate].page_id = 40;
    obtained_index = 1;
    notObtain_index = 1;
    currentPage_obtain = 0;
    previousPage_obtain = 0;
    currentPage_notObtain = 0;
    previousPage_notObtain = 0;
}

//畫面出現前
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    ob_nibsRegistered = NO;
    notob_nibsRegistered = NO;
    [self initObtainView];
    [self initNotObtainView];
    [self initObtainDetailView];
    [self initNotObtainDetailView];
    obtained_index = 1;
    notObtain_index = 1;
    
    [self getTicketsData:1];
    [obtainView removeFromSuperview];
    [notObtainView removeFromSuperview];
    [obtainDeatilView removeFromSuperview];
    [notObtainDetailView removeFromSuperview];
    [self.view addSubview:obtainView];
    menu_select = 2;
    [self pressMenu_1];
}

//離開畫面前
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)appendNavigationBarItem {
    CGFloat topbarHeight = ([UIApplication sharedApplication].statusBarFrame.size.height +
    (self.navigationController.navigationBar.frame.size.height ?: 0.0));
    [navi_view removeFromSuperview];
    navi_view = nil;
    pos_x = 0;
    pos_y = topbarHeight + STATUS_BAR_HEIGHT - NAVI_BAR_HEIGHT;
    width = DEVICE_WIDTH;
    height = NAVI_BAR_HEIGHT - 5;
    navi_view = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    navi_view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:navi_view];
    height = NAVI_BAR_HEIGHT;
    width = (height * (378 / 48)) * 0.8;
    pos_y = 0;
    pos_x = (DEVICE_WIDTH - width) / 2;
    navi_logo = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    navi_logo.contentMode = UIViewContentModeScaleAspectFit;
    
    navi_logo.image = [UIImage imageNamed:@"header_LOGO"];
    [navi_view addSubview:navi_logo];
    pos_x = 0;
    pos_y = navi_view.frame.origin.y + navi_view.frame.size.height;
    width = navi_view.frame.size.width;
    height = 10;
    navi_line = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    navi_line.backgroundColor = [UIColor colorWithRed:(70/255.0) green:(70/255.0) blue:(70/255.0) alpha:1.0f];
  
    navi_line.layer.shadowColor = [[UIColor grayColor] CGColor];
    navi_line.layer.shadowOffset = CGSizeMake(2.0f, 2.0f); // [水平偏移, 垂直偏移]
    navi_line.layer.shadowOpacity = 0.6f; // 0.0 ~ 1.0 的值
    navi_line.layer.shadowRadius = 3.0f; // 陰影發散的程度
    //[self.view addSubview:navi_line];
}

- (void)getTicketsData:(NSInteger)tag {
    [self showLoadingView:@"資料處理中，請稍候。"];
    [self getObtainedTicketFromInternet:tag];
}

- (void)initView {
    pos_x = 0;
    pos_y = navi_view.frame.origin.y + navi_view.frame.size.height + 10;
    width = DEVICE_WIDTH;
    height = 50;
    menu_view = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    menu_view.backgroundColor = [UIColor whiteColor];
    pos_x = 0;
    pos_y = menu_view.frame.size.height - 1;
    width = DEVICE_WIDTH;
    height = 1;
    menu_line_1 = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    menu_line_1.backgroundColor = [UIColor colorWithRed:(230/255.0) green:(230/255.0) blue:(230/255.0) alpha:1.0f];
    [menu_view addSubview:menu_line_1];
    [self.view addSubview:menu_view];
    //
    float menu_label_width = (DEVICE_WIDTH) / 2;
    pos_x = 0;
    pos_y = 0;
    width = menu_label_width;
    height = menu_view.frame.size.height - 1;
    menu_label_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    menu_label_1.backgroundColor = [UIColor whiteColor];
    menu_label_1.font = [UIFont systemFontOfSize:18];
    menu_label_1.textAlignment = NSTextAlignmentCenter;
    menu_label_1.textColor = [UIColor colorWithRed:(0/255.0) green:(0/255.0) blue:(0/255.0) alpha:1.0f];
    menu_label_1.text = @"已取電子票券";
    menu_label_1.numberOfLines = 0;
    UITapGestureRecognizer *singleTap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressMenu_1)];
    [menu_label_1 addGestureRecognizer:singleTap1];
    [menu_label_1 setMultipleTouchEnabled:YES];
    [menu_label_1 setUserInteractionEnabled:YES];
    [menu_view addSubview:menu_label_1];
    pos_x = menu_label_1.frame.origin.x + menu_label_1.frame.size.width;
    pos_y = 0;
    height = menu_view.frame.size.height - 1;
    width = menu_label_width;
    menu_label_2 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    menu_label_2.backgroundColor = [UIColor whiteColor];
    menu_label_2.font = [UIFont systemFontOfSize:18];
    menu_label_2.textAlignment = NSTextAlignmentCenter;
    menu_label_2.textColor = [UIColor colorWithRed:(150/255.0) green:(150/255.0) blue:(150/255.0) alpha:1.0f];
    menu_label_2.text = @"未取電子票券";
    menu_label_2.numberOfLines = 0;
    UITapGestureRecognizer *singleTap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressMenu_2)];
    [menu_label_2 addGestureRecognizer:singleTap2];
    [menu_label_2 setMultipleTouchEnabled:YES];
    [menu_label_2 setUserInteractionEnabled:YES];
    [menu_view addSubview:menu_label_2];
    pos_x = 0;
    pos_y = menu_view.frame.size.height - 5;
    width = menu_label_width;
    height = 5;
    menu_underline = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    menu_underline.backgroundColor = [UIColor colorWithRed:(33/255.0) green:(62/255.0) blue:(94/255.0) alpha:0.3f];
    [menu_view addSubview:menu_underline];
}

- (void)initObtainView {
    [obtainView removeFromSuperview];
    obtainView = nil;
    pos_x = 20;
    pos_y = menu_view.frame.origin.y + menu_view.frame.size.height + 10;
    width = DEVICE_WIDTH - 40;
    height = DEVICE_HEIGHT - pos_y - TAB_BAR_HEIGHT - 1;
    obtainView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    obtainView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:obtainView];
    [_obtainTb removeFromSuperview];
    _obtainTb = nil;
    pos_x = 0;
    pos_y = 0;
    width = obtainView.frame.size.width;
    height = obtainView.frame.size.height;
    _obtainTb = [[UITableView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _obtainTb.tag = 1;
    _obtainTb.delegate = self;
    _obtainTb.dataSource = self;
    _obtainTb.separatorStyle = UITableViewCellSeparatorStyleNone;
    _obtainTb.backgroundColor = [UIColor clearColor];
    [_obtainTb setShowsHorizontalScrollIndicator:NO];
    [_obtainTb setShowsVerticalScrollIndicator:NO];
    menu_select = 1;
    [obtainView addSubview:_obtainTb];
}

- (void)initObtainDetailView {
    [obtainDeatilView removeFromSuperview];
    obtainDeatilView = nil;
    pos_x = 0;
    pos_y = menu_view.frame.origin.y + menu_view.frame.size.height + 30;
    width = DEVICE_WIDTH;
    height = DEVICE_HEIGHT - pos_y - TAB_BAR_HEIGHT - 20;
    obtainDeatilView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    obtainDeatilView.backgroundColor = [UIColor clearColor];
    
    //[self.view addSubview:obtainDeatilView];
    pos_x = 0;
    pos_y = 0;
    width = obtainDeatilView.frame.size.width;
    height = obtainDeatilView.frame.size.height;
    [obtainScrollView removeFromSuperview];
    obtainScrollView = nil;
    obtainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    
    if (@available(iOS 11.0, *)) {
        obtainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    obtainScrollView.backgroundColor = [UIColor clearColor];
    [obtainScrollView setPagingEnabled:YES];
    [obtainScrollView setShowsHorizontalScrollIndicator:NO];
    [obtainScrollView setShowsVerticalScrollIndicator:NO];
    [obtainScrollView setScrollsToTop:YES];
    [obtainScrollView setDelegate:self];
    obtainScrollView.tag = 1;
    [obtainDeatilView addSubview:obtainScrollView];
}

- (void)initObtainTicketView {
    [obtainView removeFromSuperview];
    [notObtainView removeFromSuperview];
    [obtainDeatilView removeFromSuperview];
    [notObtainDetailView removeFromSuperview];
    [self.view addSubview:obtainDeatilView];
    //清除舊的view
    currentPage_obtain = 0;
    previousPage_obtain = 0;
    [leftTicketButton_obtain removeFromSuperview];
    leftTicketButton_obtain = nil;
    [rightTicketButton_obtain removeFromSuperview];
    rightTicketButton_obtain = nil;
    //[pageControl_notObtain removeFromSuperview];
    //pageControl_notObtain = nil;
    [pageControl_obtain removeFromSuperview];
    pageControl_obtain = nil;
    pos_x = 0;
    pos_y = pos_y = menu_view.frame.origin.y + menu_view.frame.size.height + 8;
    width = DEVICE_WIDTH;
    height = 15;
    pageControl_obtain = [[UIPageControl alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [pageControl_obtain setPageIndicatorTintColor:[UIColor blackColor]];
    [pageControl_obtain setCurrentPageIndicatorTintColor:[UIColor redColor]];
    [pageControl_obtain setNumberOfPages:[obtained_ticket_Entries count]];
    [pageControl_obtain setCurrentPage:0];
    [pageControl_obtain setNeedsDisplay];
    [self.view addSubview: pageControl_obtain];
    pos_x = 0;
    width = 24;
    height = width;
    pos_y = (obtainDeatilView.frame.size.height - height) / 3;
    leftTicketButton_obtain = [[UIButton alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [leftTicketButton_obtain setBackgroundImage:[UIImage imageNamed:@"arrow_pre"] forState:UIControlStateNormal];
    [leftTicketButton_obtain setTag:1];
    [leftTicketButton_obtain setHidden:YES];
    [obtainDeatilView  addSubview:leftTicketButton_obtain];
    pos_x = obtainDeatilView.frame.size.width - width;
    pos_y = leftTicketButton_obtain.frame.origin.y;
    width = 24;
    height = width;
    rightTicketButton_obtain = [[UIButton alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [rightTicketButton_obtain setBackgroundImage:[UIImage imageNamed:@"arrow_next"] forState:UIControlStateNormal];
    [rightTicketButton_obtain setTag:1];
    [rightTicketButton_obtain setHidden:YES];
    if([obtained_ticket_Entries count] > 1)
        [rightTicketButton_obtain setHidden:NO];
    [obtainDeatilView addSubview:rightTicketButton_obtain];
    for (UIView *view in obtainScrollView.subviews) {
        if(view.tag >= 1000 && view.tag < 2000)
            [view removeFromSuperview];
    }
    CGFloat sWidth, sHeight;
    sWidth  = obtainScrollView.frame.size.width;
    sHeight = obtainScrollView.frame.size.height;
    [obtainScrollView setContentSize:CGSizeMake(sWidth * [obtained_ticket_Entries count], sHeight)];
    
    if([obtained_ticket_Entries count] == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
        obtained_index = 1;
        [AppDelegate sharedAppDelegate].page_id = 40;
        [_obtainTb reloadData];
        [self.view addSubview:obtainView];
        [reloadTimer invalidate];
        reloadTimer = nil;
        return;
    }
    if(obtained_viewArray)
        [obtained_viewArray removeAllObjects];
    else
        obtained_viewArray = [[NSMutableArray alloc] init];
    //讀取票券內容
    for (NSInteger idx=0;idx<[obtained_ticket_Entries count];idx++) {
        CGRect frame = CGRectMake(sWidth*idx, 0, sWidth, sHeight);
        TicketView *view  = [[TicketView alloc] initWithFrame:frame];
        view.tag = idx + 1000;
        [view setBackgroundColor:[UIColor clearColor]];
        TicketObject *obj = [obtained_ticket_Entries objectAtIndex:idx];
        view.orderTime = obj.orderTime;
        view.productName = obj.performanceName;
        view.showTime = obj.startTime;
        view.place = obj.placeName;
        view.performanceNo = obj.performanceNo;
        view.ticketId = obj.ticketId;
        view.price = obj.orderPrice;
        view.priceType = obj.priceType;
        view.taxNumber = obj.tax_number;
        view.taxInfo = obj.tax_info;
        view.orderNumber = obj.orderNumber;
        view.isObtained = @"Y";
        view.ticketType = obj.ticketType;
        view.transferFlag = obj.transferFlag;
        view.orderSeat = obj.orderSeat;
        view.senderName = obj.senderName;
        view.senderMessage = obj.senderMessage;
        view.remark = obj.notice_msg;
        view.serialNo = obj.RockNo;
        view.getMethod = obj.getMethod;
        view.payMethod = obj.payMethod;
        view.virtualNo = obj.virtualNo;
        view.verifyNo = obj.verifyNo;
        view.admissionStatus = obj.admissionStatus;
        view.buyBySelf = obj.buyBySelf;
        view.receiveName = obj.receiver_name;
        view.isGoods = obj.isGoods;
        view.goodsItem = obj.goodsItem;
        view.notice_msg = obj.notice_msg;
        view.notice_msg2 = obj.notice_msg2;
        view.notice_msg3 = obj.notice_msg3;
        view.total_amount = obj.total_amount;
        view.ticket_format = obj.ticket_format;
        view.is_seasonticket = obj.is_seasonticket;
        view.updateTicketView = @"Y";
        if(view.frame.size.height > sHeight)
            sHeight = view.frame.size.height;
        [obtainScrollView setContentSize:CGSizeMake(sWidth * [obtained_ticket_Entries count], sHeight)];
        [obtained_viewArray addObject:view];
        [obtainScrollView addSubview:view];
    }
}

- (void)initNotObtainView {
    [notObtainView removeFromSuperview];
    notObtainView = nil;
    pos_x = 20;
    pos_y = menu_view.frame.origin.y + menu_view.frame.size.height + 10;
    width = DEVICE_WIDTH  - 40;
    height = DEVICE_HEIGHT - pos_y - TAB_BAR_HEIGHT - 1;
    notObtainView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    notObtainView.backgroundColor = [UIColor clearColor];
    [_notObtainTb removeFromSuperview];
    _notObtainTb = nil;
    pos_x = 0;
    pos_y = 0;
    width = notObtainView.frame.size.width;
    height = notObtainView.frame.size.height;
    _notObtainTb = [[UITableView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    _notObtainTb.tag = 2;
    _notObtainTb.delegate = self;
    _notObtainTb.dataSource = self;
    _notObtainTb.separatorStyle = UITableViewCellSeparatorStyleNone;
    _notObtainTb.backgroundColor = [UIColor clearColor];
    [_notObtainTb setShowsHorizontalScrollIndicator:NO];
    [_notObtainTb setShowsVerticalScrollIndicator:NO];
    [notObtainView addSubview:_notObtainTb];
}

- (void)initNotObtainDetailView {
    [notObtainDetailView removeFromSuperview];
    notObtainDetailView = nil;
    pos_x = 0;
    pos_y = menu_view.frame.origin.y + menu_view.frame.size.height + 30;
    width = DEVICE_WIDTH;
    height = DEVICE_HEIGHT - pos_y - TAB_BAR_HEIGHT - 20;
    notObtainDetailView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    notObtainDetailView.backgroundColor = [UIColor clearColor];
    //[self.view addSubview:obtainDeatilView];
    pos_x = 0;
    pos_y = 0;
    width = notObtainDetailView.frame.size.width;
    height = notObtainDetailView.frame.size.height;
    [notObtainScrollView removeFromSuperview];
    notObtainScrollView = nil;
    notObtainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    
    if (@available(iOS 11.0, *)) {
        notObtainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    notObtainScrollView.backgroundColor = [UIColor clearColor];
    [notObtainScrollView setPagingEnabled:YES];
    [notObtainScrollView setShowsHorizontalScrollIndicator:NO];
    [notObtainScrollView setShowsVerticalScrollIndicator:NO];
    [notObtainScrollView setScrollsToTop:NO];
    [notObtainScrollView setDelegate:self];
    notObtainScrollView.tag = 2;
    [notObtainDetailView addSubview:notObtainScrollView];
}

- (void)initNotObtainTicketView {
    [obtainView removeFromSuperview];
    [notObtainView removeFromSuperview];
    [obtainDeatilView removeFromSuperview];
    [notObtainDetailView removeFromSuperview];
    [self.view addSubview:notObtainDetailView];
    currentPage_notObtain = 0;
    previousPage_notObtain = 0;
    [leftTicketButton_notObtain removeFromSuperview];
    leftTicketButton_notObtain = nil;
    [rightTicketButton_notObtain removeFromSuperview];
    rightTicketButton_notObtain = nil;
    [pageControl_notObtain removeFromSuperview];
    pageControl_notObtain = nil;

    [pageControl_obtain removeFromSuperview];
    // pageControl_obtain = nil;
    pos_x = 0;
    pos_y = pos_y = menu_view.frame.origin.y + menu_view.frame.size.height + 8;
    width = DEVICE_WIDTH;
    height = 15;
    pageControl_notObtain = [[UIPageControl alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [pageControl_notObtain setPageIndicatorTintColor:[UIColor blackColor]];
    [pageControl_notObtain setCurrentPageIndicatorTintColor:[UIColor redColor]];
    [pageControl_notObtain setNumberOfPages:[notObtainYet_ticket_Entries count]];
    [pageControl_notObtain setCurrentPage:0];
    [pageControl_notObtain setNeedsDisplay];
    [self.view addSubview: pageControl_notObtain];
    //清除舊的view
    for (UIView *view in notObtainScrollView.subviews) {
        if(view.tag >= 2000)
            [view removeFromSuperview];
    }
    pos_x = 0;
    width = 24;
    height = width;
    pos_y = (notObtainDetailView.frame.size.height - height) / 3;
    leftTicketButton_notObtain = [[UIButton alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [leftTicketButton_notObtain setBackgroundImage:[UIImage imageNamed:@"arrow_pre"] forState:UIControlStateNormal];
    [leftTicketButton_notObtain setTag:1];
    [leftTicketButton_notObtain setHidden:YES];
    [notObtainDetailView addSubview:leftTicketButton_notObtain];
    pos_x = notObtainDetailView.frame.size.width - width;
    pos_y = leftTicketButton_notObtain.frame.origin.y;
    width = 24;
    height = width;
    rightTicketButton_notObtain = [[UIButton alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [rightTicketButton_notObtain setBackgroundImage:[UIImage imageNamed:@"arrow_next"] forState:UIControlStateNormal];
    [rightTicketButton_notObtain setTag:1];
    [rightTicketButton_notObtain setHidden:YES];
    if([notObtainYet_ticket_Entries count] > 1)
        [rightTicketButton_notObtain setHidden:NO];
    [notObtainDetailView addSubview:rightTicketButton_notObtain];
    
    
    CGFloat sWidth, sHeight;
    sWidth  = notObtainScrollView.frame.size.width;
    sHeight = notObtainScrollView.frame.size.height;
    [notObtainScrollView setContentSize:CGSizeMake(sWidth * [notObtainYet_ticket_Entries count], sHeight)];
    
    if([notObtainYet_ticket_Entries count] == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
        notObtain_index = 1;
        [AppDelegate sharedAppDelegate].page_id = 42;
        [_notObtainTb reloadData];
        [self.view addSubview:notObtainView];
        [reloadTimer invalidate];
        reloadTimer = nil;
        return;
    }
    
    
    
    if(notObtainYet_viewArray)
        [notObtainYet_viewArray removeAllObjects];
    else
        notObtainYet_viewArray = [[NSMutableArray alloc] init];
    //讀取票券內容
    if([notObtainYet_ticket_Entries count] == 0) {  //無票券 回到未取票券清單
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
        [AppDelegate sharedAppDelegate].page_id = 42;
        [self.view addSubview:notObtainView];
        return;
    }
    
    for (NSInteger idx=0;idx<[notObtainYet_ticket_Entries count];idx++) {
        CGRect frame = CGRectMake(sWidth*idx, 0, sWidth, sHeight);
        TicketView *view  = [[TicketView alloc] initWithFrame:frame];
        view.tag = idx + 2000;
        [view setBackgroundColor:[UIColor clearColor]];
        TicketObject *obj = [notObtainYet_ticket_Entries objectAtIndex:idx];
        view.orderTime = obj.orderTime;
        view.productName = obj.performanceName;
        view.showTime = obj.startTime;
        view.place = obj.placeName;
        view.performanceNo = obj.performanceNo;
        view.ticketId = obj.ticketId;
        view.price = obj.orderPrice;
        view.priceType = obj.priceType;
        view.taxNumber = obj.tax_number;
        view.taxInfo = obj.tax_info;
        view.getMethodCode = obj.get_method_no;
        view.sendMailDate = obj.mail_date;
        view.printDate = obj.print_date;
        view.sendMailNumber = obj.mail_sn;
        view.orderNumber = obj.orderNumber;
        view.isObtained = @"N";
        view.ticketType = obj.ticketType;
        view.transferFlag = obj.transferFlag;
        view.orderSeat = obj.orderSeat;
        view.senderName = obj.senderName;
        view.senderMessage = obj.senderMessage;
        view.getMethod = obj.getMethod;
        view.payMethod = obj.payMethod;
        view.virtualNo = obj.virtualNo;
        view.verifyNo = obj.verifyNo;
        view.admissionStatus = obj.admissionStatus;
        view.remark = obj.RockNo;
        view.isGoods = obj.isGoods;
        view.updateTicketView = @"Y";
        if(view.frame.size.height > sHeight)
            sHeight = view.frame.size.height;
        [notObtainScrollView setContentSize:CGSizeMake(sWidth * [notObtainYet_ticket_Entries count], sHeight)];
        [notObtainYet_viewArray addObject:view];
        [notObtainScrollView addSubview:view];
    }
}


- (void)pressMenu_1 {
    if(menu_select == 1)
        return;
    menu_select = 1;
    pos_x = (DEVICE_WIDTH / 2) * 0;
    CGRect frame = menu_underline.frame;
    frame.origin.x = pos_x;
    menu_underline.frame = frame;
    menu_label_1.textColor = [UIColor whiteColor];
    menu_label_1.backgroundColor = [UIColor colorWithRed:(0xc1/255.0) green:(0x17/255.0) blue:(0x2d/255.0) alpha:1.0f];
    menu_label_2.textColor = [UIColor whiteColor];
    menu_label_2.backgroundColor = [UIColor colorWithRed:(0xd2/255.0) green:(0x17/255.0) blue:(0x2d/255.0) alpha:1.0f];
    
    [notObtainView removeFromSuperview];
    [notObtainDetailView removeFromSuperview];
    [obtainView removeFromSuperview];
    [obtainDeatilView removeFromSuperview];
    [pageControl_notObtain removeFromSuperview];
    [pageControl_obtain removeFromSuperview];
    [self.view addSubview:pageControl_obtain];
    [reloadTimer invalidate];
    reloadTimer = nil;
    //[self getTicketFromDB];
    if(obtained_index == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
        [AppDelegate sharedAppDelegate].page_id = 40;
        [self.view addSubview:obtainView];
        [pageControl_notObtain removeFromSuperview];
        [pageControl_obtain removeFromSuperview];
    } else if(obtained_index == 2) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_1" object:nil userInfo:nil];
        [AppDelegate sharedAppDelegate].page_id = 41;
        [self.view addSubview:obtainDeatilView];
        reloadTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f  target:self selector:@selector(reloadCurrentView) userInfo:nil repeats:NO];
        
        [self.view addSubview:pageControl_obtain];
        
    }

}

- (void)pressMenu_2 {
    if(menu_select == 2)
        return;
    menu_select = 2;
    pos_x = (DEVICE_WIDTH / 2) * 1;
    CGRect frame = menu_underline.frame;
    frame.origin.x = pos_x;
    menu_underline.frame = frame;
    menu_label_2.textColor = [UIColor whiteColor];
    menu_label_2.backgroundColor = [UIColor colorWithRed:(0xc1/255.0) green:(0x17/255.0) blue:(0x2d/255.0) alpha:1.0f];
    menu_label_1.textColor = [UIColor whiteColor];
    menu_label_1.backgroundColor = [UIColor colorWithRed:(0xd2/255.0) green:(0x17/255.0) blue:(0x2d/255.0) alpha:1.0f];
    
    [notObtainView removeFromSuperview];
    [notObtainDetailView removeFromSuperview];
    [obtainView removeFromSuperview];
    [obtainDeatilView removeFromSuperview];
    [pageControl_notObtain removeFromSuperview];
    [pageControl_obtain removeFromSuperview];
    [self.view addSubview:pageControl_notObtain];
    [reloadTimer invalidate];
    reloadTimer = nil;
    //[self getTicketFromDB];
    if(notObtain_index == 1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
        [AppDelegate sharedAppDelegate].page_id = 42;
        [self.view addSubview:notObtainView];
        [pageControl_notObtain removeFromSuperview];
        [pageControl_obtain removeFromSuperview];
    } else if(notObtain_index == 2) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_1" object:nil userInfo:nil];
        [AppDelegate sharedAppDelegate].page_id = 43;
        [self.view addSubview:notObtainDetailView];
    }
}

//已取票券
- (void)getObtainedTicketFromInternet:(NSInteger)tag {
    obtainedEntries = nil;
    obtainedEntries = [[NSMutableArray alloc] init];
    NSString *urlStr = [NSString stringWithFormat:@"%@/U0201_", SERVER_URL];
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:30];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    udid = [NSString stringWithFormat:@"%@%@",udid,@"已取票券"];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    NSDictionary *dict = [[Tools getInstance] getUserInfo];
    NSString *account = [dict objectForKey:ACCOUNT];
    NSString *tokenId = [dict objectForKey:TOKENID];
    [params setObject:aesEncryptString(account, AES_KEY) forKey:@"account"];
    [params setObject:aesEncryptString(tokenId, AES_KEY) forKey:@"interface_token_id"];
    
    [_manager POST:urlStr parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status  = [dd objectForKey:@"status"];
        NSString *message = [dd objectForKey:@"message"];
        
        if(status == nil) {
            [self closeLoadingView];
            [self showAlertMessage:@"錯誤訊息" message:@"系統發生錯誤,請洽服務人員！" tag:0];
            //NSLog(@"系統發生錯誤");
        } else if([status isEqualToString:@"S000"] == YES) {
            self->friends_dict = [dd objectForKey:@"friend"];
            if([self->friends_dict isEqual:[NSNull null]]) {

            } else {



            }
            NSArray *obj = (NSArray *)[responseObject objectForKey:@"performance"];
            self->obtainedTicketArr = nil;
            self->obtainedTicketArr = [[NSMutableArray alloc] init];
            if((NSNull *)obj != [NSNull null]) {
                [self clearDB];
                [self phaserProduct:obj toTicketsArray:self->obtainedTicketArr toPerformanceEntries:self->obtainedEntries];
                self->obtainedEntries = [self sortPerformanceDataArray:self->obtainedEntries];
                [self->_obtainTb reloadData];
                [self writeDB];
                [self getTicketFromDB];
                if(tag == 110) {
                    [self showAlertMessage:@"提示訊息" message:@"票券轉送成功！" tag:0];
                    PerformanceObject *tickets = [self->obtainedEntries objectAtIndex:self->selectIndex_obtain];
                    self->obtained_ticket_Entries = [tickets.tickets copy];
                    [self->_obtainTb reloadData];
                    [self initObtainTicketView];
                }
                if(tag == 111) {
                    [self showAlertMessage:@"提示訊息" message:@"票券取回成功！" tag:0];
                    PerformanceObject *tickets = [self->obtainedEntries objectAtIndex:self->selectIndex_obtain];
                    self->obtained_ticket_Entries = [tickets.tickets copy];
                    [self initObtainTicketView];
                }
                [self getNotObtainYetTicketFromInternet:tag];
            } else {
              [self clearDB];
              [self getNotObtainYetTicketFromInternet:tag];
              //NSLog(@"無票券");
            }
        } else {
            [self closeLoadingView];
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
            //NSLog(@"系統發生錯誤");
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //NSLog(@"Error");
        [self closeLoadingView];
        if (self->notObtainYetTicketArr)
            [self->notObtainYetTicketArr removeAllObjects];
        if (self->notObtainYetEntries)
            [self->notObtainYetEntries removeAllObjects];
        [self getTicketFromDB];
        [self.view addSubview:self->obtainView];
        [self->_obtainTb reloadData];
        [self showAlertMessage:@"提示訊息" message:@"連線逾時或無網路, 請確認您的手機網路狀態！" tag:0];
        }];
}

//未取票券
- (void)getNotObtainYetTicketFromInternet:(NSInteger)tag {
    notObtainYetEntries = nil;
    notObtainYetEntries = [[NSMutableArray alloc] init];
    NSString *urlStr = [NSString stringWithFormat:@"%@/U0202_", SERVER_URL];
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:10];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    udid = [NSString stringWithFormat:@"%@%@",udid,@"未取票券"];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    NSDictionary *dict = [[Tools getInstance] getUserInfo];
    NSString *account = [dict objectForKey:ACCOUNT];
    NSString *tokenId = [dict objectForKey:TOKENID];
    [params setObject:aesEncryptString(account, AES_KEY) forKey:@"account"];
    [params setObject:aesEncryptString(tokenId, AES_KEY) forKey:@"interface_token_id"];
    
    [_manager POST:urlStr parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status  = [dd objectForKey:@"status"];
        NSString *message = [dd objectForKey:@"message"];
        if(status == nil) {
            [self closeLoadingView];
            [self showAlertMessage:@"錯誤訊息" message:@"系統發生錯誤,請洽服務人員！" tag:0];
            //NSLog(@"系統發生錯誤");
        } else if([status isEqualToString:@"S000"] == YES) {
            NSArray *obj = (NSArray *)[responseObject objectForKey:@"performance"];
            self->notObtainYetTicketArr = nil;
            self->notObtainYetTicketArr = [[NSMutableArray alloc] init];
            if ((NSNull *)obj != [NSNull null]) {
                [self phaserProduct:obj toTicketsArray:self->notObtainYetTicketArr toPerformanceEntries:self->notObtainYetEntries];
            }
            [self closeLoadingView];
            if(tag == 112) {   //接收票券
                [self showAlertMessage:@"提示訊息" message:@"票券接收成功！" tag:0];
                if([self->notObtainYetEntries count] != 0) {
                    if(self->selectIndex_notObtain > [self->notObtainYetEntries count] - 1) {
                        self->notObtainYet_ticket_Entries = nil;
                    } else if(self->current_count_notObtain != [self->notObtainYetEntries count]) {
                        self->notObtainYet_ticket_Entries = nil;
                    } else {
                        PerformanceObject *tickets = [self->notObtainYetEntries objectAtIndex:self->selectIndex_notObtain];
                        self->notObtainYet_ticket_Entries = [tickets.tickets copy];
                    }
                } else {
                    self->notObtainYet_ticket_Entries = nil;
                }
                [self initNotObtainTicketView];
            }
            
            if(tag == 113) {   //退回票券
                [self showAlertMessage:@"提示訊息" message:@"票券退回成功！" tag:0];
                if([self->notObtainYetEntries count] != 0) {
                    if(self->selectIndex_notObtain > [self->notObtainYetEntries count] - 1) {
                        self->notObtainYet_ticket_Entries = nil;
                    } else if(self->current_count_notObtain != [self->notObtainYetEntries count]) {
                        self->notObtainYet_ticket_Entries = nil;
                    } else {
                        PerformanceObject *tickets = [self->notObtainYetEntries objectAtIndex:self->selectIndex_notObtain];
                        self->notObtainYet_ticket_Entries = [tickets.tickets copy];
                    }
                } else {
                    self->notObtainYet_ticket_Entries = nil;
                }
                [self initNotObtainTicketView];
            }
            [self->_notObtainTb reloadData];
        } else {
            [self closeLoadingView];
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
            //NSLog(@"系統發生錯誤");
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        if (self->notObtainYetTicketArr)
            [self->notObtainYetTicketArr removeAllObjects];
        if (self->notObtainYetEntries)
            [self->notObtainYetEntries removeAllObjects];
        [self getTicketFromDB];
        [self showAlertMessage:@"提示訊息" message:@"連線逾時或無網路, 請確認您的手機網路狀態！" tag:0];
    }];

}

//轉贈票券
- (void)sendTicketToServer {
    NSString *urlStr = [NSString stringWithFormat:@"%@/DI0301_", SERVER_URL];
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:10];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    if(is_batch_seasonticket == YES) {
        [params setObject:aesEncryptString(@"Y", AES_KEY)  forKey:@"is_batch_seasonticket"];
    } else {
        [params setObject:aesEncryptString(@"N", AES_KEY)  forKey:@"is_batch_seasonticket"];
    }
    
    NSString *udid = [[Tools getInstance] getRequest_id];
    udid = [NSString stringWithFormat:@"%@%@",udid,@"轉贈票券"];
    //NSLog(@"轉贈票券udid = %@",udid);
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    NSDictionary *dict = [[Tools getInstance] getUserInfo];
    NSString *account = [dict objectForKey:ACCOUNT];
    NSString *tokenId = [dict objectForKey:TOKENID];
    [params setObject:aesEncryptString(account, AES_KEY) forKey:@"account"];
    [params setObject:aesEncryptString(tokenId, AES_KEY) forKey:@"interface_token_id"];
    //.[params setObject:aesEncryptString([AppDelegate sharedAppDelegate].loginType, AES_KEY) forKey:@"authtype"];
    [params setObject:aesEncryptString(sendTicketId, AES_KEY)  forKey:@"ticket_id"];
    [params setObject:aesEncryptString(emailTextField.text, AES_KEY)  forKey:@"transferEmail"];
    [params setObject:aesEncryptString(mobileTextField.text, AES_KEY)  forKey:@"transferPhone"];
    [params setObject:aesEncryptString(memoTextField.text, AES_KEY)  forKey:@"transferRemark"];
    if(![friendsField.text isEqual:@""]) {
        NSDictionary *dict_f = [friends_dict objectAtIndex:friend_select];
        [params setObject:aesEncryptString([dict_f objectForKey:@"friend_id"], AES_KEY)  forKey:@"transferfriend_id"];
    }
    [_manager POST:urlStr parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status  = [dd objectForKey:@"status"];
        NSString *message = [dd objectForKey:@"message"];
        if(status == nil) {
            [self showAlertMessage:@"錯誤訊息" message:@"系統發生錯誤,請洽服務人員！" tag:0];
            //NSLog(@"系統發生錯誤");
        } else if([status isEqualToString:@"S000"] == YES) {
            [self getTicketsData:110];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
            //NSLog(@"系統發生錯誤");
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showAlertMessage:@"提示訊息" message:@"連線逾時或無網路, 請確認您的手機網路狀態！" tag:0];
    }];
}

//取回票券
- (void)getTicketBack {
    NSString *urlStr = [NSString stringWithFormat:@"%@/U0302_", SERVER_URL];
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:10];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    udid = [NSString stringWithFormat:@"%@%@",udid,@"取回票券"];
    //NSLog(@"取回票券udid = %@",udid);
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    NSDictionary *dict = [[Tools getInstance] getUserInfo];
    NSString *account = [dict objectForKey:ACCOUNT];
    NSString *tokenId = [dict objectForKey:TOKENID];
    [params setObject:aesEncryptString(account, AES_KEY) forKey:@"account"];
    [params setObject:aesEncryptString(tokenId, AES_KEY) forKey:@"interface_token_id"];
    [params setObject:aesEncryptString(sendTicketId, AES_KEY)  forKey:@"ticket_id"];
    
    [_manager POST:urlStr parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status  = [dd objectForKey:@"status"];
        NSString *message = [dd objectForKey:@"message"];
        if(status == nil) {
            [self showAlertMessage:@"錯誤訊息" message:@"系統發生錯誤,請洽服務人員！" tag:0];
            //NSLog(@"系統發生錯誤");
        } else if([status isEqualToString:@"S000"] == YES) {
            [self getTicketsData:111];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
            //NSLog(@"系統發生錯誤");
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showAlertMessage:@"提示訊息" message:@"連線逾時或無網路, 請確認您的手機網路狀態！" tag:0];
    }];
}

//接受票券
- (void)acceptTicket {
    NSString *urlString = [NSString stringWithFormat:@"%@/U0303_", SERVER_URL];
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:10];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    udid = [NSString stringWithFormat:@"%@%@",udid,@"接受票券"];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    NSDictionary *dict = [[Tools getInstance] getUserInfo];
    NSString *account = [dict objectForKey:ACCOUNT];
    NSString *tokenId = [dict objectForKey:TOKENID];
    [params setObject:aesEncryptString(account, AES_KEY) forKey:@"account"];
    [params setObject:aesEncryptString(tokenId, AES_KEY) forKey:@"interface_token_id"];
    [params setObject:aesEncryptString(sendTicketId, AES_KEY)  forKey:@"ticket_id"];
    [_manager POST:urlString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status  = [dd objectForKey:@"status"];
        NSString *message = [dd objectForKey:@"message"];
        if(status == nil) {
            [self showAlertMessage:@"錯誤訊息" message:@"系統發生錯誤,請洽服務人員！" tag:0];
            //NSLog(@"系統發生錯誤");
        } else if([status isEqualToString:@"S000"] == YES) {
            [self getTicketsData:112];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
            //NSLog(@"系統發生錯誤");
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showAlertMessage:@"提示訊息" message:@"連線逾時或無網路, 請確認您的手機網路狀態！" tag:0];
    }];
 }

//退回票券
- (void)goBackTicket {
    NSString *urlString = [NSString stringWithFormat:@"%@/U0304_", SERVER_URL];
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:10];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    udid = [NSString stringWithFormat:@"%@%@",udid,@"退回票券"];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    NSDictionary *dict = [[Tools getInstance] getUserInfo];
    NSString *account = [dict objectForKey:ACCOUNT];
    NSString *tokenId = [dict objectForKey:TOKENID];
    [params setObject:aesEncryptString(account, AES_KEY) forKey:@"account"];
    [params setObject:aesEncryptString(tokenId, AES_KEY) forKey:@"interface_token_id"];
    [params setObject:aesEncryptString(sendTicketId, AES_KEY)  forKey:@"ticket_id"];
    [_manager POST:urlString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status  = [dd objectForKey:@"status"];
        NSString *message = [dd objectForKey:@"message"];
        if(status == nil) {
            [self showAlertMessage:@"錯誤訊息" message:@"系統發生錯誤,請洽服務人員！" tag:0];
            //NSLog(@"系統發生錯誤");
        } else if([status isEqualToString:@"S000"] == YES) {
            [self getTicketsData:113];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
            //NSLog(@"系統發生錯誤");
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showAlertMessage:@"提示訊息" message:@"連線逾時或無網路, 請確認您的手機網路狀態！" tag:0];
    }];
}



- (void)getTicketFromDB {
    obtainedEntries = nil;
    obtainedEntries = [[NSMutableArray alloc] init];
    FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate sharedAppDelegate].writableDBPath];
    if([db open]) {
        NSString *account = [[Tools getInstance] getUserAccount];
        NSString *sqlStr = [NSString stringWithFormat:@"select Performance_id,Performance_name,Performance_no,categories_name,product_name,product_image,start_time,place_name from Performance where account='%@'", account];
        FMResultSet *rs = [db executeQuery:sqlStr];
        while([rs next]) {
            PerformanceObject *perObj = [[PerformanceObject alloc] init];
            perObj.performanceId = [rs stringForColumnIndex:0];
            perObj.performanceName = [rs stringForColumnIndex:1];
            perObj.performanceNo = [rs stringForColumnIndex:2];
            perObj.categoriesName = [rs stringForColumnIndex:3];
            perObj.productName = [rs stringForColumnIndex:4];
            //perObj.productImage = [rs dataForColumnIndex:5];
            perObj.imageUrl = [rs stringForColumnIndex:5];
            perObj.startTime = [rs stringForColumnIndex:6];
            perObj.placeName = [rs stringForColumnIndex:7];
            NSString *sqlStr_1 = [NSString stringWithFormat:@"select ticket_id,order_time,order_number,order_seat,price_type,order_price,ticket_type,get_method,pay_method,transfer_flag,place_name,performance_no,start_time,buy_by_self,categories_name,verify_no,virtual_no,admission_status,tax_number,tax_info,get_method_no,notice_msg,mail_sn,RockNo,print_date,mail_date,sender_name,sender_msg,receiver_name,isGoods,goods_item,notice_msg,notice_msg2,notice_msg3,total_amount,ticket_format,is_seasonticket from Tickets where Performance_no='%@' and product_name = '%@' and account='%@' order by order_number asc,isGoods",perObj.performanceNo,perObj.productName,account];
            FMResultSet *rs_1 = [db executeQuery:sqlStr_1];
            NSMutableArray *tickets = [[NSMutableArray alloc] init];
            while([rs_1 next]) {
                TicketObject *obj  = [[TicketObject alloc] init];
                obj.ticketId       = [rs_1 stringForColumnIndex:0];
                obj.orderTime      = [rs_1 stringForColumnIndex:1];
                obj.orderNumber    = [rs_1 stringForColumnIndex:2];
                obj.orderSeat      = [rs_1 stringForColumnIndex:3];
                obj.priceType      = [rs_1 stringForColumnIndex:4];
                obj.orderPrice     = [rs_1 stringForColumnIndex:5];
                obj.ticketType     = [rs_1 stringForColumnIndex:6];
                obj.getMethod      = [rs_1 stringForColumnIndex:7];
                obj.payMethod      = [rs_1 stringForColumnIndex:8];
                obj.transferFlag   = [rs_1 stringForColumnIndex:9];
                obj.placeName      = [rs_1 stringForColumnIndex:10];
                obj.performanceNo  = [rs_1 stringForColumnIndex:11];
                obj.startTime      = [rs_1 stringForColumnIndex:12];
                obj.buyBySelf      = [rs_1 stringForColumnIndex:13];
                obj.categoriesName = [rs_1 stringForColumnIndex:14];
                obj.verifyNo       = [rs_1 stringForColumnIndex:15];
                obj.virtualNo      = [rs_1 stringForColumnIndex:16];
                obj.admissionStatus = [rs_1 stringForColumnIndex:17];
                obj.tax_number = [rs_1 stringForColumnIndex:18];
                obj.tax_info = [rs_1 stringForColumnIndex:19];
                obj.get_method_no = [rs_1 stringForColumnIndex:20];
                obj.notice_msg = [rs_1 stringForColumnIndex:21];
                obj.mail_sn = [rs_1 stringForColumnIndex:22];
                obj.RockNo = [rs_1 stringForColumnIndex:23];
                obj.print_date = [rs_1 stringForColumnIndex:24];
                obj.mail_date = [rs_1 stringForColumnIndex:25];
                obj.senderName = [rs_1 stringForColumnIndex:26];
                obj.senderMessage = [rs_1 stringForColumnIndex:27];
                obj.receiver_name = [rs_1 stringForColumnIndex:28];
                obj.isGoods = [rs_1 stringForColumnIndex:29];
                obj.goodsItem = [rs_1 stringForColumnIndex:30];
/*
                //NSString to NSDictionary
                NSString *dataStr = [rs_1 stringForColumnIndex:30];
                if(dataStr.length != 0) {
                    NSError *err = nil;
                    NSArray *arr =
                     [NSJSONSerialization JSONObjectWithData:[dataStr dataUsingEncoding:NSUTF8StringEncoding]
                                                     options:NSJSONReadingMutableContainers
                                                       error:&err];
                    NSMutableDictionary *dict = arr[0];
                    obj.goodsItem = dict;
                } else {
                    obj.goodsItem = nil;
                }
*/
                obj.notice_msg = [rs_1 stringForColumnIndex:31];
                obj.notice_msg2 = [rs_1 stringForColumnIndex:32];
                obj.notice_msg3 = [rs_1 stringForColumnIndex:33];
                obj.total_amount = [rs_1 stringForColumnIndex:34];
                obj.ticket_format = [rs_1 stringForColumnIndex:35];
                obj.is_seasonticket = [rs_1 stringForColumnIndex:36];
                obj.performanceName = perObj.performanceName;
                [tickets addObject:obj];
            }
            [rs_1 close];
            perObj.tickets = tickets;
            [obtainedEntries addObject:perObj];
        }
        [rs close];
        [db close];
    }
    [self closeLoadingView];
}

- (void)phaserProduct:(NSArray *)obj toTicketsArray:(NSMutableArray *)array toPerformanceEntries:(NSMutableArray *)entries {
    for (NSDictionary *performance in obj) {
        PerformanceObject *perf = [[PerformanceObject alloc] init];
        perf.productName     = [performance objectForKey:@"product_name"];
        perf.performanceName = [performance objectForKey:@"performance_name"];
        perf.performanceNo   = [performance objectForKey:@"performance_no"];
        perf.categoriesName  = [performance objectForKey:@"categories_name"];
        perf.startTime       = [performance objectForKey:@"start_time"];
        perf.placeName       = [performance objectForKey:@"place_name"];
        perf.imageUrl = [performance objectForKey:@"image_url"];

/*
        NSString *tmpStr = [performance objectForKey:@"image"];
        if([tmpStr length] > 0) {
            perf.productImage = [[NSData alloc] initWithBase64EncodedString:tmpStr options:0];
        }
*/
        perf.tickets = [[NSMutableArray alloc] init];
        //取得票券清單
        NSArray *ticketObj = (NSArray *)[performance objectForKey:@"ticket"];
        if(![ticketObj isEqual:[NSNull null]]) {
            for(NSDictionary *ticketDict in ticketObj) {
                TicketObject *ticket  = [[TicketObject alloc] init];
                ticket.performanceName = perf.performanceName;
                ticket.isGoods        = @"N";
                ticket.ticketId       = [ticketDict objectForKey:@"ticket_id"];
                ticket.orderTime      = [ticketDict objectForKey:@"order_time"];
                ticket.orderNumber    = [ticketDict objectForKey:@"order_number"];
                ticket.orderSeat      = [ticketDict objectForKey:@"order_seat"];
                ticket.priceType      = [ticketDict objectForKey:@"price_type"];
                ticket.orderPrice     = [ticketDict objectForKey:@"order_price"];
                ticket.getMethod      = [ticketDict objectForKey:@"get_method"];
                ticket.payMethod      = [ticketDict objectForKey:@"pay_method"];
                ticket.transferFlag   = [ticketDict objectForKey:@"transfer_flag"];
                ticket.ticketType     = [ticketDict objectForKey:@"ticket_type"];
                ticket.productName    = perf.productName;
                ticket.placeName      = perf.placeName;
                ticket.senderName     = [ticketDict objectForKey:@"send_name"];
                ticket.senderMessage  = [ticketDict objectForKey:@"send_msg"];
                ticket.performanceNo  = perf.performanceNo;
                ticket.buyBySelf      = [ticketDict objectForKey:@"buybyself"];
                ticket.categoriesName = perf.categoriesName;
                ticket.verifyNo       = [ticketDict objectForKey:@"verify_no"];
                ticket.virtualNo      = [ticketDict objectForKey:@"virtual_no"];
                ticket.admissionStatus = [ticketDict objectForKey:@"admission_status"];
                ticket.startTime = perf.startTime;
                ticket.tax_number = [ticketDict objectForKey:@"tax_number"];
                ticket.tax_info = [ticketDict objectForKey:@"tax_info"];
                ticket.get_method_no = [ticketDict objectForKey:@"get_method_no"];
                ticket.notice_msg = [ticketDict objectForKey:@"notice_msg"];
                ticket.mail_sn = [ticketDict objectForKey:@"mail_sn"];
                ticket.RockNo = [ticketDict objectForKey:@"RockNo"];
                ticket.print_date = [ticketDict objectForKey:@"print_date"];
                ticket.mail_date = [ticketDict objectForKey:@"mail_date"];
                ticket.receiver_name = [ticketDict objectForKey:@"receiver_name"];
                ticket.ticket_format = [ticketDict objectForKey:@"ticket_format"];
                ticket.is_seasonticket = [ticketDict objectForKey:@"is_seasonticket"];
                
                //ticket.is_seasonticket = @"Y";
                
                [perf.tickets addObject:ticket];
                [array addObject:ticket];
            }
        }
        NSArray *goodsObj = (NSArray *)[performance objectForKey:@"goods"];
        if(![goodsObj isEqual:[NSNull null]]) {
            for(NSDictionary *goodsDict in goodsObj) {
                TicketObject *ticket  = [[TicketObject alloc] init];
                ticket.performanceName = perf.performanceName;
                ticket.isGoods        = @"Y";
                ticket.productName    = perf.productName;
                ticket.placeName      = perf.placeName;
                ticket.performanceNo  = perf.performanceNo;
                ticket.categoriesName = perf.categoriesName;
                ticket.categoriesName = perf.categoriesName;
                ticket.startTime      = perf.startTime;
                //
                ticket.orderNumber    = [goodsDict objectForKey:@"order_number"];
                ticket.admissionStatus= [goodsDict objectForKey:@"admission_status"];
                ticket.notice_msg3    = [goodsDict objectForKey:@"notice_msg3"];
                ticket.total_amount   = [goodsDict objectForKey:@"total_amount"];
                ticket.getMethod      = [goodsDict objectForKey:@"get_method"];
                ticket.get_method_no  = [goodsDict objectForKey:@"get_method_no"];
                ticket.ticketType     = [goodsDict objectForKey:@"ticket_type"];
                ticket.goodsItem      = [goodsDict objectForKey:@"goodsItem"];
                ticket.ticket_format  = [goodsDict objectForKey:@"ticket_format"];
            /*
                //NSDictionary to NSString
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ticket.goodsItem options:0 error:0];
                NSString *dataStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                //NSString to NSDictionary
                NSError *err = nil;
                NSArray *arr =
                    [NSJSONSerialization JSONObjectWithData:[dataStr dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
                NSMutableDictionary *dict = arr[0];
                for (NSMutableDictionary *dictionary in arr) {
                }
            */
                ticket.orderTime      = [goodsDict objectForKey:@"order_time"];
                ticket.payMethod      = [goodsDict objectForKey:@"pay_method"];
                ticket.ticketId       = [goodsDict objectForKey:@"ticket_id"];
                ticket.notice_msg     = [goodsDict objectForKey:@"notice_msg"];
                ticket.notice_msg2    = [goodsDict objectForKey:@"notice_msg2"];
                [perf.tickets addObject:ticket];
                [array addObject:ticket];
            }
        }
        perf.tickets = [self sortTicketsArray:perf.tickets];
        [entries addObject:perf];
    }
    entries = [self sortPerformanceDataArray:entries];
}

- (NSMutableArray *)sortPerformanceDataArray:(NSMutableArray *)array {
    
    array = (NSMutableArray *)[array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(PerformanceObject *)a startTime];
        if ([first rangeOfString:@"~"].location != NSNotFound)
            first = [first substringToIndex:[first rangeOfString:@"~"].location - 1];
        NSString *second = [(PerformanceObject *)b startTime];
        if ([second rangeOfString:@"~"].location != NSNotFound)
            second = [second substringToIndex:[second rangeOfString:@"~"].location - 1];
        
        return [first compare:second];
    }];
    
    return array;
}

- (NSMutableArray *)sortTicketsArray:(NSMutableArray *)array {
    array = (NSMutableArray *)[array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(TicketObject *)a orderTime];
        if ([first rangeOfString:@"~"].location != NSNotFound)
            first = [first substringToIndex:[first rangeOfString:@"~"].location - 1];
        NSString *second = [(TicketObject *)b orderTime];
        if ([second rangeOfString:@"~"].location != NSNotFound)
            second = [second substringToIndex:[second rangeOfString:@"~"].location - 1];
        
        return [first compare:second];
    }];
    
    return array;
}

//無票券要清除ＤＢ
- (void)clearDB {
    FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate sharedAppDelegate].writableDBPath];
    if([db open]) {
        NSString *account = [[Tools getInstance] getUserAccount];
        NSString *sqlStr = [NSString stringWithFormat:@"delete from Performance where account = '%@'", account];
        [db executeUpdate:sqlStr];
        sqlStr = [NSString stringWithFormat:@"delete from Tickets where account = '%@'", account];
        [db executeUpdate:sqlStr];
    }
    [db close];
}



- (void)writeDB {
    NSInteger performanceCount = [obtainedEntries count];
    FMDatabase *db = [FMDatabase databaseWithPath:[AppDelegate sharedAppDelegate].writableDBPath];
    if([db open]) {
        
        NSString *account = [[Tools getInstance] getUserAccount];
        NSString *sqlStr = [NSString stringWithFormat:@"delete from Performance where account = '%@'",account];
        [db executeUpdate:sqlStr];
        sqlStr = [NSString stringWithFormat:@"delete from Tickets where account = '%@'", account];
        [db executeUpdate:sqlStr];
        for(NSInteger idx = 0;idx < performanceCount;idx++) {
            PerformanceObject *perf = [obtainedEntries objectAtIndex:idx];
            //新增場次
            [db executeUpdate:@"insert into Performance (performance_id,performance_name,performance_no,categories_name,product_name,product_image,start_time,place_name,account) values (?,?,?,?,?,?,?,?,?)", perf.performanceId, perf.performanceName, perf.performanceNo, perf.categoriesName, perf.productName, perf.imageUrl, perf.startTime, perf.placeName, account];
            
            NSArray *tickets = perf.tickets;
            NSInteger ticketCount = [tickets count];
            for(NSInteger idx1 = 0; idx1 < ticketCount; idx1++) {
                TicketObject *ticket = [tickets objectAtIndex:idx1];
                //新增票券 isGoods total_amount notice_msg notice_msg2 notice_msg3
                //NSDictionary to NSString
                NSString *goodItems = @"";
                if(ticket.goodsItem != nil) {
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ticket.goodsItem options:0 error:0];
                    goodItems = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                }
                //for test
                
                [db executeUpdate:@"Insert into Tickets (performance_id,performance_no,ticket_id,product_name,order_time,order_number,order_seat,price_type,order_price,get_method,pay_method,ticket_type,transfer_flag,place_name,status,download_time,start_time,buy_by_self,categories_name,account,verify_no,virtual_no,admission_status,tax_number,tax_info,get_method_no,notice_msg,mail_sn,RockNo,print_date,mail_date,sender_name,sender_msg,receiver_name,isGoods,notice_msg,notice_msg2,notice_msg3,total_amount,goods_item,ticket_format,is_seasonticket) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ,?,?,?,?,?,?,?,?)", perf.performanceId, perf.performanceNo, ticket.ticketId, perf.productName, ticket.orderTime, ticket.orderNumber, ticket.orderSeat, ticket.priceType, ticket.orderPrice, ticket.getMethod, ticket.payMethod, ticket.ticketType, ticket.transferFlag, ticket.placeName, ticket.status, ticket.downloadTime, ticket.startTime, ticket.buyBySelf, perf.categoriesName, account,ticket.verifyNo,ticket.virtualNo,ticket.admissionStatus,ticket.tax_number,ticket.tax_info,ticket.get_method_no,ticket.notice_msg,ticket.mail_sn,ticket.RockNo,ticket.print_date,ticket.mail_date,ticket.senderName,ticket.senderMessage,ticket.receiver_name,ticket.isGoods,ticket.notice_msg,ticket.notice_msg2,ticket.notice_msg3,ticket.total_amount,goodItems,ticket.ticket_format,ticket.is_seasonticket];
            }
        }
        [db close];
        
    } else {
        //NSLog(@"open DB error");
    }
    //[ticketTypeTableView reloadData];
}


- (void)showAlertMessage:(NSString *)title message:(NSString *)message tag:(NSUInteger)tag {
    [self closeLoadingView];
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setDelegate:self];
    [alertView setTag:tag];
    [[MessageAlertView getInstance] showAlertView:alertView withTitleMessage:title andContentMessage:message buttonsTitleArray:[NSArray arrayWithObjects:@"確定", nil]];
}

//顯示正在登入訊息
- (void)showLoadingView:(NSString *)message {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!self->waitingView) {
                self->waitingView = [MBProgressHUD showHUDAddedTo:[AppDelegate sharedAppDelegate].window animated:YES];
                self->waitingView.mode = MBProgressHUDModeIndeterminate;
            } else {
            }
        });
    });
}

- (void)closeLoadingView {
    [waitingView hideAnimated:YES];
    waitingView = nil;
}


#pragma UITableViewDelegate & UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    cell_height = 0;
    if(tableView.tag == 1) {
        //cell_height = (obtainView.frame.size.height - 20) / 1.5 + 10;
        cell_height = 250;
    } else if(tableView.tag == 2) {
        //cell_height = (notObtainView.frame.size.height - 20) / 1.5 + 10;
        cell_height = 250;
    } else  if(tableView.tag == 99) {
        cell_height = 40;
    }
    return cell_height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView.tag == 1)
        return [obtainedEntries count];
    else if(tableView.tag == 2)
        return [notObtainYetEntries count];
    else if(tableView.tag == 99)
        return [friends_dict count];
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView.tag == 1) {  //已取票券
        NSString *CustomCellIdentifier = @"ObtainedListCellIdentifier";
        if (!ob_nibsRegistered) {
            UINib *nib = [UINib nibWithNibName:@"TicketListCell" bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:CustomCellIdentifier];
            ob_nibsRegistered = YES;
        }
        TicketListCell *cell = [tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier];
        if (cell == nil) {
            cell = [[TicketListCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:CustomCellIdentifier];
        }
        PerformanceObject *ticket = [obtainedEntries objectAtIndex:indexPath.row];
        cell.name = [NSString stringWithFormat:@"%@", ticket.performanceName];
        cell.date = ticket.startTime;
        NSArray *array = ticket.tickets;
        cell.number = [NSString stringWithFormat:@"%lu",(unsigned long)[array count]];
        //cell.image = [UIImage imageWithData:ticket.productImage];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:ticket.imageUrl] placeholderImage:nil options:SDWebImageRetryFailed];
        cell.height = (obtainView.frame.size.height - 20) / 1.5 + 10;
        cell.height = 250;
        cell.width = _obtainTb.frame.size.width;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if(tableView.tag == 2) {  //未取票券
        NSString *CustomCellIdentifier = @"NotObtainedListCellIdentifier";
        if (!notob_nibsRegistered) {
            UINib *nib = [UINib nibWithNibName:@"TicketListCell" bundle:nil];
            [tableView registerNib:nib forCellReuseIdentifier:CustomCellIdentifier];
            notob_nibsRegistered = YES;
        }
        TicketListCell *cell = [tableView dequeueReusableCellWithIdentifier:CustomCellIdentifier];
        if (cell == nil) {
            cell = [[TicketListCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:CustomCellIdentifier];
        }
        PerformanceObject *ticket = [notObtainYetEntries objectAtIndex:indexPath.row];
        NSArray *array = ticket.tickets;
        cell.name = [NSString stringWithFormat:@"%@", ticket.performanceName];
        cell.date = ticket.startTime;
        cell.number = [NSString stringWithFormat:@"%lu",(unsigned long)[array count]];
        //cell.image = [UIImage imageWithData:ticket.productImage];
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:ticket.imageUrl] placeholderImage:nil options:SDWebImageRetryFailed];
        cell.height = (obtainView.frame.size.height - 20) / 1.5 + 10;
        cell.height = 250;
        cell.width = _obtainTb.frame.size.width;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else if(tableView.tag == 99) {
        NSString *CellIdentifier = @"FriendsDataCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        [friends_dict count];
        NSDictionary *dict = [friends_dict objectAtIndex:indexPath.row];
        cell.textLabel.text = [dict objectForKey:@"friend_name"];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView.tag == 1) {
        selectIndex_obtain = indexPath.row;
        obtained_index = 2;
        [AppDelegate sharedAppDelegate].page_id = 41;
        PerformanceObject *tickets = [obtainedEntries objectAtIndex:indexPath.row];
        obtained_ticket_Entries = [tickets.tickets copy];
        current_count_obtain = [obtained_ticket_Entries count];
        [obtainScrollView setContentOffset:CGPointZero animated:NO];
        [obtainView removeFromSuperview];
        [notObtainView removeFromSuperview];
        [notObtainDetailView removeFromSuperview];
        [self getTicketFromDB];
        [self initObtainTicketView];
        [self.view addSubview:obtainDeatilView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_1" object:nil userInfo:nil];
        reloadTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f  target:self selector:@selector(reloadCurrentView) userInfo:nil repeats:NO];
    } else if(tableView.tag == 2) {
        notObtain_index = 2;
        selectIndex_notObtain = indexPath.row;
        [AppDelegate sharedAppDelegate].page_id = 43;
        PerformanceObject *tickets = [notObtainYetEntries objectAtIndex:indexPath.row];
        notObtainYet_ticket_Entries = [tickets.tickets copy];
        current_count_notObtain = [notObtainYet_ticket_Entries count];
        [notObtainScrollView setContentOffset:CGPointZero animated:NO];
        [obtainView removeFromSuperview];
        [notObtainView removeFromSuperview];
        [obtainDeatilView removeFromSuperview];
        [self initNotObtainTicketView];
        [self.view addSubview:notObtainDetailView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_1" object:nil userInfo:nil];
    } else if(tableView.tag == 99) {
        friend_select = indexPath.row;
        NSDictionary *dict = [friends_dict objectAtIndex:indexPath.row];
        friendsField.text = [dict objectForKey:@"friend_name"];
        [friendPopover dismissPopoverAnimated:YES];
        mobileTextField.enabled = NO;
        emailTextField.enabled = NO;

    }
}

- (void)reloadCurrentView {
    if([AppDelegate sharedAppDelegate].page_id == 41) {
        obtained_currentViewIdx = obtainScrollView.contentOffset.x / obtainScrollView.frame.size.width;
        if(obtained_viewArray)
            if(obtained_currentViewIdx < [obtained_viewArray count]) {
                TicketView *ticketView = [obtained_viewArray objectAtIndex:obtained_currentViewIdx];
                ticketView.updateQrcode = @"Y";
            }
        reloadTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f  target:self selector:@selector(reloadCurrentView) userInfo:nil repeats:NO];
    }
}

- (void)backToObtainedListView {
    [notObtainView removeFromSuperview];
    [notObtainDetailView removeFromSuperview];
    [obtainView removeFromSuperview];
    [obtainDeatilView removeFromSuperview];
    [AppDelegate sharedAppDelegate].page_id = 40;
    obtained_index = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
    [_obtainTb reloadData];
    [self.view addSubview:obtainView];
    [reloadTimer invalidate];
    reloadTimer = nil;
    [pageControl_obtain removeFromSuperview];
    [pageControl_notObtain removeFromSuperview];
}

- (void)backToNotObtainedListView {
    [notObtainView removeFromSuperview];
    [notObtainDetailView removeFromSuperview];
    [obtainView removeFromSuperview];
    [obtainDeatilView removeFromSuperview];
    [AppDelegate sharedAppDelegate].page_id = 42;
    notObtain_index = 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
    [_notObtainTb reloadData];
    [self.view addSubview:notObtainView];
    [reloadTimer invalidate];
    reloadTimer = nil;
    [pageControl_obtain removeFromSuperview];
    [pageControl_notObtain removeFromSuperview];
}

//轉贈票券
- (void)sendTicket:(NSNotification *)notification {
    sendTicketId = [notification.userInfo objectForKey:@"ticket_id"];
    is_seasonticket = [notification.userInfo objectForKey:@"is_seasonticket"];
    [self showTransferTicketAlertView];
}

//取回票券
- (void)retrieveTicket:(NSNotification *)notification {
    sendTicketId = [notification.userInfo objectForKey:@"ticket_id"];
    [self showRetrieveTicketAlertView];
    
}

//退回票券
- (void)goBackTicket:(NSNotification *)notification {
    sendTicketId = [notification.userInfo objectForKey:@"ticket_id"];
    [self showSendBackTransferTicketAlertView];
}

//接收票券
- (void)acceptTicket:(NSNotification *)notification {
    sendTicketId = [notification.userInfo objectForKey:@"ticket_id"];
    [self showAcceptTransferTicketAlertView];
}

- (void)showAcceptTransferTicketAlertView {
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setDelegate:self];
    [alertView setTag:3];
    [[MessageAlertView getInstance] showAlertView:alertView withTitleMessage:@"提示訊息" andContentMessage:@"接收票券"  buttonsTitleArray:[NSArray arrayWithObjects:@"取消", @"確定", nil]];
}

- (void)showSendBackTransferTicketAlertView {
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setDelegate:self];
    [alertView setTag:4];
    [[MessageAlertView getInstance] showAlertView:alertView withTitleMessage:@"提示訊息" andContentMessage:@"退回票券"  buttonsTitleArray:[NSArray arrayWithObjects:@"取消", @"確定", nil]];
}

- (void)showRetrieveTicketAlertView {
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setDelegate:self];
    [alertView setTag:6];
    [[MessageAlertView getInstance] showAlertView:alertView withTitleMessage:@"提示訊息" andContentMessage:@"取回票券"  buttonsTitleArray:[NSArray arrayWithObjects:@"取消", @"確定", nil]];
}

- (void)showTransferTicketAlertView {
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setButtonTitles:[NSArray arrayWithObjects:@"取消轉送", @"確認轉送", nil]];
    [alertView setDelegate:self];
    [alertView setTag:5];
    containerView = [[UIView alloc] init];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, ALERT_VIEW_TITLE_MARGIN_TOP, ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_HEIGHT)];
    [titleLabel setText:@"轉送"];
    [titleLabel setTextColor:[UIColor messageAlertViewTitleColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:ALERT_VIEW_TITLE_FONT_SIZE]];
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, titleLabel.frame.origin.y + titleLabel.frame.size.height, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, 2.0f)];
    [separator setBackgroundColor:[UIColor messageAlertViewTitleColor]];
    
    NSString *contentMessage = @"轉送對象需為味全龍售票網會員，確認送出前，請確認資料正確性。";
    CGSize textSize = [[Tools getInstance] getTextSize:contentMessage withFont:[UIFont systemFontOfSize:ALERT_VIEW_MESSAGE_FONT_SIZE] maxSize:CGSizeMake(ALERT_VIEW_WIDTH, MAXFLOAT)];
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, separator.frame.origin.y + separator.frame.size.height + 5, ALERT_VIEW_WIDTH, textSize.height)];
    [messageLabel setText:contentMessage];
    [messageLabel setFont:[UIFont systemFontOfSize:ALERT_VIEW_MESSAGE_FONT_SIZE]];
    [messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [messageLabel setNumberOfLines:0];
    [messageLabel setTextAlignment:NSTextAlignmentLeft];
    [messageLabel setTextColor:[UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f]];

    friendsField = [[UITextField alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, messageLabel.frame.origin.y + messageLabel.frame.size.height + 5 , ALERT_VIEW_WIDTH, 30.0)];
    [friendsField setPlaceholder:@"好友名單"];
    UIImageView *imgv = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,28,28)];
    imgv.image = [self imageWithImage:[UIImage imageNamed:@"BtnSelect_3.png"] scaledToSize:CGSizeMake(28, 28)];
    friendsField.rightView = imgv;
    friendsField.rightViewMode = UITextFieldViewModeAlways;
    [friendsField setBorderStyle:UITextBorderStyleBezel];
    [friendsField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [friendsField setTag:99];
    [friendsField setDelegate:self];
    [friendsField addTarget:self action:@selector(doEditFieldDone:) forControlEvents:UIControlEventEditingDidEndOnExit];

    UILabel *friend_label = [[UILabel alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, friendsField.frame.origin.y + friendsField.frame.size.height, ALERT_VIEW_WIDTH, 15)];
    [friend_label setText:@"編輯好友名單可到【會員專區】中新增修改"];
    [friend_label setFont:[UIFont systemFontOfSize:10]];
    [friend_label setLineBreakMode:NSLineBreakByWordWrapping];
    [friend_label setNumberOfLines:0];
    [friend_label setTextAlignment:NSTextAlignmentLeft];
    [friend_label setTextColor:[UIColor redColor]];
    mobileTextField = [[UITextField alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, friend_label.frame.origin.y + friend_label.frame.size.height + 5 , ALERT_VIEW_WIDTH, 30.0)];
    [mobileTextField setPlaceholder:@"輸入對方手機號碼"];
    [mobileTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [mobileTextField setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [mobileTextField setTag:10];
    //[mobileTextField setText:@"0975087226"];
    [mobileTextField setDelegate:self];
    [mobileTextField addTarget:self action:@selector(doEditFieldDone:) forControlEvents:UIControlEventEditingDidEndOnExit];
    emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(mobileTextField.frame.origin.x, mobileTextField.frame.origin.y + mobileTextField.frame.size.height + 5, mobileTextField.frame.size.width, mobileTextField.frame.size.height)];
    [emailTextField setPlaceholder:@"輸入對方電子信箱"];
    [emailTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [emailTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [emailTextField setDelegate:self];
    //[emailTextField setText:@"ching90907@me.com"];
    [emailTextField addTarget:self action:@selector(doEditFieldDone:) forControlEvents:UIControlEventEditingDidEndOnExit];
    memoTextField = [[UITextField alloc] initWithFrame:CGRectMake(mobileTextField.frame.origin.x, emailTextField.frame.origin.y + emailTextField.frame.size.height + 5, mobileTextField.frame.size.width, mobileTextField.frame.size.height)];
    [memoTextField setPlaceholder:@"訊息內容"]; // @"訊息內容(限50字內容)"
    [memoTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [memoTextField addTarget:self action:@selector(doEditFieldDone:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [memoTextField setDelegate:self];

    pos_x = memoTextField.frame.origin.x;
    pos_y = memoTextField.frame.origin.y + memoTextField.frame.size.height + 10;
    width = 20;
    height = 20;
    checkBox = [[MICheckBox alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    checkBox.isChecked = NO;
    is_batch_seasonticket = NO;
    [checkBox addTarget:self action:@selector(clickCheckbox:) forControlEvents:UIControlEventTouchUpInside];
    pos_x = checkBox.frame.origin.x + checkBox.frame.size.width + 5;
    pos_y = checkBox.frame.origin.y;
    width = memoTextField.frame.size.width - pos_x;
    UILabel *titleLabel_1 = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [titleLabel_1 setText:@"轉贈此座位的成套季票"];
    [titleLabel_1 setFont:[UIFont boldSystemFontOfSize:16]];
    [titleLabel_1 setLineBreakMode:NSLineBreakByWordWrapping];
    [titleLabel_1 setNumberOfLines:0];
    [titleLabel_1 setTextAlignment:NSTextAlignmentLeft];
    [titleLabel_1 setTextColor:[UIColor blueColor]];
    [containerView addSubview:titleLabel];
    [containerView addSubview:separator];
    [containerView addSubview:messageLabel];
    [containerView addSubview:friendsField];
    [containerView addSubview:friend_label];
    [containerView addSubview:mobileTextField];
    [containerView addSubview:emailTextField];
    [containerView addSubview:memoTextField];
    if([is_seasonticket isEqualToString:@"Y"]) {
        [containerView addSubview:checkBox];
        [containerView addSubview:titleLabel_1];
        [containerView setFrame:CGRectMake(0, 0, mobileTextField.frame.origin.x * 2 + mobileTextField.frame.size.width, checkBox.frame.origin.y + checkBox.frame.size.height + 10)];
    } else {
        [containerView setFrame:CGRectMake(0, 0, mobileTextField.frame.origin.x * 2 + mobileTextField.frame.size.width, memoTextField.frame.origin.y + memoTextField.frame.size.height + 10)];
    }
    [alertView setContainerView:containerView];
    [alertView show];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    if(textField.tag == 99) {
        
        if(![friends_dict isEqual:[NSNull null]]) {
            UITableView *friendTb;
            friendTb = [[UITableView alloc] initWithFrame:CGRectMake(0.0,0.0, friendsField.frame.size.width, 240.0)];
            friendTb.dataSource = self;
            friendTb.delegate = self;
            friendTb.tag = 99;
            friendTb.scrollEnabled = NO;
            UIViewController *showView = [[UIViewController alloc] init];
            friendTb.separatorStyle = UITableViewCellSeparatorStyleNone;
            [showView.view addSubview:friendTb];
            friendPopover = [[WYPopoverController alloc] initWithContentViewController:showView];
            [friendPopover setPopoverContentSize:CGSizeMake(friendsField.frame.size.width, 240)];
            [friendPopover presentPopoverFromRect:friendsField.frame inView:containerView  permittedArrowDirections:WYPopoverArrowDirectionUp animated:YES];
            showView = nil;
            friendTb = nil;
        }
        return NO;
    }
    return YES;
}

- (IBAction)doEditFieldDone:(id)sender {
    [sender resignFirstResponder];
}

#pragma CustomIOS7AlertViewDelegate
- (void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSInteger tag = [alertView tag];
    
    switch (tag) {
        case 3: //接受票券
            if(buttonIndex == 1) {
                [self acceptTicket];
            }
            break;
        case 4: //退回票券
            if(buttonIndex == 1) {
                [self goBackTicket];
            }
            break;
        case 5: //轉贈票券
            if(buttonIndex == 1) {
                [self checkSendTicket];
            }
            break;
        case 6: //取回票券
            if(buttonIndex == 1) {
                [self getTicketBack];
            }
            break;
    }
    [(CustomIOSAlertView *)alertView close];
}

- (void)checkSendTicket {
    //mobileTextField.text = @"+886988264925";
    //emailTextField.text = @"cillinchen@gmail.com";
    //memoTextField.text = @"none";
    
    NSString * str = friendsField.text;
    if([str isEqual:@""]) {
        NSString *str = [mobileTextField.text stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([str length] == 0) {
            [self showAlertMessage:@"錯誤訊息" message:@"手機號碼欄位必填" tag:0];
            return;
        }
        str = [emailTextField.text stringByTrimmingCharactersInSet:
               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([str length] == 0) {
            [self showAlertMessage:@"錯誤訊息" message:@"電子信箱欄位必填" tag:0];
            return;
        }
        [self showLoadingView:@"資料處理中，請稍候。"];
        [self sendTicketToServer]; //轉贈票券
    } else {
       [self sendTicketToServer]; //轉贈票券
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    NSInteger tag = [sender tag];
    if(tag == 1) {
        CGFloat width = obtainScrollView.frame.size.width;
        NSInteger page = (obtainScrollView.contentOffset.x + (0.5f * width)) / width;
        [pageControl_obtain setCurrentPage:page];
    } else if(tag == 2) {
        CGFloat width = notObtainScrollView.frame.size.width;
        NSInteger page = (notObtainScrollView.contentOffset.x + (0.5f * width)) / width;
        [pageControl_notObtain setCurrentPage:page];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView.tag == 1) {
        CGFloat pageWidth = scrollView.frame.size.width;
        float fractionalPage = scrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        if (previousPage_obtain != page) {
            previousPage_obtain = page;
            obtained_currentViewIdx = previousPage_obtain;
            if([AppDelegate sharedAppDelegate].page_id == 41) { //已取票券
                if(previousPage_obtain >= 1) {   //左邊按鈕
                    [leftTicketButton_obtain setHidden:NO];
                } else if(previousPage_obtain == 0) {
                    [leftTicketButton_obtain setHidden:YES];
                }
                NSInteger ticketCount = [obtained_ticket_Entries count];  //右邊按鈕
                if(ticketCount <= 0) {
                    [leftTicketButton_obtain setHidden:YES];
                    [rightTicketButton_obtain setHidden:YES];
                } else if(previousPage_obtain >= 1 && previousPage_obtain < ticketCount - 1) {
                    [rightTicketButton_obtain setHidden:NO];
                } else if(previousPage_obtain == 0 && ticketCount > 1) {
                    [rightTicketButton_obtain setHidden:NO];
                } else {
                    [rightTicketButton_obtain setHidden:YES];
                }
            }
        }
    } else if(scrollView.tag == 2) {
        CGFloat pageWidth = scrollView.frame.size.width;
        float fractionalPage = scrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        if (previousPage_notObtain != page) {
            previousPage_notObtain = page;
            obtained_currentViewIdx = previousPage_notObtain;
            if([AppDelegate sharedAppDelegate].page_id == 43) { //已取票券
                if(previousPage_notObtain >= 1) {   //左邊按鈕
                    [leftTicketButton_notObtain setHidden:NO];
                } else if(previousPage_notObtain == 0) {
                    [leftTicketButton_notObtain setHidden:YES];
                }
                NSInteger ticketCount = [notObtainYet_ticket_Entries count];  //右邊按鈕
                if(ticketCount <= 0) {
                    [leftTicketButton_notObtain setHidden:YES];
                    [rightTicketButton_notObtain setHidden:YES];
                } else if(previousPage_notObtain >= 1 && previousPage_notObtain < ticketCount - 1) {
                    [rightTicketButton_notObtain setHidden:NO];
                } else if(previousPage_notObtain == 0 && ticketCount > 1) {
                    [rightTicketButton_notObtain setHidden:NO];
                } else {
                    [rightTicketButton_notObtain setHidden:YES];
                }
            }
        }
        
    }
}




- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)clickCheckbox:(id)sender {
    if(checkBox.isChecked == YES) {
        is_batch_seasonticket = YES;
    } else {
        is_batch_seasonticket = NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
