//
//  MyTicketsController.h
//  brothers
//
//  Created by 陳威宇 on 2020/1/13.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantData.h"
#import <WebKit/WebKit.h>
#import "AFNetworking.h"
#import "AESCipher.h"
#import "Tools.h"
#import "MBProgressHUD.h"
#import "CustomIOSAlertView.h"
#import "MessageAlertView.h"
#import "PerformanceObject.h"
#import "TicketObject.h"
#import "TicketListObject.h"
#import "TicketListCell.h"
#import "TicketView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIView+WebCache.h"
#import "WYPopoverController.h"
#import "MICheckBox.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyTicketsController : UIViewController <UIScrollViewDelegate,CustomIOSAlertViewDelegate,UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate> {
    
    UIImageView *navi_logo;
    float           pos_x;
    float           pos_y;
    float           width;
    float           height;
    UIView          *navi_view;
    UIView          *navi_line;
    UIView          *menu_view;
    UIView          *menu_line_1;
    UILabel         *menu_label_1;
    UILabel         *menu_label_2;
    UIView          *menu_underline;
    NSInteger       menu_select;
    UIView          *obtainView;
    UIView          *obtainDeatilView;
    UIScrollView    *obtainScrollView;
    UIView          *notObtainView;
    UIView          *notObtainDetailView;
    UIScrollView    *notObtainScrollView;
    UIPageControl   *pageControl_obtain;
    UIPageControl   *pageControl_notObtain;

    NSTimer         *reloadTimer;
    NSInteger       obtained_index;
    NSInteger       notObtain_index;
    
    NSMutableArray  *obtainedEntries, *notObtainYetEntries;
    MBProgressHUD   *waitingView;

    NSMutableArray  *obtained_ticket_Entries, *notObtainYet_ticket_Entries;
    NSMutableArray  *obtained_viewArray;
    NSMutableArray  *notObtainYet_viewArray;
    NSMutableArray  *ticketList;
    NSMutableArray  *obtainedTicketArr, *notObtainYetTicketArr;
    BOOL            ob_nibsRegistered;
    BOOL            notob_nibsRegistered;
    float           cell_height;
    UIButton        *leftTicketButton_obtain;
    UIButton        *rightTicketButton_obtain;
    UIButton        *leftTicketButton_notObtain;
    UIButton        *rightTicketButton_notObtain;
    NSInteger       obtained_currentViewIdx;
    NSInteger       notObtained_currentViewIdx;
    //NSInteger       viewId;
    UITextField     *mobileTextField;
    UITextField     *emailTextField;
    UITextField     *memoTextField;
    NSString        *sendTicketId;
    NSInteger       currentPage_obtain;
    NSInteger       previousPage_obtain;
    NSInteger       currentPage_notObtain;
    NSInteger       previousPage_notObtain;
    NSInteger       selectIndex_obtain;
    NSInteger       selectIndex_notObtain;
    NSInteger       current_count_obtain;
    NSInteger       current_count_notObtain;
    NSArray         *friends_dict;
    UITextField     *friendsField;
    
    WYPopoverController   *friendPopover;
    UIView                *containerView;
    NSInteger             friend_select;
    BOOL                  is_batch_seasonticket;
    NSString              *is_seasonticket;
    MICheckBox            *checkBox;
}

@property (strong, nonatomic)AFHTTPSessionManager *manager;
@property (strong, nonatomic)UITableView *obtainTb;
@property (strong, nonatomic)UITableView *notObtainTb;


@end

NS_ASSUME_NONNULL_END
