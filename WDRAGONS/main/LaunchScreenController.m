//
//  LaunchScreenController.m
//  BrothersTicket
//
//  Created by 陳威宇 on 2020/2/12.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import "LaunchScreenController.h"

@interface LaunchScreenController ()

@end

@implementation LaunchScreenController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)showImage {
    float pos_x = -1;
    float pos_y = 0;
    float width = self.view.frame.size.width + 1;
    float height = self.view.frame.size.height;
    imgV = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    imgV.image = [UIImage imageNamed:@"welcome(1246x2688)"];
    [self.view addSubview:imgV];

    
}

- (void)viewWillAppear:(BOOL)animated {
    [self showImage];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(gotoMain) userInfo:nil repeats:NO];

}

//離開畫面前
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    timer = nil;
}

- (void)gotoMain {
    [self performSegueWithIdentifier:@"GotoMain" sender:self]; //轉至rwd畫面
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
