//
//  TabBarController.h
//  brothers
//
//  Created by 陳威宇 on 2020/1/9.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOSAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface TabBarController : UITabBarController <CustomIOSAlertViewDelegate> {
    
    UIView      *backgroundView;
    UIButton    *button1;
    UIButton    *button2;
    UIButton    *button3;
    UIButton    *button4;
    UIButton    *button5;
    
    BOOL        firstRun;
}

@end

NS_ASSUME_NONNULL_END
