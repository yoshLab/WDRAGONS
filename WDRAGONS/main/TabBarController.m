//
//  TabBarController.m
//  brothers
//
//  Created by 陳威宇 on 2020/1/9.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import "TabBarController.h"
#import "ConstantData.h"
#import "AppDelegate.h"
#import "Tools.h"
#import "MessageAlertView.h"
 
@interface TabBarController ()

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoHomePage) name:@"GotoHomePage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enable_tab_1) name:@"Enable_TAB_1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disable_tab_1) name:@"Disable_TAB_1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enable_tab_4) name:@"Enable_TAB_4" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disable_tab_4) name:@"Disable_TAB_4" object:nil];
    //[self createTabButton];
       [self setSelectedIndex:1];
    firstRun = YES;

    
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    if(firstRun == YES) {
        firstRun = NO;
        CGFloat bottomPadding = 0;
        if (@available(iOS 11.0, *)) {
            bottomPadding = self.view.safeAreaInsets.bottom;
        }
        float height = bottomPadding + 50;
        float width = [UIScreen mainScreen].bounds.size.width;
        float pos_x = 0;
        float pos_y = 0;
        backgroundView = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        //backgroundView.backgroundColor = [UIColor colorWithRed:(90/255.0) green:(38/255.0) blue:(138/255.0) alpha:1.0f];
        backgroundView.backgroundColor = [UIColor whiteColor];//[UIColor colorWithRed:(0/255.0) green:(38/255.0) blue:(75/255.0) alpha:1.0f];
        [self.tabBar addSubview:backgroundView];
        
        [self createTabButton];
 
    }
    
    
}

//建立tab按鍵
- (void)createTabButton {
    //按鍵寬度
    float btnWidth = (backgroundView.frame.size.width) / 5;
    NSArray *imageArray = [self buttonImage];
    //加入按鍵 1 回上頁
    button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.tag = 1;
    float pos_x = 0;
    float pos_y = 0;
    button1.frame = CGRectMake(pos_x, pos_y, btnWidth, TAB_BAR_HEIGHT);
    [button1 setImage:[[imageArray objectAtIndex:0] objectForKey:@"Default"] forState:UIControlStateNormal];
    [button1 setImage:[[imageArray objectAtIndex:0] objectForKey:@"Highlighted"] forState:UIControlStateHighlighted];
    [button1 setImage:[[imageArray objectAtIndex:0] objectForKey:@"Selected"] forState:UIControlStateSelected];
    [[button1 imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [button1 addTarget:self action:@selector(pressButton1) forControlEvents:UIControlEventTouchUpInside];
    //button1.enabled = NO;
    [backgroundView addSubview:button1];

    //加入按鍵 2 最新消息
    button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.tag = 2;
    pos_x = button1.frame.origin.x + button1.frame.size.width + 0;
    button2.frame = CGRectMake(pos_x, pos_y, btnWidth, TAB_BAR_HEIGHT);
    [button2 setImage:[[imageArray objectAtIndex:1] objectForKey:@"Default"] forState:UIControlStateNormal];
    [button2 setImage:[[imageArray objectAtIndex:1] objectForKey:@"Highlighted"] forState:UIControlStateHighlighted];
    [button2 setImage:[[imageArray objectAtIndex:1] objectForKey:@"Selected"] forState:UIControlStateSelected];
    [[button2 imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [button2 addTarget:self action:@selector(pressButton2) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:button2];
    //加入按鍵 3 賽事列表
    button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    button3.tag = 3;
    pos_x = button2.frame.origin.x + button2.frame.size.width + 0;
    button3.frame = CGRectMake(btnWidth * 2, 0, btnWidth, TAB_BAR_HEIGHT);
    [button3 setImage:[[imageArray objectAtIndex:2] objectForKey:@"Default"] forState:UIControlStateNormal];
    [button3 setImage:[[imageArray objectAtIndex:2] objectForKey:@"Highlighted"] forState:UIControlStateHighlighted];
    [button3 setImage:[[imageArray objectAtIndex:2] objectForKey:@"Selected"] forState:UIControlStateSelected];
    [[button3 imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [button3 addTarget:self action:@selector(pressButton3) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:button3];
   // if([AppDelegate sharedAppDelegate].isLogin == NO)
   //     button3.enabled = NO;
    //加入按鍵 4 我的票券
    button4 = [UIButton buttonWithType:UIButtonTypeCustom];
    button4.tag = 4;
    pos_x = button3.frame.origin.x + button3.frame.size.width + 0;
    button4.frame = CGRectMake(pos_x, 0, btnWidth, TAB_BAR_HEIGHT);
    [button4 setImage:[[imageArray objectAtIndex:3] objectForKey:@"Default"] forState:UIControlStateNormal];
    [button4 setImage:[[imageArray objectAtIndex:3] objectForKey:@"Highlighted"] forState:UIControlStateHighlighted];
    [button4 setImage:[[imageArray objectAtIndex:3] objectForKey:@"Selected"] forState:UIControlStateSelected];
    [[button4 imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [button4 addTarget:self action:@selector(pressButton4) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:button4];
    NSMutableDictionary *dict =  [[Tools getInstance] getUserInfo];
    NSString *isLogin = [dict objectForKey:ISLOGIN];
    if([isLogin isEqualToString:@"Y"]) {
        //button4.enabled = YES;
    } else {
        //button4.enabled = NO;
    }
     //加入按鍵 5 服務說明
     button5 = [UIButton buttonWithType:UIButtonTypeCustom];
     button5.tag = 5;
     pos_x = button4.frame.origin.x + button4.frame.size.width + 0;
     button5.frame = CGRectMake(pos_x, 0, btnWidth, TAB_BAR_HEIGHT);
     [button5 setImage:[[imageArray objectAtIndex:4] objectForKey:@"Default"] forState:UIControlStateNormal];
     [button5 setImage:[[imageArray objectAtIndex:4] objectForKey:@"Highlighted"] forState:UIControlStateHighlighted];
     [button5 setImage:[[imageArray objectAtIndex:4] objectForKey:@"Selected"] forState:UIControlStateSelected];
     [[button5 imageView] setContentMode:UIViewContentModeScaleAspectFit];
     [button5 addTarget:self action:@selector(pressButton5) forControlEvents:UIControlEventTouchUpInside];
     [backgroundView addSubview:button5];
    //[AppDelegate sharedAppDelegate].selectTabRow = 1;
    button3.selected = YES;
    [AppDelegate sharedAppDelegate].page_id = TAB_3_ID;
 

}

- (NSArray *)buttonImage {
    NSMutableDictionary *imgDic1 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic1 setObject:[UIImage imageNamed:@"tab_1_n"]  forKey:@"Default"];
    [imgDic1 setObject:[UIImage imageNamed:@"tab_1_n"] forKey:@"Highlighted"];
    [imgDic1 setObject:[UIImage imageNamed:@"tab_1_g"] forKey:@"Selected"];
    NSMutableDictionary *imgDic2 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic2 setObject:[UIImage imageNamed:@"tab_2_n"]  forKey:@"Default"];
    [imgDic2 setObject:[UIImage imageNamed:@"tab_2_n"] forKey:@"Highlighted"];
    [imgDic2 setObject:[UIImage imageNamed:@"tab_2_g"] forKey:@"Selected"];
    NSMutableDictionary *imgDic3 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic3 setObject:[UIImage imageNamed:@"tab_3_n"]  forKey:@"Default"];
    [imgDic3 setObject:[UIImage imageNamed:@"tab_3_n"] forKey:@"Highlighted"];
    [imgDic3 setObject:[UIImage imageNamed:@"tab_3_g"] forKey:@"Selected"];
    NSMutableDictionary *imgDic4 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic4 setObject:[UIImage imageNamed:@"tab_4_n"]  forKey:@"Default"];
    [imgDic4 setObject:[UIImage imageNamed:@"tab_4_n"] forKey:@"Highlighted"];
    [imgDic4 setObject:[UIImage imageNamed:@"tab_4_g"] forKey:@"Selected"];
    NSMutableDictionary *imgDic5 = [NSMutableDictionary dictionaryWithCapacity:3];
    [imgDic5 setObject:[UIImage imageNamed:@"tab_5_n"]  forKey:@"Default"];
    [imgDic5 setObject:[UIImage imageNamed:@"tab_5_n"] forKey:@"Highlighted"];
    [imgDic5 setObject:[UIImage imageNamed:@"tab_5_g"] forKey:@"Selected"];
    NSArray *imgArr = [NSArray arrayWithObjects:imgDic1,imgDic2,imgDic3,imgDic4,imgDic5,nil];
    return imgArr;
}

- (void)pressButton1 {
    if(button2.isSelected == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoBackWebPage_news" object:nil userInfo:nil];
    } else if(button3.isSelected == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoBackWebPage_homepage" object:nil userInfo:nil];
    } else if(button5.isSelected == YES) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GoBackWebPage_services" object:nil userInfo:nil];
    }  else if(button4.isSelected == YES) {
        if([AppDelegate sharedAppDelegate].page_id == 41) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BackToObtainedListView" object:nil userInfo:nil];
        } else if([AppDelegate sharedAppDelegate].page_id == 43) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BackToNotObtainedListView" object:nil userInfo:nil];
        }
    }
}

- (void)pressButton2 {
    
    usleep(500000);

    if([AppDelegate sharedAppDelegate].is_news_loading == NO) {
         [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshNewsPage" object:nil userInfo:nil];
    }
    [AppDelegate sharedAppDelegate].page_id = TAB_2_ID;
    //[self enable_tab_1];
    [self setSelectedIndex:0];
    button1.selected = NO;
    button2.selected = YES;
    button3.selected = NO;
    button4.selected = NO;
    button5.selected = NO;
}

- (void)pressButton3 {

    usleep(500000);

    if([AppDelegate sharedAppDelegate].error_reload == YES) {
        [AppDelegate sharedAppDelegate].reloadRWD = NO;
        [AppDelegate sharedAppDelegate].error_reload = NO;
    } else {
        [AppDelegate sharedAppDelegate].reloadRWD = YES;
    }

    if([AppDelegate sharedAppDelegate].is_home_loading == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshHomePage" object:nil userInfo:nil];
    }

    [AppDelegate sharedAppDelegate].page_id = TAB_3_ID;
    //[self enable_tab_1];
    [self setSelectedIndex:1];
    button1.selected = NO;
    button2.selected = NO;
    button3.selected = YES;
    button4.selected = NO;
    button5.selected = NO;
}

- (void)pressButton4 {
    
    usleep(500000);

    [AppDelegate sharedAppDelegate].page_id = TAB_4_ID;
   // [self disable_tab_1];
    
    NSMutableDictionary *dict =  [[Tools getInstance] getUserInfo];
    NSString *isLogin = [dict objectForKey:ISLOGIN];
    if([isLogin isEqualToString:@"Y"]) {
        [self setSelectedIndex:2];
        button1.selected = NO;
        button2.selected = NO;
        button3.selected = NO;
        button4.selected = YES;
        button5.selected = NO;
    } else {
        [self showAlertMessage:@"訊息" message:@"請先登入會員！" tag:0];
    }
}

- (void)pressButton5 {
    
    usleep(500000);

    if([AppDelegate sharedAppDelegate].is_services_loading == NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshServicesPage" object:nil userInfo:nil];
    }
    [AppDelegate sharedAppDelegate].page_id = TAB_5_ID;
    //[self enable_tab_1];
    [self setSelectedIndex:3];
    button1.selected = NO;
    button2.selected = NO;
    button3.selected = NO;
    button4.selected = NO;
    button5.selected = YES;
}

- (void)enable_tab_1 {
    button1.enabled = YES;
}

- (void)disable_tab_1 {
    button1.enabled = NO;
}

- (void)enable_tab_4 {
    button4.enabled = YES;
}

- (void)disable_tab_4 {
   // button4.enabled = NO;
}


- (void)showAlertMessage:(NSString *)title message:(NSString *)message tag:(NSInteger)tag {
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setDelegate:self];
    [alertView setTag:tag];
    [[MessageAlertView getInstance] showAlertView:alertView withTitleMessage:title andContentMessage:message buttonsTitleArray:[NSArray arrayWithObjects:@"確定", nil]];
}

#pragma CustomIOS7AlertViewDelegate
- (void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    [(CustomIOSAlertView *)alertView close];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
