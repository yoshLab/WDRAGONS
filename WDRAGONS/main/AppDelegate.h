//
//  AppDelegate.h
//  BrothersTicket
//
//  Created by 陳威宇 on 2020/2/5.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstantData.h"
#import "FMDatabase.h"
#import "SecureUDID.h"
#import "SFHFKeychainUtils.h"
#import "Tools.h"
#import <UserNotifications/UserNotifications.h>
#import <GoogleSignIn/GoogleSignIn.h>

@import Firebase;
@import UserNotifications;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate,GIDSignInDelegate> {
    NSString            *writableDBPath;
}
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *writableDBPath,*rwd_url,*FCM_token,*auth_type,*user_id;
@property (nonatomic) NSInteger page_id;
@property (nonatomic) BOOL is_home_loading,is_news_loading,is_services_loading,gotoMyTickets,isReturnTicket,reloadRWD,error_reload;
+ (AppDelegate *)sharedAppDelegate;
@end

