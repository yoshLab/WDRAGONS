//
//  LaunchScreenController.h
//  BrothersTicket
//
//  Created by 陳威宇 on 2020/2/12.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LaunchScreenController : UIViewController {
    
    NSTimer             *timer;
    UIImageView         *imgV; 
}

@end

NS_ASSUME_NONNULL_END
