//
//  SceneDelegate.h
//  BrothersTicket
//
//  Created by 陳威宇 on 2020/2/5.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

