//
//  NewsController.h
//  brothers
//
//  Created by 陳威宇 on 2020/1/13.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "ConstantData.h"
#import "Tools.h"
#import "MBProgressHUD.h"
#import "AESCipher.h"
#import "AFNetworking.h"
#import "CustomIOSAlertView.h"
#import "MessageAlertView.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsController : UIViewController <WKNavigationDelegate,WKUIDelegate,CustomIOSAlertViewDelegate> {
    float                   pos_x;
    float                   pos_y;
    float                   width;
    float                   height;
    NSTimer                 *timer;
    MBProgressHUD           *waitingView;
    BOOL                    isLoading;
    NSString                *backUrl;
    UIImageView             *errorImageView;
    UILabel                 *errorMessageLabel;
    UIButton                *errorBtn;
}

@property (strong, nonatomic)AFHTTPSessionManager *manager;
@property (strong, nonatomic)WKWebView *homePageWebView;


@end

NS_ASSUME_NONNULL_END
