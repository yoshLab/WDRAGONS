//
//  NewsController.m
//  brothers
//
//  Created by 陳威宇 on 2020/1/13.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import "NewsController.h"

@interface NewsController ()

@end

@implementation NewsController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshNewsPage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshNewsPage) name:@"RefreshNewsPage" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GoBackWebPage_news" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBackWebPage) name:@"GoBackWebPage_news" object:nil];
    self.view.backgroundColor = [UIColor colorWithRed:(248.0f / 255.0f) green:(248.0f / 255.0f) blue:(248.0f / 255.0f) alpha:1];
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        //CGFloat topPadding = window.safeAreaInsets.top;
        CGFloat bottomPadding = window.safeAreaInsets.bottom;
        pos_x = 0;
        pos_y = DEVICE_HEIGHT - bottomPadding;
        width = DEVICE_WIDTH;
        height = bottomPadding;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        view.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:view];
    }
    [self initWebView];
}

- (void)showErrorView {
    [_homePageWebView removeFromSuperview];
    [errorImageView removeFromSuperview];
    [errorMessageLabel removeFromSuperview];
    [errorBtn removeFromSuperview];
    errorBtn = nil;
    errorImageView = nil;
    errorMessageLabel = nil;
    width = DEVICE_WIDTH - 40;
    height = width;
    pos_x = 20;
    pos_y = (DEVICE_HEIGHT - height) / 2;
    
    
    errorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    errorImageView.image = [UIImage imageNamed:@"CF_1"];
    [self.view addSubview:errorImageView];

    pos_x = 0;
    pos_y = errorImageView.frame.origin.y + (errorImageView.frame.size.height / 2) + 40;
    width = DEVICE_WIDTH;
    height = 20;
    errorMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    errorMessageLabel.backgroundColor = [UIColor clearColor];
    errorMessageLabel.font = [UIFont systemFontOfSize:18];
    errorMessageLabel.textAlignment = NSTextAlignmentCenter;
    errorMessageLabel.textColor = [UIColor colorWithRed:(0x33/255.0) green:(0x33/255.0) blue:(0x33/255.0) alpha:1.0f];
    errorMessageLabel.text = @"系統連線異常";
    errorMessageLabel.numberOfLines = 0;
    [self.view addSubview:errorMessageLabel];
    
    pos_x = errorImageView.frame.origin.x;
    pos_y = errorMessageLabel.frame.origin.y + errorMessageLabel.frame.size.height + 10;
    width = errorImageView.frame.size.width;
    height = 20;
    errorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    errorBtn.tag = 1;
    errorBtn.frame = CGRectMake(pos_x, pos_y, width, height);
    [errorBtn addTarget:self action:@selector(pressErrorBtn) forControlEvents:UIControlEventTouchUpInside];
    [errorBtn setTitle:@"回首頁" forState:UIControlStateNormal];
    errorBtn.backgroundColor =  [UIColor clearColor];
    [errorBtn setTitleColor:[UIColor colorWithRed:(0x33/255.0) green:(0x33/255.0) blue:(0x33/255.0) alpha:1.0f] forState:UIControlStateNormal];
    errorBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:errorBtn];

    
}

- (void)pressErrorBtn {
    [errorImageView removeFromSuperview];
    [errorMessageLabel removeFromSuperview];
    [errorBtn removeFromSuperview];
    errorBtn = nil;
    errorImageView = nil;
    errorMessageLabel = nil;
    [self.view addSubview:_homePageWebView];
    [self loadWebView];
}


//畫面出現前
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar invalidateIntrinsicContentSize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_1" object:nil userInfo:nil];
    [errorImageView removeFromSuperview];
    [errorMessageLabel removeFromSuperview];
    [errorBtn removeFromSuperview];
    errorBtn = nil;
    errorImageView = nil;
    errorMessageLabel = nil;
    [_homePageWebView removeFromSuperview];
    [self.view addSubview:_homePageWebView];
    [self loadWebView];
}

//離開畫面前
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_homePageWebView stopLoading];
    [timer invalidate];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
}

- (void)refreshNewsPage {
    [self loadWebView];
    //NSLog(@"refreshHomePage OK");
}

- (void)initWebView {
    isLoading = NO;
    [_homePageWebView removeFromSuperview];
    _homePageWebView = nil;
    pos_x = 0;
    pos_y = 20;
    float width = DEVICE_WIDTH;
    float height = DEVICE_HEIGHT - STATUS_BAR_HEIGHT - TAB_BAR_HEIGHT;
    CGRect rectStatus = [[UIApplication sharedApplication] statusBarFrame];
    if(rectStatus.size.height >= 44) {
        height = DEVICE_HEIGHT - STATUS_BAR_HEIGHT_X - TAB_BAR_HEIGHT - SAFE_AREA;
        pos_y = STATUS_BAR_HEIGHT_X;
    }
    _homePageWebView = [[WKWebView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    if (@available(iOS 11.0, *)) {
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //[_homePageWebView setDataDetectorTypes:UIDataDetectorTypeLink];
    _homePageWebView.tag = 1;
    _homePageWebView.navigationDelegate = self;
    _homePageWebView.UIDelegate = self;
    _homePageWebView.scrollView.bounces = NO;
    _homePageWebView.scrollView.showsVerticalScrollIndicator = NO;
    _homePageWebView.scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_homePageWebView];
}

//顯示正在登入訊息
- (void)showLoadingView:(NSString *)message {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!self->waitingView) {
                self->waitingView = [MBProgressHUD showHUDAddedTo:[AppDelegate sharedAppDelegate].window animated:YES];
                self->waitingView.mode = MBProgressHUDModeIndeterminate;
                self->waitingView.label.text = message;
            } else {
                // self->waitingView.label.text = message;
            }
        });
    });
}

- (void)closeLoadingView {
    [waitingView hideAnimated:YES];
    waitingView = nil;
}

- (void)timeout{
    [AppDelegate sharedAppDelegate].is_news_loading = NO;
    [_homePageWebView stopLoading];
    [self closeLoadingView];
    [timer invalidate];
    timer= nil;
    [self showErrorView];
}

- (void)loadWebView {
    
    
    [AppDelegate sharedAppDelegate].is_news_loading = YES;
    NSString *urlString = NEWS_URL;
    NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                     @"_DV", @"IAMAPP",
                                     @"MENU", @"1",
                                     nil];
    [self webViewWithPost:_homePageWebView urlString:urlString params:webViewParams];
 
}

- (void)webViewWithPost:(WKWebView *)webView urlString:(NSString *)urlString params:(NSMutableArray *)params {
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    [s appendString: [NSString stringWithFormat:@"<html><body onload=\"document.forms[0].submit()\">"
                      "<form method=\"post\" action=\"%@\">", urlString]];
    if([params count] % 2 == 1) { NSLog(@"WebViewWithPost error: params don't seem right"); return; }
    for (int i=0; i < [params count] / 2; i++) {
        [s appendString: [NSString stringWithFormat:@"<input type=\"hidden\" name=\"%@\" value=\"%@\" >\n", [params objectAtIndex:i*2], [params objectAtIndex:(i*2)+1]]];
    }
    [s appendString: @"</input></form></body></html>"];
    //NSLog(@"%@", s);
    [webView loadHTMLString:s baseURL:nil];
    [timer invalidate];
    timer= nil;
    timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timeout) userInfo:nil repeats:NO];
}

- (void)goBackWebPage {
    if([_homePageWebView canGoBack]) {
        [_homePageWebView goBack];
    }
}

#pragma mark - WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    [self showLoadingView:@""];
    [timer invalidate];
    timer= nil;
    timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timeout) userInfo:nil repeats:NO];
    
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
    
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [AppDelegate sharedAppDelegate].is_news_loading = NO;
    [timer invalidate];
    [self closeLoadingView];
    
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    [AppDelegate sharedAppDelegate].is_news_loading = NO;
    //NSLog(@"webView.URL = %@",webView.URL);
    
}
// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    
}
// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    // NSLog(@"%@",navigationResponse.response.URL.absoluteString);
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    //不允许跳转
    //decisionHandler(WKNavigationResponsePolicyCancel);
}
// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    //NSLog(@"%@",navigationAction.request.URL.absoluteString);
    NSString *requestString = [navigationAction.request.URL.absoluteString uppercaseString];
    //不允许跳转
    //decisionHandler(WKNavigationActionPolicyCancel);
    //允许跳转
    
    if([requestString rangeOfString:@"APPACTION?"].location != NSNotFound) {
        [AppDelegate sharedAppDelegate].is_news_loading = NO;
        [webView stopLoading];
        [self closeLoadingView];
        [self parser_cmd:navigationAction.request.URL.absoluteString];
        //不允许跳转
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    
    decisionHandler(WKNavigationActionPolicyAllow);
}
#pragma mark - WKUIDelegate
// 创建一个新的WebView
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    
    
    if (navigationAction.request.URL) {
        NSURL *url = navigationAction.request.URL;
        NSString *urlPath = url.absoluteString;
        if ([urlPath rangeOfString:@"https://"].location != NSNotFound || [urlPath rangeOfString:@"http://"].location != NSNotFound) {
            NSString *str = [urlPath uppercaseString];
            if([str rangeOfString:@".PDF"].location != NSNotFound) {
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
            } else {
                [webView loadRequest:[NSURLRequest requestWithURL:url]];
                
            }
        }
    }
    // --- 解决Crash: reason: 'Returned WKWebView was not created with the given configuration.'
    return nil;
    
    
    
    // return [[WKWebView alloc]init];
}
// 输入框
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * __nullable result))completionHandler{
    completionHandler(@"http");
}
// 确认框
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler{
    completionHandler(YES);
}
// 警告框
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    //NSLog(@"%@",message);
    [self showAlertMessage:@"提示訊息" message:message];
    completionHandler();
}

#pragma CustomIOS7AlertViewDelegate
- (void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSInteger tag = [alertView tag];
    
    [(CustomIOSAlertView *)alertView close];
}

- (void)parser_cmd:(NSString *)cmd_url {
    @try {
        NSArray *firstSplit = [cmd_url componentsSeparatedByString:@"APPACTION?"];
        NSArray *secondSplit = [[firstSplit objectAtIndex:1] componentsSeparatedByString:@"&"];
        secondSplit = [[firstSplit objectAtIndex:1] componentsSeparatedByString:@"OPENBROWSER"];
        if([secondSplit count] >= 2) {
            NSArray *thirdSplit = [[secondSplit objectAtIndex:1] componentsSeparatedByString:@"URL="];
            if([thirdSplit count] >= 2) {
                NSString *urlStr = [thirdSplit objectAtIndex:1];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr] options:@{} completionHandler:nil];
            }
            return;
        }
    } @catch (NSException *exception) {
    }
}

- (void)showAlertMessage:(NSString *)title message:(NSString *)message {
    [self closeLoadingView];
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setDelegate:self];
    [alertView setTag:1];
    [[MessageAlertView getInstance] showAlertView:alertView withTitleMessage:title andContentMessage:message buttonsTitleArray:[NSArray arrayWithObjects:@"確定", nil]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
