//
//  TicketListObject.h
//  CKSCCeTicket
//
//  Created by 陳威宇 on 2014/12/7.
//  Copyright (c) 2014年 陳威宇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TicketListObject : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSData   *image;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *performanceId;
@property (nonatomic, strong) NSString *performanceName;
@property (nonatomic, strong) NSString *performanceNo;
@property (nonatomic, strong) NSString *categoriesName;
@end
