//
//  PerformanceObject.h
//  CKSCCeTicket
//
//  Created by 陳威宇 on 2014/12/8.
//  Copyright (c) 2014年 陳威宇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PerformanceObject : NSObject

//@property (nonatomic, strong) NSString *productId;
@property (nonatomic, strong) NSString *productName;
//@property (nonatomic, strong) NSData   *productImage;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *performanceId;
@property (nonatomic, strong) NSString *performanceName;
@property (nonatomic, strong) NSString *performanceNo;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *placeName;
@property (nonatomic, strong) NSMutableArray *tickets;
@property (nonatomic, strong) NSString *categoriesName;

@end
