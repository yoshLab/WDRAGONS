//
//  TicketListCell.m
//  udn----
//
//  Created by 陳威宇 on 2018/10/16.
//  Copyright © 2018 陳威宇. All rights reserved.
//

#import "TicketListCell.h"

@implementation TicketListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor colorWithRed:(248/255.0) green:(248/255.0) blue:(248/255.0) alpha:1.0f];
    backgrourdView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    backgrourdView.backgroundColor = [UIColor whiteColor];
    
    backgrourdView.layer.cornerRadius = 15.0; // 圓角的弧度
    backgrourdView.layer.masksToBounds = YES;
    backgrourdView.layer.shadowColor = [[UIColor blackColor] CGColor];
    backgrourdView.layer.shadowOffset = CGSizeMake(3.0f, 3.0f); // [水平偏移, 垂直偏移]
    backgrourdView.layer.shadowOpacity = 0.8f; // 0.0 ~ 1.0 的值
    backgrourdView.layer.shadowRadius = 15.0f; // 陰影發散的程度
    [self addSubview:backgrourdView];
    pos_x = 0;
    pos_y = 0;
    cell_height = 0;
    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,cell_width,cell_height)];
    [backgrourdView addSubview:_imgView];
    pos_x = 0;
    pos_y = 0;
    cell_height = 0;
    cell_width = 0;
    programName = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,cell_width,cell_height)];
    programName.backgroundColor = [UIColor clearColor];
    
    if(DEVICE_WIDTH <= 320) {
        programName.font = [UIFont systemFontOfSize:CELL_TITLE_FONT_SIZE - 2];
    } else if(DEVICE_WIDTH > 320 && DEVICE_WIDTH <= 375) {
        programName.font = [UIFont systemFontOfSize:CELL_TITLE_FONT_SIZE - 0];
    } else {
        programName.font = [UIFont systemFontOfSize:CELL_TITLE_FONT_SIZE + 2];
    }
    programName.textAlignment = NSTextAlignmentCenter;
    programName.textColor = [UIColor colorWithRed:(0x33/255.0) green:(0x33/255.0) blue:(0x33/255.0) alpha:1.0f];
    programName.numberOfLines = 0;
    [backgrourdView addSubview:programName];
    
    programDate = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,cell_width,cell_height)];
    programDate.backgroundColor = [UIColor clearColor];
    if(DEVICE_WIDTH <= 320) {
         programDate.font = [UIFont systemFontOfSize:CELL_TITLE_FONT_SIZE - 4];
    } else if(DEVICE_WIDTH > 320 && DEVICE_WIDTH <= 375) {
         programDate.font = [UIFont systemFontOfSize:CELL_TITLE_FONT_SIZE - 0];
    } else {
         programDate.font = [UIFont systemFontOfSize:CELL_TITLE_FONT_SIZE + 2];
    }
    programDate.textAlignment = NSTextAlignmentCenter;
    programDate.textColor = [UIColor colorWithRed:(0x99/255.0) green:(0x00/255.0) blue:(0x00/255.0) alpha:1.0f];
    programDate.numberOfLines = 0;
    [backgrourdView addSubview:programDate];
    
    pos_x = 0;
    pos_y = 0;
    NSInteger number_width = 0;
    NSInteger number_height = 0;
    ticketNumber = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,number_width,number_height)];
    ticketNumber.backgroundColor = [UIColor whiteColor];
    if(DEVICE_WIDTH <= 320) {
        ticketNumber.font = [UIFont systemFontOfSize:TICKET_NUMBER_FONT_SIZE - 2];
    } else if(DEVICE_WIDTH > 320 && DEVICE_WIDTH <= 375) {
        ticketNumber.font = [UIFont systemFontOfSize:TICKET_NUMBER_FONT_SIZE + 0];
    } else {
        ticketNumber.font = [UIFont systemFontOfSize:TICKET_NUMBER_FONT_SIZE + 2];
    }
    ticketNumber.textAlignment = NSTextAlignmentCenter;
    ticketNumber.textColor = [UIColor colorWithRed:(99.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0f];
    ticketNumber.numberOfLines = 0;
    [backgrourdView addSubview:ticketNumber];

    
/*
    barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    barView.backgroundColor = [UIColor clearColor];
    [backgrourdView addSubview:barView];
    pos_x = 0;
    pos_y = 0;
    NSInteger number_width = 0;
    NSInteger number_height = 0;
    ticketNumber = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,number_width,number_height)];
    ticketNumber.backgroundColor = [UIColor whiteColor];
    
    if(DEVICE_WIDTH <= 320) {
        ticketNumber.font = [UIFont systemFontOfSize:TICKET_NUMBER_FONT_SIZE - 2];
    } else if(DEVICE_WIDTH > 320 && DEVICE_WIDTH <= 375) {
        ticketNumber.font = [UIFont systemFontOfSize:TICKET_NUMBER_FONT_SIZE + 0];
    } else {
        ticketNumber.font = [UIFont systemFontOfSize:TICKET_NUMBER_FONT_SIZE + 2];
    }
    ticketNumber.textAlignment = NSTextAlignmentCenter;
    ticketNumber.textColor = [UIColor colorWithRed:(99.0/255.0) green:(0.0/255.0) blue:(0.0/255.0) alpha:1.0f];
    ticketNumber.numberOfLines = 0;
    [barView addSubview:ticketNumber];
*/
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*
- (void)setImage:(UIImage *)image {
    imgView.image = [image copy];
}
*/

- (void)setName:(NSString *)text {
   programName.text = [text copy];
    [programName setText:[text copy] lineSpacing:CELL_TEXT_LINE_SPACE];
}

- (void)setDate:(NSString *)text {
    [programDate setText:[text copy] lineSpacing:CELL_TEXT_LINE_SPACE];
}

- (void)setNumber:(NSString *)text {
    NSString *noticeStr = [NSString stringWithFormat:@"共  %@  張",[text copy]];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:noticeStr];
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor colorWithRed:(0x99/255.0) green:(0x0/255.0) blue:(0x0/255.0) alpha:1.0f]
                          range:[[attributedStr string] rangeOfString:@"共"]];
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor colorWithRed:(0xff/255.0) green:(0x0/255.0) blue:(0x0/255.0) alpha:1.0f]
                          range:[[attributedStr string] rangeOfString:[text copy]]];
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor colorWithRed:(0x99/255.0) green:(0x0/255.0) blue:(0x0/255.0) alpha:1.0f]
                          range:[[attributedStr string] rangeOfString:@"張"]];
    
    [ticketNumber setAttributedText:attributedStr];

    
    
}

- (void)setHeight:(float)cell_height {
    //背景
    CGRect frame = backgrourdView.frame;
    frame.size.height = cell_height - CELL_SPACING;
    backgrourdView.frame = frame;
    //節目圖檔
    frame = _imgView.frame;
    frame.size.width = cell_height * 0.6;
    frame.size.height = frame.size.width * 0.72;
    frame.origin.x = ((DEVICE_WIDTH - 40) - frame.size.width) / 2;
    frame.origin.y = 10;
    _imgView.frame = frame;
    //節目名稱
    frame = programName.frame;
    frame.origin.x = 20;
    frame.origin.y = _imgView.frame.origin.y + _imgView.frame.size.height + 5;
    frame.size.width = DEVICE_WIDTH - 80;
    
    if(DEVICE_WIDTH <= 320) {
        frame.size.height = [[Tools getInstance] text:programName.text heightWithFontSize:CELL_TITLE_FONT_SIZE-2 width:frame.size.width lineSpacing:CELL_TEXT_LINE_SPACE];
   } else if(DEVICE_WIDTH > 320 && DEVICE_WIDTH <= 375) {
       frame.size.height = [[Tools getInstance] text:programName.text heightWithFontSize:CELL_TITLE_FONT_SIZE-0 width:frame.size.width lineSpacing:CELL_TEXT_LINE_SPACE];
    } else {
        frame.size.height = [[Tools getInstance] text:programName.text heightWithFontSize:CELL_TITLE_FONT_SIZE+2 width:frame.size.width lineSpacing:CELL_TEXT_LINE_SPACE];
    }
    programName.frame = frame;
    //日期
    frame = programDate.frame;
    frame.origin.x = 0;
    frame.origin.y = programName.frame.origin.y + programName.frame.size.height;
    frame.size.width = (DEVICE_WIDTH - 40);
    
    if(DEVICE_WIDTH <= 320) {
        frame.size.height = [[Tools getInstance] text:programDate.text heightWithFontSize:CELL_DATE_FONT_SIZE - 4 width:frame.size.width lineSpacing:CELL_TEXT_LINE_SPACE];
   } else if(DEVICE_WIDTH > 320 && DEVICE_WIDTH <= 375) {
        frame.size.height = [[Tools getInstance] text:programDate.text heightWithFontSize:CELL_DATE_FONT_SIZE + 0 width:frame.size.width lineSpacing:CELL_TEXT_LINE_SPACE];

    } else {
        frame.size.height = [[Tools getInstance] text:programDate.text heightWithFontSize:CELL_DATE_FONT_SIZE + 2 width:frame.size.width lineSpacing:CELL_TEXT_LINE_SPACE];
    }
    programDate.frame = frame;
    //張數
    frame = ticketNumber.frame;
    frame.size.width = programDate.frame.size.width;
    frame.size.height = 25;
    frame.origin.x = programDate.frame.origin.x;
    frame.origin.y = programDate.frame.origin.y + programDate.frame.size.height + 10;
    ticketNumber.frame = frame;
    
    
    
    
/*
    frame = barView.frame;
    frame.size.width = DEVICE_WIDTH - 100;
    frame.origin.x = 30;
    
    if(DEVICE_WIDTH <= 320) {
        frame.size.height = 17;
        frame.origin.y = cell_height - frame.size.height - 20;

    } else if(DEVICE_WIDTH > 320 && DEVICE_WIDTH <= 375) {
        frame.size.height = 21;
       frame.origin.y = cell_height - frame.size.height - 25;
    } else {
        frame.size.height = 25;
        frame.origin.y = cell_height - frame.size.height - 30;
    }

    
    
    
    barView.frame = frame;
    frame = ticketNumber.frame;
    frame.size.width = barView.frame.size.width / 3;
    frame.size.height = barView.frame.size.height;
    frame.origin.x = (barView.frame.size.width - frame.size.width) / 2;
    frame.origin.y = 0;
    ticketNumber.frame = frame;
*/
}

- (void)setWidth:(float)cell_width {
    CGRect frame = backgrourdView.frame;
    frame.size.width = cell_width;
    backgrourdView.frame = frame;
    
    
    
/*
    frame = programName.frame;
    frame.origin.x = imgView.frame.origin.x + imgView.frame.size.width + CELL_SPACING;
    frame.origin.y = imgView.frame.origin.y;
    frame.size.width = backgrourdView.frame.size.width - frame.origin.x - CELL_SPACING;
    frame.size.height = [[Tools getInstance] text:programName.text heightWithFontSize:CELL_TITLE_FONT_SIZE-1 width:frame.size.width lineSpacing:CELL_TEXT_LINE_SPACE];
    programName.frame = frame;
    frame = programDate.frame;
    frame.origin.x = programName.frame.origin.x;
    frame.origin.y = programName.frame.origin.y + programName.frame.size.height + (CELL_SPACING / 2) - 4;
    frame.size.width = programName.frame.size.width;
    frame.size.height = [[Tools getInstance] text:programDate.text heightWithFontSize:CELL_DATE_FONT_SIZE-1 width:frame.size.width lineSpacing:CELL_TEXT_LINE_SPACE];
    programDate.frame = frame;

    frame = ticketNumber.frame;
    frame.origin.x = backgrourdView.frame.size.width - TICKET_NUMBER_WIDTH - CELL_SPACING;
    frame.origin.y = backgrourdView.frame.size.height - TICKET_NUMBER_WIDTH - CELL_SPACING;
    frame.size.height = TICKET_NUMBER_HEIGHT;
    frame.size.width = TICKET_NUMBER_WIDTH;
    ticketNumber.frame = frame;
    ticketNumber.layer.cornerRadius = ticketNumber.frame.size.width / 2;
    ticketNumber.clipsToBounds = YES;
*/
}

@end
