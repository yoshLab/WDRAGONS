//
//  TicketListCell.h
//  udn----
//
//  Created by 陳威宇 on 2018/10/16.
//  Copyright © 2018 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tools.h"
#import "UILabel+Utils.h"
#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface TicketListCell : UITableViewCell {
//    UIImageView        *imgView;
    UIView             *backgrourdView;
    UILabel            *programName;
    UILabel            *programDate;
    UILabel            *ticketNumber;
    UIView             *barView;
    float              pos_x; 
    float              pos_y;
    float              cell_height;
    float              cell_width;
}

//@property (copy, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImageView *imgView;
@property (copy, nonatomic) NSString *name,*date,*number;
@property (nonatomic) float height,width;

@end

NS_ASSUME_NONNULL_END
