//
//  TicketView.h
//  udn----
//
//  Created by 陳威宇 on 2018/10/19.
//  Copyright © 2018 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tools.h"
#import "UIImage+MDQRCode.h"

NS_ASSUME_NONNULL_BEGIN

@interface TicketView : UIView {

    UIView          *ticket_background_view;
    UIImageView     *ticket_view;
    UILabel         *orderTimeLabel;
    UILabel         *productNameLabel;
    UILabel         *showTimeLabel;
    UILabel         *placeLabel;
    UILabel         *orderNumberLabel;
    UILabel         *getMethodLabel;
    UILabel         *orderSeatLabel;
    UILabel         *payMethodLabel;
    UILabel         *verifyNoLabel;
    UILabel         *virtualNoLabel;
    float           pos_x;
    float           pos_y;
    float           width;
    float           height;
    UIImageView     *qrCodeView;
    UILabel         *priceLabel;
    UILabel         *priceTypeLabel;
    UILabel         *taxNumberLabel;
    UILabel         *taxInfoLabel;
    NSString        *updateView;
    UIButton        *returnTicketBtn;    //退票
    UIButton        *sendTicketBtn;      //轉贈
    UIButton        *getBackTicketBtn;   //取回
    UIButton        *goBackTicketBtn;    //退回
    UIButton        *acceptTicketBtn;    //接受
    UILabel         *senderNameLabel;
    UILabel         *senderMessageLabel;
    UILabel         *senderRemarkLabel;
    UILabel         *admissionStatusLabel;
    UILabel         *remarkLabel;
    UILabel         *serialNoLabel;
    UILabel         *itemLabel_1;
    UILabel         *itemLabel_2;
    UILabel         *statusLabel;
    UIView          *sendMsgView;
    UILabel         *total_amountLabel;
    UILabel         *msg1Label;
    UILabel         *msg2Label;
    UILabel         *msg3Label;
}


@property (copy, nonatomic)NSString  *performanceNo;
@property (copy, nonatomic)NSString  *orderNumber;
@property (copy, nonatomic)NSString  *ticketId;
@property (copy, nonatomic)NSString  *orderTime;
@property (copy, nonatomic)NSString  *productName;
@property (copy, nonatomic)NSString  *showTime;
@property (copy, nonatomic)NSString  *place;
@property (copy, nonatomic)NSString  *qrcode;
@property (copy, nonatomic)NSString  *price;
@property (copy, nonatomic)NSString  *priceType;
@property (copy, nonatomic)NSString  *taxInfo;
@property (copy, nonatomic)NSString  *taxNumber;
@property (copy, nonatomic)NSString  *updateTicketView;
@property (copy, nonatomic)NSString  *ticketType; //R:實體票券。E:電子票券 V:虛擬票券
@property (copy, nonatomic)NSString  *isObtained; //1:已取票券。 2:未取票券
@property (copy, nonatomic)NSString  *transferFlag; //Y:轉贈的票券
@property (copy, nonatomic)NSString  *admissionStatus; //票券是否使用
@property (copy, nonatomic)NSString  *orderSeat;
@property (copy, nonatomic)NSString  *senderName;
@property (copy, nonatomic)NSString  *senderMessage;
@property (copy, nonatomic)NSString  *getMethod;
@property (copy, nonatomic)NSString  *payMethod;
@property (copy, nonatomic)NSString  *verifyNo;
@property (copy, nonatomic)NSString  *virtualNo;
@property (copy, nonatomic)NSString  *updateQrcode;
@property (copy, nonatomic)NSString  *buyBySelf;
@property (copy, nonatomic)NSString  *remark;           //注意事項
@property (copy, nonatomic)NSString  *getMethodCode;    //取票方式代碼
@property (copy, nonatomic)NSString  *sendMailNumber;   //郵寄號碼
@property (copy, nonatomic)NSString  *sendMailDate;     //郵寄日期
@property (copy, nonatomic)NSString  *printDate;        //列印日期
@property (copy, nonatomic)NSString  *serialNo;         //自由入座序號
@property (copy, nonatomic)NSString  *receiveName;      //接收者姓名
//goods
@property (copy, nonatomic)NSString  *isGoods;
@property (copy, nonatomic)NSString  *notice_msg3;
@property (copy, nonatomic)NSString  *total_amount;
@property (copy, nonatomic)NSString  *get_method_no;
@property (copy, nonatomic)NSString  *goodsItem;
@property (copy, nonatomic)NSString  *notice_msg;
@property (copy, nonatomic)NSString  *notice_msg2;

@property (copy, nonatomic)NSString  *ticket_format;
@property (copy, nonatomic)NSString  *is_seasonticket;

@end
NS_ASSUME_NONNULL_END
