//
//  TicketObject.h
//  CKSCCeTicket
//
//  Created by 陳威宇 on 2014/12/8.
//  Copyright (c) 2014年 陳威宇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TicketObject : NSObject

@property (nonatomic, strong) NSString *isGoods;
@property (nonatomic, strong) NSString *ticketId;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *orderTime;
@property (nonatomic, strong) NSString *orderNumber;
@property (nonatomic, strong) NSString *orderSeat;
@property (nonatomic, strong) NSString *priceType;
@property (nonatomic, strong) NSString *orderPrice;
@property (nonatomic, strong) NSString *senderName;
@property (nonatomic, strong) NSString *senderMessage;
@property (nonatomic, strong) NSString *statusName;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *downloadTime;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *placeName;
@property (nonatomic, strong) NSString *getMethod;
@property (nonatomic, strong) NSString *payMethod;
@property (nonatomic, strong) NSString *ticketType;
@property (nonatomic, strong) NSString *transferFlag;
@property (nonatomic, strong) NSString *performanceNo;
@property (nonatomic, strong) NSString *performanceName;
@property (nonatomic, strong) NSString *buyBySelf;
@property (nonatomic, strong) NSString *categoriesName;
@property (nonatomic, strong) NSString *verifyNo;
@property (nonatomic, strong) NSString *virtualNo;
@property (nonatomic, strong) NSString *admissionStatus;
@property (nonatomic, strong) NSString *tax_number;
@property (nonatomic, strong) NSString *tax_info;
@property (nonatomic, strong) NSString *get_method_no;
@property (nonatomic, strong) NSString *notice_msg;
@property (nonatomic, strong) NSString *notice_msg2;
@property (nonatomic, strong) NSString *notice_msg3;
@property (nonatomic, strong) NSString *mail_sn;
@property (nonatomic, strong) NSString *RockNo;
@property (nonatomic, strong) NSString *print_date;
@property (nonatomic, strong) NSString *mail_date;
@property (nonatomic, strong) NSString *receiver_name;
@property (nonatomic, strong) NSString *goods_name;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *quanity;
@property (nonatomic, strong) NSString *total_amount;
@property (nonatomic, strong) NSString *goodsItem;
@property (nonatomic, strong) NSString *ticket_format;
@property (nonatomic, strong) NSString *is_seasonticket;
@end
