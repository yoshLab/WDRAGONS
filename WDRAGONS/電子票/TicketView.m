//
//  TicketView.m
//  udn----
//
//  Created by 陳威宇 on 2018/10/19.
//  Copyright © 2018 陳威宇. All rights reserved.
//

#import "TicketView.h"

@implementation TicketView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // load view frame XIB
        [self initView:frame];
    }
    return self;
}

- (void)initView:(CGRect)frame {
    
    if(!ticket_background_view) {
        pos_x = TICKET_SPACING;
        pos_y = 0;
        width = frame.size.width - (TICKET_SPACING * 2);
        height = frame.size.height;
        ticket_background_view = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
       
        //ticket_background_view.layer.borderColor = [UIColor redColor].CGColor;
        //ticket_background_view.layer.borderWidth = 3.0f;

        ticket_background_view.backgroundColor = [UIColor whiteColor];
        ticket_background_view.layer.cornerRadius = 15.0; // 圓角的弧度
        ticket_background_view.layer.masksToBounds = YES;
        ticket_background_view.layer.shadowColor = [[UIColor blackColor] CGColor];
        ticket_background_view.layer.shadowOffset = CGSizeMake(3.0f, 3.0f); // [水平偏移, 垂直偏移]
        ticket_background_view.layer.shadowOpacity = 0.8f; // 0.0 ~ 1.0 的值
        ticket_background_view.layer.shadowRadius = 15.0f; // 陰影發散的程度
        [self addSubview:ticket_background_view];
    }
}

- (void)setUpdateTicketView:(NSString *)updateTicketView {
    //先移除舊有的view
    for(UIView *view in self.subviews){
        [view removeFromSuperview];
    }
    pos_x = TICKET_SPACING;
    pos_y = 0;
    width = self.frame.size.width - (TICKET_SPACING * 2);
    height = self.frame.size.height;
    ticket_background_view = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    
    //ticket_background_view.layer.borderColor = [UIColor redColor].CGColor;
    //ticket_background_view.layer.borderWidth = 6.0f;

    ticket_background_view.backgroundColor = [UIColor whiteColor];
    ticket_background_view.layer.cornerRadius = 15.0; // 圓角的弧度
    ticket_background_view.layer.masksToBounds = YES;
    ticket_background_view.layer.shadowColor = [[UIColor blackColor] CGColor];
    ticket_background_view.layer.shadowOffset = CGSizeMake(3.0f, 3.0f); // [水平偏移, 垂直偏移]
    ticket_background_view.layer.shadowOpacity = 0.8f; // 0.0 ~ 1.0 的值
    ticket_background_view.layer.shadowRadius = 15.0f; // 陰影發散的程度
    [self addSubview:ticket_background_view];
    if([_isGoods isEqualToString:@"N"]) {
        if ([_isObtained isEqualToString:@"Y"]) { // 已取票券
            if ([_ticketType isEqualToString:@"R"])
                [self createPhysicalTicket_Obtained]; //紙本票券
            else if ([_ticketType isEqualToString:@"E"]) {
                if ([_transferFlag isEqualToString:@"Y"])
                    [self createObtainedETicketTransfer]; //轉贈的票券
                else
                    [self createObtainedETicket]; //電子票
            } else if ([_ticketType isEqualToString:@"V"])
                [self createVirtualTicket]; //虛擬票券
        } else if ([_isObtained isEqualToString:@"N"]) { // 未取票券
            if ([_ticketType isEqualToString:@"R"])
                [self createPhysicalTicket_NotObtained];
            else if ([_ticketType isEqualToString:@"E"])
                [self createNotObtainedYetETicket];
        }
    } else {
        [self createGoodsETicket];
    }
}

//商品
- (void)createGoodsETicket {
    
    NSInteger font_size = 16;
    NSInteger space;
    pos_x = TICKET_SPACING;
    pos_y = 20;
    width = ticket_background_view.frame.size.width - (CELL_SPACING * 2);
    height = [[Tools getInstance] text:_productName heightWithFontSize:font_size width:width lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [productNameLabel setText:_productName lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel.font = [UIFont boldSystemFontOfSize:font_size];
    productNameLabel.textAlignment = NSTextAlignmentLeft;
    productNameLabel.numberOfLines = 0;
    [ticket_background_view addSubview:productNameLabel];
    productNameLabel.textColor = [UIColor blackColor];
    [productNameLabel setBackgroundColor:[UIColor clearColor]];
    [ticket_background_view addSubview:productNameLabel];
    //ticket type 2.24
    if([_ticket_format isEqualToString:@"blue"]) {
        height = (ticket_background_view.frame.size.width * 2) / 6 + 36;
        width = height * 2.24;
        pos_x = (ticket_background_view.frame.size.width - width) / 2;
        pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING;
        ticket_view = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        ticket_view.image = [UIImage imageNamed:@"ticket_1"];
        [ticket_background_view addSubview:ticket_view];
    } else if([_ticket_format isEqualToString:@"yellow"]) {
        height = (ticket_background_view.frame.size.width * 2) / 6 + 36;
        width = height * 2.24;
        pos_x = (ticket_background_view.frame.size.width - width) / 2;
        pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING;
        ticket_view = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        ticket_view.image = [UIImage imageNamed:@"ticket_yellow"];
        [ticket_background_view addSubview:ticket_view];
    } else  if([_ticket_format isEqualToString:@"green"]) {
        height = (ticket_background_view.frame.size.width * 2) / 6 + 36;
        width = height * 2.24;
        pos_x = (ticket_background_view.frame.size.width - width) / 2;
        pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING;
        ticket_view = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        ticket_view.image = [UIImage imageNamed:@"ticket_3"];
        [ticket_background_view addSubview:ticket_view];
    } else  if([_ticket_format isEqualToString:@"red"]) {
        height = (ticket_background_view.frame.size.width * 2) / 6 + 36;
        width = height * 2.24;
        pos_x = (ticket_background_view.frame.size.width - width) / 2;
        pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING;
        ticket_view = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        ticket_view.image = [UIImage imageNamed:@"ticket_red"];
        [ticket_background_view addSubview:ticket_view];
    }

    //qr code
    NSInteger qwidth = (ticket_background_view.frame.size.width * 2) / 6;
    NSInteger qheight = qwidth;
    pos_x = (ticket_background_view.frame.size.width - qwidth) / 2;
    pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING + 20;
    qrCodeView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, qwidth, qheight)];
    if([_admissionStatus isEqualToString:@"1"]) { //已使用
        qrCodeView.image = [UIImage mdQRCodeForString:@"已使用"
                                                 size:qwidth
                                            fillColor:[UIColor lightGrayColor]];
        width = qrCodeView.frame.size.width;
        height = 50.0f;
        pos_x = 0;
        pos_y = (qrCodeView.frame.size.height - height) / 2;
        admissionStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        admissionStatusLabel.backgroundColor = [UIColor clearColor];
        [admissionStatusLabel setText:[NSString stringWithFormat:@"已使用\n%@",_ticketId] lineSpacing:CELL_TEXT_LINE_SPACE];
        admissionStatusLabel.font = [UIFont systemFontOfSize:TICKET_NORMAL_FONT_SIZE + 4];
        admissionStatusLabel.textAlignment = NSTextAlignmentCenter;
        admissionStatusLabel.textColor = [UIColor colorWithRed:(0xA0/255.0) green:(0x40/255.0) blue:(0x40/255.0) alpha:1];
        admissionStatusLabel.backgroundColor = [UIColor whiteColor];
        admissionStatusLabel.numberOfLines = 0;
        [qrCodeView addSubview:admissionStatusLabel];
    } else { //未使用
        qrCodeView.image = [UIImage mdQRCodeForString:[[Tools getInstance] getQRCodeStringWithPerformanceNo:_performanceNo andTicketID:_ticketId]
                                                 size:qwidth
                                            fillColor:[UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1]];
    }
    [ticket_background_view addSubview:qrCodeView];
    //購買日期
    font_size = 15;
    space = 4;
    NSString *str = [NSString stringWithFormat:@"購買日期： %@",_orderTime];
    pos_x = 60;
    pos_y = qrCodeView.frame.origin.y + qrCodeView.frame.size.height + 30;
    width = ticket_background_view.frame.size.width - 120;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderTimeLabel.backgroundColor = [UIColor clearColor];
    [orderTimeLabel setText:str lineSpacing:space];
    orderTimeLabel.font = [UIFont systemFontOfSize:font_size];
    orderTimeLabel.textAlignment = NSTextAlignmentLeft;
    orderTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderTimeLabel];
    //訂單編號
    str = [NSString stringWithFormat:@"訂單編號： %@",_orderNumber];
    pos_x = orderTimeLabel.frame.origin.x;
    pos_y = orderTimeLabel.frame.origin.y + orderTimeLabel.frame.size.height;
    width = orderTimeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderNumberLabel.backgroundColor = [UIColor clearColor];
    [orderNumberLabel setText:str lineSpacing:space];
    orderNumberLabel.font = [UIFont systemFontOfSize:font_size];
    orderNumberLabel.textAlignment = NSTextAlignmentLeft;
    orderNumberLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderNumberLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderNumberLabel];
    //球賽日期
    str = [NSString stringWithFormat:@"取貨日期： %@",_showTime];
    pos_x = orderNumberLabel.frame.origin.x;
    pos_y = orderNumberLabel.frame.origin.y + orderNumberLabel.frame.size.height;
    width = orderNumberLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    showTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    showTimeLabel.backgroundColor = [UIColor clearColor];
    [showTimeLabel setText:str lineSpacing:space];
    showTimeLabel.font = [UIFont systemFontOfSize:font_size];
    showTimeLabel.textAlignment = NSTextAlignmentLeft;
    showTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    showTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:showTimeLabel];
    //活動地點
    str = [NSString stringWithFormat:@"活動地點： %@",_place];
    pos_x = showTimeLabel.frame.origin.x;
    pos_y = showTimeLabel.frame.origin.y + showTimeLabel.frame.size.height;
    width = showTimeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    placeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    placeLabel.backgroundColor = [UIColor clearColor];
    [placeLabel setText:str lineSpacing:space];
    placeLabel.font = [UIFont systemFontOfSize:font_size];
    placeLabel.textAlignment = NSTextAlignmentLeft;
    placeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    placeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:placeLabel];
    //
    pos_y = placeLabel.frame.origin.y + placeLabel.frame.size.height + 15;
    //NSString to NSDictionary
    NSError *err = nil;
    NSArray *items =
     [NSJSONSerialization JSONObjectWithData:[_goodsItem dataUsingEncoding:NSUTF8StringEncoding]
                                     options:NSJSONReadingMutableContainers
                                       error:&err];
    for(NSInteger cnt=0;cnt<items.count;cnt++) {
        NSDictionary *dict = [items objectAtIndex:cnt];
        NSString *name = [dict objectForKey:@"name"];
        NSString *quanity = [dict objectForKey:@"quanity"];
        NSString *description = [dict objectForKey:@"description"];
        pos_x = TICKET_SPACING;
        width = ((ticket_background_view.frame.size.width - (CELL_SPACING * 2)) * 3) / 5;
        height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        nameLabel.backgroundColor = [UIColor clearColor];
        [nameLabel setText:name lineSpacing:space];
        nameLabel.font = [UIFont boldSystemFontOfSize:font_size];
        nameLabel.textAlignment = NSTextAlignmentLeft;
        nameLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        nameLabel.numberOfLines = 0;
        //nameLabel.backgroundColor = [UIColor redColor];
        [ticket_background_view addSubview:nameLabel];
        pos_x = nameLabel.frame.origin.x + nameLabel.frame.size.width;
        width = ((ticket_background_view.frame.size.width - (CELL_SPACING * 2)) * 1) / 5;
        height = nameLabel.frame.size.height;
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        [descriptionLabel setText:description lineSpacing:space];
        descriptionLabel.font = [UIFont boldSystemFontOfSize:font_size];
        descriptionLabel.textAlignment = NSTextAlignmentCenter;
        descriptionLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        descriptionLabel.numberOfLines = 0;
        //descriptionLabel.backgroundColor = [UIColor yellowColor];
        [ticket_background_view addSubview:descriptionLabel];
        pos_x = descriptionLabel.frame.origin.x + descriptionLabel.frame.size.width;
        width = descriptionLabel.frame.size.width;
        height = descriptionLabel.frame.size.height;
        NSString *str = [NSString stringWithFormat:@"數量:%@",quanity];
        UILabel *quanityLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        quanityLabel.backgroundColor = [UIColor clearColor];
        [quanityLabel setText:str lineSpacing:space];
        quanityLabel.font = [UIFont boldSystemFontOfSize:font_size];
        quanityLabel.textAlignment = NSTextAlignmentCenter;
        quanityLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        quanityLabel.numberOfLines = 0;
        //quanityLabel.backgroundColor = [UIColor yellowColor];
        [ticket_background_view addSubview:quanityLabel];
        pos_y = nameLabel.frame.origin.y + nameLabel.frame.size.height + 5;
    }
    
    str = [NSString stringWithFormat:@"合計:%@",_total_amount];
    pos_x = TICKET_SPACING;
    pos_y = pos_y + 10;
    width = ticket_background_view.frame.size.width - (CELL_SPACING * 2);
    height = [[Tools getInstance] text:str heightWithFontSize:font_size+4 width:width lineSpacing:space];
    total_amountLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    total_amountLabel.backgroundColor = [UIColor clearColor];
    [total_amountLabel setText:str lineSpacing:space];
    total_amountLabel.font = [UIFont boldSystemFontOfSize:font_size+4];
    total_amountLabel.textAlignment = NSTextAlignmentRight;
    total_amountLabel.textColor = [UIColor orangeColor];
    total_amountLabel.numberOfLines = 0;
    //total_amountLabel.backgroundColor = [UIColor redColor];
    [ticket_background_view addSubview:total_amountLabel];

    pos_x = total_amountLabel.frame.origin.x;
    pos_y = total_amountLabel.frame.origin.y + total_amountLabel.frame.size.height + 15;
    width = total_amountLabel.frame.size.width;
    if(_notice_msg.length > 0) {
        height = [[Tools getInstance] text:_notice_msg heightWithFontSize:font_size width:width lineSpacing:space];
        msg1Label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        msg1Label.backgroundColor = [UIColor clearColor];
        [msg1Label setText:_notice_msg lineSpacing:space];
        msg1Label.font = [UIFont boldSystemFontOfSize:font_size];
        msg1Label.textAlignment = NSTextAlignmentLeft;
        msg1Label.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        msg1Label.numberOfLines = 0;
        //msg1Label.backgroundColor = [UIColor redColor];
        [ticket_background_view addSubview:msg1Label];
        pos_y = msg1Label.frame.origin.y + msg1Label.frame.size.height + 5;
    }
    if(_notice_msg2.length > 0) {
        height = [[Tools getInstance] text:_notice_msg2 heightWithFontSize:font_size width:width lineSpacing:space];
        msg2Label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        msg2Label.backgroundColor = [UIColor clearColor];
        [msg2Label setText:_notice_msg2 lineSpacing:space];
        msg2Label.font = [UIFont boldSystemFontOfSize:font_size];
        msg2Label.textAlignment = NSTextAlignmentLeft;
        msg2Label.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        msg2Label.numberOfLines = 0;
        //msg2Label.backgroundColor = [UIColor redColor];
        [ticket_background_view addSubview:msg2Label];
        pos_y = msg2Label.frame.origin.y + msg2Label.frame.size.height + 5;
    }
    if(_notice_msg3.length > 0) {
        height = [[Tools getInstance] text:_notice_msg3 heightWithFontSize:font_size width:width lineSpacing:space];
        msg3Label = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        msg3Label.backgroundColor = [UIColor clearColor];
        [msg3Label setText:_notice_msg3 lineSpacing:space];
        msg3Label.font = [UIFont boldSystemFontOfSize:font_size];
        msg3Label.textAlignment = NSTextAlignmentLeft;
        msg3Label.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        msg3Label.numberOfLines = 0;
        //msg3Label.backgroundColor = [UIColor redColor];
        [ticket_background_view addSubview:msg3Label];
        pos_y = msg3Label.frame.origin.y + msg3Label.frame.size.height + 5;
    }
    CGRect frame = self.frame;
    frame.size.height = pos_y + 20;
    self.frame = frame;
    frame = ticket_background_view.frame;
    frame.size.height = pos_y + 20;
    ticket_background_view.frame = frame;
}

//已取電子票
- (void)createObtainedETicket {
    NSInteger font_size = 16;
    NSInteger space;
    pos_x = TICKET_SPACING;
    pos_y = 20;
    width = ticket_background_view.frame.size.width - (CELL_SPACING * 2);
    height = [[Tools getInstance] text:_productName heightWithFontSize:font_size width:width lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [productNameLabel setText:_productName lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel.font = [UIFont boldSystemFontOfSize:font_size];
    productNameLabel.textAlignment = NSTextAlignmentCenter;
    productNameLabel.numberOfLines = 0;
    [ticket_background_view addSubview:productNameLabel];
    productNameLabel.textColor = [UIColor blackColor];
    [productNameLabel setBackgroundColor:[UIColor clearColor]];
    [ticket_background_view addSubview:productNameLabel];
    
    //ticket type 2.24
    if([_ticket_format isEqualToString:@"blue"]) {
        height = (ticket_background_view.frame.size.width * 2) / 6 + 36;
        width = height * 2.24;
        pos_x = (ticket_background_view.frame.size.width - width) / 2;
        pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING;
        ticket_view = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        ticket_view.image = [UIImage imageNamed:@"ticket_1"];
        [ticket_background_view addSubview:ticket_view];
    } else if([_ticket_format isEqualToString:@"yellow"]) {
        height = (ticket_background_view.frame.size.width * 2) / 6 + 36;
        width = height * 2.24;
        pos_x = (ticket_background_view.frame.size.width - width) / 2;
        pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING;
        ticket_view = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        ticket_view.image = [UIImage imageNamed:@"ticket_yellow"];
        [ticket_background_view addSubview:ticket_view];
    } else if([_ticket_format isEqualToString:@"green"]) {
        height = (ticket_background_view.frame.size.width * 2) / 6 + 36;
        width = height * 2.24;
        pos_x = (ticket_background_view.frame.size.width - width) / 2;
        pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING;
        ticket_view = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        ticket_view.image = [UIImage imageNamed:@"ticket_3"];
        [ticket_background_view addSubview:ticket_view];
    }
    else if([_ticket_format isEqualToString:@"red"]) {
       height = (ticket_background_view.frame.size.width * 2) / 6 + 36;
       width = height * 2.24;
       pos_x = (ticket_background_view.frame.size.width - width) / 2;
       pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING;
       ticket_view = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
       ticket_view.image = [UIImage imageNamed:@"ticket_red"];
       [ticket_background_view addSubview:ticket_view];
   }
    //qr code
    NSInteger qwidth = (ticket_background_view.frame.size.width * 2) / 6;
    NSInteger qheight = qwidth;
    pos_x = (ticket_background_view.frame.size.width - qwidth) / 2;
    pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING + 20;
    qrCodeView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, qwidth, qheight)];
    if([_admissionStatus isEqualToString:@"1"]) { //已使用
        qrCodeView.image = [UIImage mdQRCodeForString:@"已使用"
                                                 size:qwidth
                                            fillColor:[UIColor lightGrayColor]];
        width = qrCodeView.frame.size.width;
        height = 50.0f;
        pos_x = 0;
        pos_y = (qrCodeView.frame.size.height - height) / 2;
        admissionStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        admissionStatusLabel.backgroundColor = [UIColor clearColor];
        [admissionStatusLabel setText:[NSString stringWithFormat:@"已使用\n%@",_ticketId] lineSpacing:CELL_TEXT_LINE_SPACE];
        admissionStatusLabel.font = [UIFont systemFontOfSize:TICKET_NORMAL_FONT_SIZE + 4];
        admissionStatusLabel.textAlignment = NSTextAlignmentCenter;
        admissionStatusLabel.textColor = [UIColor colorWithRed:(0xA0/255.0) green:(0x40/255.0) blue:(0x40/255.0) alpha:1];
        admissionStatusLabel.backgroundColor = [UIColor whiteColor];
        admissionStatusLabel.numberOfLines = 0;
        [qrCodeView addSubview:admissionStatusLabel];
    } else { //未使用
        qrCodeView.image = [UIImage mdQRCodeForString:[[Tools getInstance] getQRCodeStringWithPerformanceNo:_performanceNo andTicketID:_ticketId]
                                                 size:qwidth
                                            fillColor:[UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1]];
    }
    [ticket_background_view addSubview:qrCodeView];
    //購買日期
    font_size = 15;
    space = 4;
/*
    NSString *str = [NSString stringWithFormat:@"購買日期： %@",_orderTime];
    pos_x = 60;
    pos_y = qrCodeView.frame.origin.y + qrCodeView.frame.size.height + 30;
    width = ticket_background_view.frame.size.width - 120;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderTimeLabel.backgroundColor = [UIColor clearColor];
    [orderTimeLabel setText:str lineSpacing:space];
    orderTimeLabel.font = [UIFont systemFontOfSize:font_size];
    orderTimeLabel.textAlignment = NSTextAlignmentLeft;
    orderTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderTimeLabel];
*/
    //訂單編號
    NSString *str = [NSString stringWithFormat:@"訂單編號： %@",_orderNumber];
    pos_x = 60;
    pos_y = qrCodeView.frame.origin.y + qrCodeView.frame.size.height + 30;
    width = ticket_background_view.frame.size.width - 120;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderNumberLabel.backgroundColor = [UIColor clearColor];
    [orderNumberLabel setText:str lineSpacing:space];
    orderNumberLabel.font = [UIFont systemFontOfSize:font_size];
    orderNumberLabel.textAlignment = NSTextAlignmentLeft;
    orderNumberLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderNumberLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderNumberLabel];
    //球賽日期
    str = [NSString stringWithFormat:@"活動日期： %@",_showTime];
    pos_x = orderNumberLabel.frame.origin.x;
    pos_y = orderNumberLabel.frame.origin.y + orderNumberLabel.frame.size.height;
    width = orderNumberLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    showTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    showTimeLabel.backgroundColor = [UIColor clearColor];
    [showTimeLabel setText:str lineSpacing:space];
    showTimeLabel.font = [UIFont systemFontOfSize:font_size];
    showTimeLabel.textAlignment = NSTextAlignmentLeft;
    showTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    showTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:showTimeLabel];
    //活動地點
    str = [NSString stringWithFormat:@"活動地點： %@",_place];
    pos_x = showTimeLabel.frame.origin.x;
    pos_y = showTimeLabel.frame.origin.y + showTimeLabel.frame.size.height;
    width = showTimeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    placeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    placeLabel.backgroundColor = [UIColor clearColor];
    [placeLabel setText:str lineSpacing:space];
    placeLabel.font = [UIFont systemFontOfSize:font_size];
    placeLabel.textAlignment = NSTextAlignmentLeft;
    placeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    placeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:placeLabel];
    //座位
    str = [NSString stringWithFormat:@"%@",_orderSeat];
    pos_x = placeLabel.frame.origin.x;
    pos_y = placeLabel.frame.origin.y + placeLabel.frame.size.height + 5;
    width = placeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size + 2 width:width lineSpacing:space];
    orderSeatLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderSeatLabel.backgroundColor = [UIColor clearColor];
    [orderSeatLabel setText:str lineSpacing:space];
    orderSeatLabel.font = [UIFont boldSystemFontOfSize:font_size + 1];
    orderSeatLabel.textAlignment = NSTextAlignmentLeft;
    orderSeatLabel.textColor = [UIColor colorWithRed:(0xff/255.0) green:(0xbb/255.0) blue:(0x66/255.0) alpha:1.0f];
    orderSeatLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderSeatLabel];
    //票價 + 票別
    NSString *priceText = [NSString stringWithFormat:@"%@  %@",_price,_priceType];
    pos_x = orderSeatLabel.frame.origin.x;
    pos_y = orderSeatLabel.frame.origin.y + orderSeatLabel.frame.size.height + 10;
    width = orderSeatLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    priceLabel.backgroundColor = [UIColor clearColor];
    [priceLabel setText:priceText lineSpacing:space];
    priceLabel.font = [UIFont systemFontOfSize:font_size];
    priceLabel.textAlignment = NSTextAlignmentLeft;
    priceLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    priceLabel.numberOfLines = 0;
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:priceText];
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor orangeColor]
                          range:[[attributedStr string] rangeOfString:@"$"]];
    [attributedStr addAttribute:NSFontAttributeName value: [UIFont fontWithName:@"Arial" size:22.0] range:[[attributedStr string] rangeOfString:@"$"]];

    if(_price.length > 1) {
        NSString *str = [_price substringFromIndex:1];
        [attributedStr addAttribute:NSForegroundColorAttributeName
                              value:[UIColor orangeColor]
                              range:[[attributedStr string] rangeOfString:str]];
        [attributedStr addAttribute:NSFontAttributeName value: [UIFont fontWithName:@"Arial" size:22.0] range:[[attributedStr string] rangeOfString:str]];
    }
    [priceLabel setAttributedText:attributedStr];
    [ticket_background_view addSubview:priceLabel];
    //轉贈人
    //_senderName = @"陳曉薇";
    NSInteger len = [_senderName length];
     if(len > 0) {
         //轉贈人
         str = [NSString stringWithFormat:@"%@ 轉贈",_senderName];
         pos_x = priceLabel.frame.origin.x;
         pos_y = priceLabel.frame.origin.y + priceLabel.frame.size.height + 10;
         width = priceLabel.frame.size.width;
         height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
         senderNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
         senderNameLabel.backgroundColor = [UIColor clearColor];
         [senderNameLabel setText:str lineSpacing:space];
         senderNameLabel.font = [UIFont boldSystemFontOfSize:font_size];
         senderNameLabel.textAlignment = NSTextAlignmentCenter;
         senderNameLabel.textColor = [UIColor colorWithRed:(0x0/255.0) green:(0x0/255.0) blue:(0x90/255.0) alpha:1.0f];
         senderNameLabel.numberOfLines = 0;
         [ticket_background_view addSubview:senderNameLabel];
         pos_x = senderNameLabel.frame.origin.x;
         pos_y = senderNameLabel.frame.origin.y + senderNameLabel.frame.size.height + 10;
         width = senderNameLabel.frame.size.width;
     } else {
         pos_x = priceLabel.frame.origin.x;
         pos_y = priceLabel.frame.origin.y + priceLabel.frame.size.height + 10;
         width = priceLabel.frame.size.width;
    }
    if(![_admissionStatus isEqualToString:@"1"]) { //已使用
        height = 40;
        sendTicketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sendTicketBtn.tag = 1;
        sendTicketBtn.frame = CGRectMake(pos_x, pos_y, width, height);
        [sendTicketBtn setTitle:@"轉送" forState:UIControlStateNormal];
        [sendTicketBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        sendTicketBtn.layer.borderWidth = 0;
        sendTicketBtn.backgroundColor = [UIColor blueColor];
        [sendTicketBtn addTarget:self action:@selector(pressSendTicket) forControlEvents:UIControlEventTouchUpInside];
        sendTicketBtn.enabled = YES;
        [[sendTicketBtn layer] setCornerRadius:10.0f];
        [sendTicketBtn setBackgroundColor:[UIColor colorWithRed:(0xc1/255.0) green:(0x17/255.0) blue:(0x2d/255.0) alpha:1.0f]];
        [ticket_background_view addSubview:sendTicketBtn];
        CGRect frame = self.frame;
        frame.size.height = sendTicketBtn.frame.origin.y + sendTicketBtn.frame.size.height + 20;
        self.frame = frame;
        frame = ticket_background_view.frame;
        frame.size.height = sendTicketBtn.frame.origin.y + sendTicketBtn.frame.size.height + 20;
        ticket_background_view.frame = frame;
        pos_x = sendTicketBtn.frame.origin.x;
        pos_y = sendTicketBtn.frame.origin.y + sendTicketBtn.frame.size.height + 10;
        width = sendTicketBtn.frame.size.width;
    }
    //注意事項
    len = [_remark length];
    if(len > 0) {
        pos_x = TICKET_SPACING;
        width = ticket_background_view.frame.size.width - (CELL_SPACING * 2);
        str = [NSString stringWithFormat:@"%@",_remark];
        height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
        remarkLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        remarkLabel.font = [UIFont boldSystemFontOfSize:font_size];
        remarkLabel.backgroundColor = [UIColor clearColor];
        [remarkLabel setText:str lineSpacing:space];
        remarkLabel.textAlignment = NSTextAlignmentLeft;
        remarkLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        remarkLabel.numberOfLines = 0;
        [ticket_background_view addSubview:remarkLabel];
        pos_x = remarkLabel.frame.origin.x;
        pos_y = remarkLabel.frame.origin.y + remarkLabel.frame.size.height + 10;
        width = remarkLabel.frame.size.width;
    }

    len = _taxNumber.length;
    if(len > 0) {
        str = [NSString stringWithFormat:@"%@",_taxNumber];
        height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
        taxNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        taxNumberLabel.backgroundColor = [UIColor clearColor];
        [taxNumberLabel setText:str lineSpacing:space];
        taxNumberLabel.font = [UIFont boldSystemFontOfSize:font_size];
        taxNumberLabel.textAlignment = NSTextAlignmentLeft;
        taxNumberLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        taxNumberLabel.numberOfLines = 0;
        [ticket_background_view addSubview:taxNumberLabel];
        pos_x = taxNumberLabel.frame.origin.x;
        pos_y = taxNumberLabel.frame.origin.y + taxNumberLabel.frame.size.height;
        width = taxNumberLabel.frame.size.width;
    }

    len = _taxInfo.length;
   if(len > 0) {
       str = [NSString stringWithFormat:@"%@",_taxInfo];
       height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
       taxInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
       taxInfoLabel.backgroundColor = [UIColor clearColor];
       [taxInfoLabel setText:str lineSpacing:space];
       taxInfoLabel.font = [UIFont systemFontOfSize:12];
       taxInfoLabel.textAlignment = NSTextAlignmentLeft;
       taxInfoLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
       taxInfoLabel.numberOfLines = 0;
       [ticket_background_view addSubview:taxInfoLabel];
       pos_x = taxInfoLabel.frame.origin.x;
       pos_y = taxInfoLabel.frame.origin.y + taxInfoLabel.frame.size.height;
       width = taxInfoLabel.frame.size.width;
   }
    CGRect frame = self.frame;
    frame.size.height = pos_y + 20;
    self.frame = frame;
    frame = ticket_background_view.frame;
    frame.size.height = pos_y + 20;
    ticket_background_view.frame = frame;
}

//已取轉贈電子票券 轉送中
- (void)createObtainedETicketTransfer {
    NSInteger font_size = 16;
    NSInteger space;
    pos_x = TICKET_SPACING;
    pos_y = 20;
    width = ticket_background_view.frame.size.width - (CELL_SPACING * 2);
    height = [[Tools getInstance] text:_productName heightWithFontSize:font_size width:width lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [productNameLabel setText:_productName lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel.font = [UIFont boldSystemFontOfSize:font_size];
    productNameLabel.textAlignment = NSTextAlignmentCenter;
    productNameLabel.numberOfLines = 0;
    productNameLabel.textColor = [UIColor blackColor];
    [productNameLabel setBackgroundColor:[UIColor clearColor]];
    [ticket_background_view addSubview:productNameLabel];
    NSInteger qwidth = (ticket_background_view.frame.size.width * 2) / 6;
    NSInteger qheight = qwidth;
    pos_x = (ticket_background_view.frame.size.width - qwidth) / 2;
    pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING + 20;
    qrCodeView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, qwidth, qheight)];
    qrCodeView.image = [UIImage mdQRCodeForString:@"轉贈待接收"
                                             size:qwidth
                                        fillColor:[UIColor lightGrayColor]];
    [ticket_background_view addSubview:qrCodeView];
    //購買日期
    font_size = 15;
    space = 4;
/*
    NSString *str = [NSString stringWithFormat:@"購買日期： %@",_orderTime];
    pos_x = 60;
    pos_y = qrCodeView.frame.origin.y + qrCodeView.frame.size.height + 30;
    width = ticket_background_view.frame.size.width - 120;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderTimeLabel.backgroundColor = [UIColor clearColor];
    [orderTimeLabel setText:str lineSpacing:space];
    orderTimeLabel.font = [UIFont systemFontOfSize:font_size];
    orderTimeLabel.textAlignment = NSTextAlignmentLeft;
    orderTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderTimeLabel];
*/
    //訂單編號
    NSString *str = [NSString stringWithFormat:@"訂單編號： %@",_orderNumber];
    pos_x = 60;
    pos_y = qrCodeView.frame.origin.y + qrCodeView.frame.size.height + 30;
    width = ticket_background_view.frame.size.width - 120;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderNumberLabel.backgroundColor = [UIColor clearColor];
    [orderNumberLabel setText:str lineSpacing:space];
    orderNumberLabel.font = [UIFont systemFontOfSize:font_size];
    orderNumberLabel.textAlignment = NSTextAlignmentLeft;
    orderNumberLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderNumberLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderNumberLabel];
    //球賽日期
    str = [NSString stringWithFormat:@"活動日期： %@",_showTime];
    pos_x = orderNumberLabel.frame.origin.x;
    pos_y = orderNumberLabel.frame.origin.y + orderNumberLabel.frame.size.height;
    width = orderNumberLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    showTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    showTimeLabel.backgroundColor = [UIColor clearColor];
    [showTimeLabel setText:str lineSpacing:space];
    showTimeLabel.font = [UIFont systemFontOfSize:font_size];
    showTimeLabel.textAlignment = NSTextAlignmentLeft;
    showTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    showTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:showTimeLabel];
    //活動地點
    str = [NSString stringWithFormat:@"活動地點： %@",_place];
    pos_x = showTimeLabel.frame.origin.x;
    pos_y = showTimeLabel.frame.origin.y + showTimeLabel.frame.size.height;
    width = showTimeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    placeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    placeLabel.backgroundColor = [UIColor clearColor];
    [placeLabel setText:str lineSpacing:space];
    placeLabel.font = [UIFont systemFontOfSize:font_size];
    placeLabel.textAlignment = NSTextAlignmentLeft;
    placeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    placeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:placeLabel];
    //座位
    str = [NSString stringWithFormat:@"%@",_orderSeat];
    pos_x = placeLabel.frame.origin.x;
    pos_y = placeLabel.frame.origin.y + placeLabel.frame.size.height + 5;
    width = placeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size + 2 width:width lineSpacing:space];
    orderSeatLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderSeatLabel.backgroundColor = [UIColor clearColor];
    [orderSeatLabel setText:str lineSpacing:space];
    orderSeatLabel.font = [UIFont boldSystemFontOfSize:font_size + 1];
    orderSeatLabel.textAlignment = NSTextAlignmentLeft;
    orderSeatLabel.textColor = [UIColor colorWithRed:(0xff/255.0) green:(0xbb/255.0) blue:(0x66/255.0) alpha:1.0f];
    orderSeatLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderSeatLabel];
    //票價 + 票別
    NSString *priceText = [NSString stringWithFormat:@"%@  %@",_price,_priceType];
    pos_x = orderSeatLabel.frame.origin.x;
    pos_y = orderSeatLabel.frame.origin.y + orderSeatLabel.frame.size.height + 10;
    width = orderSeatLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    priceLabel.backgroundColor = [UIColor clearColor];
    [priceLabel setText:priceText lineSpacing:space];
    priceLabel.font = [UIFont systemFontOfSize:font_size];
    priceLabel.textAlignment = NSTextAlignmentLeft;
    priceLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    priceLabel.numberOfLines = 0;
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:priceText];
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor colorWithRed:(254/255.0) green:(202/255.0) blue:(16/255.0) alpha:1.0f]
                          range:[[attributedStr string] rangeOfString:@"$"]];
    [attributedStr addAttribute:NSFontAttributeName value: [UIFont fontWithName:@"Arial" size:22.0] range:[[attributedStr string] rangeOfString:@"$"]];

    if(_price.length > 1) {
        NSString *str = [_price substringFromIndex:1];
        [attributedStr addAttribute:NSForegroundColorAttributeName
                              value:[UIColor orangeColor]
                              range:[[attributedStr string] rangeOfString:str]];
        [attributedStr addAttribute:NSFontAttributeName value: [UIFont fontWithName:@"Arial" size:22.0] range:[[attributedStr string] rangeOfString:str]];
    }
    [priceLabel setAttributedText:attributedStr];
    [ticket_background_view addSubview:priceLabel];


    //轉贈人
    str = [NSString stringWithFormat:@"轉送至 %@",_receiveName];
    pos_x = priceLabel.frame.origin.x;
    pos_y = priceLabel.frame.origin.y + priceLabel.frame.size.height + 10;
    width = priceLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:CELL_TEXT_LINE_SPACE];
    senderNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    senderNameLabel.backgroundColor = [UIColor clearColor];
    [senderNameLabel setText:str lineSpacing:CELL_TEXT_LINE_SPACE];
    senderNameLabel.font = [UIFont boldSystemFontOfSize:font_size];
    senderNameLabel.textAlignment = NSTextAlignmentCenter;
    senderNameLabel.textColor = [UIColor colorWithRed:(0x0/255.0) green:(0x0/255.0) blue:(0x90/255.0) alpha:1.0f];
    senderNameLabel.numberOfLines = 0;
    [ticket_background_view addSubview:senderNameLabel];


    pos_x = senderNameLabel.frame.origin.x;
    pos_y = senderNameLabel.frame.origin.y + senderNameLabel.frame.size.height + 10;
    height = 40;
    getBackTicketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    getBackTicketBtn.tag = 2;
    getBackTicketBtn.frame = CGRectMake(pos_x, pos_y, width, height);
    [getBackTicketBtn setTitle:@"取回票券(轉贈待接收)" forState:UIControlStateNormal];
    getBackTicketBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [getBackTicketBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    getBackTicketBtn.layer.borderWidth = 0;
    getBackTicketBtn.backgroundColor = [UIColor blueColor];
    [getBackTicketBtn addTarget:self action:@selector(pressGetBackTicket) forControlEvents:UIControlEventTouchUpInside];
    getBackTicketBtn.enabled = YES;
    [[getBackTicketBtn layer] setCornerRadius:10.0f];
    [getBackTicketBtn setBackgroundColor:[UIColor colorWithRed:(0xc1/255.0) green:(0x17/255.0) blue:(0x2d/255.0) alpha:1.0f]];
    [ticket_background_view addSubview:getBackTicketBtn];
    pos_x = TICKET_SPACING;
    pos_y = getBackTicketBtn.frame.origin.y + getBackTicketBtn.frame.size.height + 10;
    //注意事項
    str = [NSString stringWithFormat:@"%@",_remark];
    if(str.length > 0) {
        width = ticket_background_view.frame.size.width - (CELL_SPACING * 2);
        height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
        remarkLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        remarkLabel.font = [UIFont boldSystemFontOfSize:font_size];
        remarkLabel.backgroundColor = [UIColor clearColor];
        [remarkLabel setText:str lineSpacing:space];
        remarkLabel.textAlignment = NSTextAlignmentLeft;
        remarkLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
        remarkLabel.numberOfLines = 0;
        [ticket_background_view addSubview:remarkLabel];
        pos_y = remarkLabel.frame.origin.y + remarkLabel.frame.size.height;
    }
    CGRect frame = self.frame;
    frame.size.height = pos_y + 20;
    self.frame = frame;
    frame = ticket_background_view.frame;
    frame.size.height = pos_y + 20;
    ticket_background_view.frame = frame;
}

//已取紙本票券
- (void)createPhysicalTicket_Obtained {
    NSInteger font_size = 16;
    NSInteger space;
    pos_x = TICKET_SPACING;
    pos_y = 20;
    width = ticket_background_view.frame.size.width - (CELL_SPACING * 2);
    height = [[Tools getInstance] text:_productName heightWithFontSize:font_size width:width lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [productNameLabel setText:_productName lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel.font = [UIFont systemFontOfSize:font_size];
    productNameLabel.textAlignment = NSTextAlignmentLeft;
    productNameLabel.numberOfLines = 0;
    [ticket_background_view addSubview:productNameLabel];
    productNameLabel.textColor = [UIColor colorWithRed:(0x0/255.0) green:(0x0/255.0) blue:(0x90/255.0) alpha:1];
    [productNameLabel setBackgroundColor:[UIColor clearColor]];
    [ticket_background_view addSubview:productNameLabel];
    //訂單編號
    font_size = 15;
    space = 4;
    
    NSString *str = [NSString stringWithFormat:@"訂單編號： %@",_orderNumber];
    pos_x = TICKET_SPACING;
    pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + 15;
    width = productNameLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderNumberLabel.backgroundColor = [UIColor clearColor];
    [orderNumberLabel setText:str lineSpacing:space];
    orderNumberLabel.font = [UIFont systemFontOfSize:font_size];
    orderNumberLabel.textAlignment = NSTextAlignmentLeft;
    orderNumberLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderNumberLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderNumberLabel];
    //訂購時間
    str = [NSString stringWithFormat:@"訂購時間： %@",_orderTime];
    pos_x = orderNumberLabel.frame.origin.x;
    pos_y = orderNumberLabel.frame.origin.y + orderNumberLabel.frame.size.height;
    width = orderNumberLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderTimeLabel.backgroundColor = [UIColor clearColor];
    [orderTimeLabel setText:str lineSpacing:space];
    orderTimeLabel.font = [UIFont systemFontOfSize:font_size];
    orderTimeLabel.textAlignment = NSTextAlignmentLeft;
    orderTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderTimeLabel];
    //活動場地
    str = [NSString stringWithFormat:@"活動場地： %@",_place];
    pos_x = orderTimeLabel.frame.origin.x;
    pos_y = orderTimeLabel.frame.origin.y + orderTimeLabel.frame.size.height;
    width = orderTimeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    placeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    placeLabel.backgroundColor = [UIColor clearColor];
    [placeLabel setText:str lineSpacing:space];
    placeLabel.font = [UIFont systemFontOfSize:font_size];
    placeLabel.textAlignment = NSTextAlignmentLeft;
    placeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    placeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:placeLabel];
    //活動時間
    str = [NSString stringWithFormat:@"活動時間： %@",_showTime];
    pos_x = placeLabel.frame.origin.x;
    pos_y = placeLabel.frame.origin.y + placeLabel.frame.size.height;
    width = placeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    showTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    showTimeLabel.backgroundColor = [UIColor clearColor];
    [showTimeLabel setText:str lineSpacing:space];
    showTimeLabel.font = [UIFont systemFontOfSize:font_size];
    showTimeLabel.textAlignment = NSTextAlignmentLeft;
    showTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    showTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:showTimeLabel];
    //取票方式
    str = [NSString stringWithFormat:@"取票方式： %@",_getMethod];
    pos_x = showTimeLabel.frame.origin.x;
    pos_y = showTimeLabel.frame.origin.y + showTimeLabel.frame.size.height;
    width = showTimeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    getMethodLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    getMethodLabel.backgroundColor = [UIColor clearColor];
    [getMethodLabel setText:str lineSpacing:space];
    getMethodLabel.font = [UIFont systemFontOfSize:font_size];
    getMethodLabel.textAlignment = NSTextAlignmentLeft;
    getMethodLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    getMethodLabel.numberOfLines = 0;
    [ticket_background_view addSubview:getMethodLabel];
    //付款方式
    str = [NSString stringWithFormat:@"付款方式： %@",_payMethod];
    pos_x = getMethodLabel.frame.origin.x;
    pos_y = getMethodLabel.frame.origin.y + getMethodLabel.frame.size.height;
    width = getMethodLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    payMethodLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    payMethodLabel.backgroundColor = [UIColor clearColor];
    [payMethodLabel setText:str lineSpacing:space];
    payMethodLabel.font = [UIFont systemFontOfSize:font_size];
    payMethodLabel.textAlignment = NSTextAlignmentLeft;
    payMethodLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    payMethodLabel.numberOfLines = 0;
    [ticket_background_view addSubview:payMethodLabel];
    //售價
    NSString *priceText = [NSString stringWithFormat:@"售價： %@（%@）",_price,_priceType];
    pos_x = payMethodLabel.frame.origin.x;
    pos_y = payMethodLabel.frame.origin.y + payMethodLabel.frame.size.height;
    width = payMethodLabel.frame.size.width;
    height = [[Tools getInstance] text:priceText heightWithFontSize:font_size width:width lineSpacing:space];
    priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    priceLabel.backgroundColor = [UIColor clearColor];
    [priceLabel setText:priceText lineSpacing:space];
    priceLabel.font = [UIFont systemFontOfSize:font_size];
    priceLabel.textAlignment = NSTextAlignmentLeft;
    priceLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    priceLabel.numberOfLines = 0;
    [ticket_background_view addSubview:priceLabel];
    //座位
    str = [NSString stringWithFormat:@"座位： %@",_orderSeat];
    pos_x = priceLabel.frame.origin.x;
    pos_y = priceLabel.frame.origin.y + priceLabel.frame.size.height;
    width = priceLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    orderSeatLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderSeatLabel.backgroundColor = [UIColor clearColor];
    orderSeatLabel.font = [UIFont systemFontOfSize:font_size];
    orderSeatLabel.textAlignment = NSTextAlignmentLeft;
    orderSeatLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    orderSeatLabel.numberOfLines = 0;
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f]
                          range:[[attributedStr string] rangeOfString:@"座位："]];
    
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor colorWithRed:(0x99/255.0) green:(0x0/255.0) blue:(0x0/255.0) alpha:1.0f]
                          range:[[attributedStr string] rangeOfString:_orderSeat]];
    
    [orderSeatLabel setAttributedText:attributedStr];
    [ticket_background_view addSubview:orderSeatLabel];
    //注意事項
    str = [NSString stringWithFormat:@"%@",_remark];
    pos_x = orderSeatLabel.frame.origin.x;
    pos_y = orderSeatLabel.frame.origin.y + orderSeatLabel.frame.size.height + 15;
    width = orderSeatLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    remarkLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    remarkLabel.font = [UIFont systemFontOfSize:font_size];
    remarkLabel.backgroundColor = [UIColor clearColor];
    [remarkLabel setText:str lineSpacing:space];
    remarkLabel.textAlignment = NSTextAlignmentLeft;
    remarkLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    remarkLabel.numberOfLines = 0;
    [ticket_background_view addSubview:remarkLabel];
    
    CGRect frame = self.frame;
    frame.size.height = remarkLabel.frame.origin.y + remarkLabel.frame.size.height + 20;
    self.frame = frame;
    frame = ticket_background_view.frame;
    frame.size.height = remarkLabel.frame.origin.y + remarkLabel.frame.size.height + 20;
    ticket_background_view.frame = frame;
}

//未取電子票券
- (void)createNotObtainedYetETicket {
    
    NSInteger font_size = 16;
    NSInteger space;
    pos_x = TICKET_SPACING;
    pos_y = 20;
    width = ticket_background_view.frame.size.width - (CELL_SPACING * 2);
    height = [[Tools getInstance] text:_productName heightWithFontSize:font_size width:width lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [productNameLabel setText:_productName lineSpacing:CELL_TEXT_LINE_SPACE];
    productNameLabel.font = [UIFont boldSystemFontOfSize:font_size];
    productNameLabel.textAlignment = NSTextAlignmentCenter;
    productNameLabel.numberOfLines = 0;
    [ticket_background_view addSubview:productNameLabel];
    productNameLabel.textColor = [UIColor blackColor];
    [productNameLabel setBackgroundColor:[UIColor clearColor]];
    [ticket_background_view addSubview:productNameLabel];

    float msgWidth = productNameLabel.frame.size.width;
    float pos_x = productNameLabel.frame.origin.x;
    float pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + 20;
    float msgHeight = 120;
    sendMsgView = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,msgWidth,msgHeight)];
    [sendMsgView setBackgroundColor:[UIColor colorWithRed:(0xff/255.0) green:(0xe6/255.0) blue:(0x99/255.0) alpha:1]];
    sendMsgView.layer.borderColor = [UIColor colorWithRed:(0xcd/255.0) green:(0xa9/255.0) blue:(0x40/255.0) alpha:1].CGColor;
    sendMsgView.layer.borderWidth = 2.0;
    sendMsgView.layer.cornerRadius = 10.0;
    [[sendMsgView layer] setMasksToBounds:YES];
    [ticket_background_view addSubview:sendMsgView];
    NSString *str = [NSString stringWithFormat:@"%@ 贈送",_senderName];
    pos_x = 0;
    pos_y = 10;
    width = sendMsgView.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:TICKET_NORMAL_FONT_SIZE width:width lineSpacing:CELL_TEXT_LINE_SPACE];
    senderNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    senderNameLabel.font = [UIFont systemFontOfSize:18];
    senderNameLabel.backgroundColor = [UIColor clearColor];
    [senderNameLabel setText:str lineSpacing:CELL_TEXT_LINE_SPACE];
    senderNameLabel.textAlignment = NSTextAlignmentCenter;
    senderNameLabel.textColor = [UIColor blackColor];
    senderNameLabel.numberOfLines = 0;
    [sendMsgView addSubview:senderNameLabel];
    pos_x = 5;
    pos_y = senderNameLabel.frame.origin.y + senderNameLabel.frame.size.height;
    width = sendMsgView.frame.size.width - 10;
    height = sendMsgView.frame.size.height - pos_y - 10;
    senderMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    senderMessageLabel.backgroundColor = [UIColor clearColor];
    [senderMessageLabel setText:_senderMessage lineSpacing:0];
    senderMessageLabel.font = [UIFont systemFontOfSize:15];
    senderMessageLabel.textAlignment = NSTextAlignmentCenter;
    senderMessageLabel.textColor = [UIColor blackColor];
    senderMessageLabel.numberOfLines = 0;
    [sendMsgView addSubview:senderMessageLabel];

    //球賽日期
    space = 4;
    pos_x = 60;
    pos_y = sendMsgView.frame.origin.y + sendMsgView.frame.size.height + 15;
    width = ticket_background_view.frame.size.width - 120;
    str = [NSString stringWithFormat:@"活動日期： %@",_showTime];
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    showTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    showTimeLabel.backgroundColor = [UIColor clearColor];
    [showTimeLabel setText:str lineSpacing:space];
    showTimeLabel.font = [UIFont systemFontOfSize:font_size];
    showTimeLabel.textAlignment = NSTextAlignmentLeft;
    showTimeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    showTimeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:showTimeLabel];
    //活動地點
    str = [NSString stringWithFormat:@"活動地點： %@",_place];
    pos_x = showTimeLabel.frame.origin.x;
    pos_y = showTimeLabel.frame.origin.y + showTimeLabel.frame.size.height;
    width = sendMsgView.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    placeLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    placeLabel.backgroundColor = [UIColor clearColor];
    [placeLabel setText:str lineSpacing:space];
    placeLabel.font = [UIFont systemFontOfSize:font_size];
    placeLabel.textAlignment = NSTextAlignmentLeft;
    placeLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    placeLabel.numberOfLines = 0;
    [ticket_background_view addSubview:placeLabel];



    //座位
    str = [NSString stringWithFormat:@"%@",_orderSeat];
    pos_x = placeLabel.frame.origin.x;
    pos_y = placeLabel.frame.origin.y + placeLabel.frame.size.height + 5;
    width = placeLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size + 2 width:width lineSpacing:space];
    orderSeatLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    orderSeatLabel.backgroundColor = [UIColor clearColor];
    [orderSeatLabel setText:str lineSpacing:space];
    orderSeatLabel.font = [UIFont boldSystemFontOfSize:font_size + 1];
    orderSeatLabel.textAlignment = NSTextAlignmentLeft;
    orderSeatLabel.textColor = [UIColor colorWithRed:(0xff/255.0) green:(0xbb/255.0) blue:(0x66/255.0) alpha:1.0f];
    orderSeatLabel.numberOfLines = 0;
    [ticket_background_view addSubview:orderSeatLabel];
    //票價 + 票別
    NSString *priceText = [NSString stringWithFormat:@"%@  %@",_price,_priceType];
    pos_x = orderSeatLabel.frame.origin.x;
    pos_y = orderSeatLabel.frame.origin.y + orderSeatLabel.frame.size.height + 10;
    width = orderSeatLabel.frame.size.width;
    height = [[Tools getInstance] text:str heightWithFontSize:font_size width:width lineSpacing:space];
    priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    priceLabel.backgroundColor = [UIColor clearColor];
    [priceLabel setText:priceText lineSpacing:space];
    priceLabel.font = [UIFont systemFontOfSize:font_size];
    priceLabel.textAlignment = NSTextAlignmentLeft;
    priceLabel.textColor = [UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1.0f];
    priceLabel.numberOfLines = 0;
    
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithString:priceText];
    [attributedStr addAttribute:NSForegroundColorAttributeName
                          value:[UIColor orangeColor]
                          range:[[attributedStr string] rangeOfString:@"$"]];
    [attributedStr addAttribute:NSFontAttributeName value: [UIFont fontWithName:@"Arial" size:22.0] range:[[attributedStr string] rangeOfString:@"$"]];

    if(_price.length > 1) {
        NSString *str = [_price substringFromIndex:1];
        [attributedStr addAttribute:NSForegroundColorAttributeName
                              value:[UIColor orangeColor]
                              range:[[attributedStr string] rangeOfString:str]];
        [attributedStr addAttribute:NSFontAttributeName value: [UIFont fontWithName:@"Arial" size:22.0] range:[[attributedStr string] rangeOfString:str]];
    }
    [priceLabel setAttributedText:attributedStr];
    [ticket_background_view addSubview:priceLabel];
    
    
    pos_x = priceLabel.frame.origin.x;
    pos_y = priceLabel.frame.origin.y + priceLabel.frame.size.height + 10;
    width = ticket_background_view.frame.size.width - 120;
    height = 40;
    acceptTicketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    acceptTicketBtn.tag = 5;
    acceptTicketBtn.frame = CGRectMake(pos_x, pos_y, width, height);
    [acceptTicketBtn setTitle:@"接收" forState:UIControlStateNormal];
    [acceptTicketBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    acceptTicketBtn.layer.borderWidth = 0;
    acceptTicketBtn.backgroundColor = [UIColor blueColor];
    [acceptTicketBtn addTarget:self action:@selector(pressAcceptTicket) forControlEvents:UIControlEventTouchUpInside];
    acceptTicketBtn.enabled = YES;
    [[acceptTicketBtn layer] setCornerRadius:10.0f];
    [acceptTicketBtn setBackgroundColor:[UIColor colorWithRed:(0xc1/255.0) green:(0x17/255.0) blue:(0x2d/255.0) alpha:1.0f]];
    [ticket_background_view addSubview:acceptTicketBtn];
    pos_x = acceptTicketBtn.frame.origin.x;
    pos_y = acceptTicketBtn.frame.origin.y + acceptTicketBtn.frame.size.height + 20;
    width = acceptTicketBtn.frame.size.width;
    height = acceptTicketBtn.frame.size.height;
    goBackTicketBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    goBackTicketBtn.tag = 6;
    goBackTicketBtn.frame = CGRectMake(pos_x, pos_y, width, height);
    [goBackTicketBtn setTitle:@"退回" forState:UIControlStateNormal];
    [goBackTicketBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    goBackTicketBtn.layer.borderWidth = 0;
    goBackTicketBtn.backgroundColor = [UIColor redColor];
    [goBackTicketBtn addTarget:self action:@selector(pressGoBackTicket) forControlEvents:UIControlEventTouchUpInside];
    goBackTicketBtn.enabled = YES;
    [[goBackTicketBtn layer] setCornerRadius:10.0f];
    [goBackTicketBtn setBackgroundColor:[UIColor colorWithRed:(0xb0/255.0) green:(0x15/255.0) blue:(0x15/255.0) alpha:1.0f]];
    [ticket_background_view addSubview:goBackTicketBtn];
    CGRect frame = self.frame;
    frame.size.height = goBackTicketBtn.frame.origin.y + goBackTicketBtn.frame.size.height + 20;
    self.frame = frame;
    frame = ticket_background_view.frame;
    frame.size.height = goBackTicketBtn.frame.origin.y + goBackTicketBtn.frame.size.height + 20;
    ticket_background_view.frame = frame;
}

//未取紙本票券
- (void)createPhysicalTicket_NotObtained {
}

- (void)setUpdateQrcode:(NSString *)updateQrcode {
    if([_isGoods isEqualToString:@"N"]) {
        if([_isObtained isEqualToString:@"Y"])  // 已取票券 test
            if([_ticketType isEqualToString:@"E"]) {
                [qrCodeView removeFromSuperview];
                qrCodeView = nil;
                NSInteger qwidth = (ticket_background_view.frame.size.width * 2) / 6;
                NSInteger qheight = qwidth;
                pos_x = (ticket_background_view.frame.size.width - qwidth) / 2;
                pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING + 20;
                if(![_admissionStatus isEqualToString:@"1"]) { //已使用
                    if([_transferFlag isEqualToString:@"N"]) {
                        qrCodeView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, qwidth, qheight)];
                        qrCodeView.image = [UIImage mdQRCodeForString:[[Tools getInstance] getQRCodeStringWithPerformanceNo:_performanceNo andTicketID:_ticketId]
                                                                 size:qwidth
                                                            fillColor:[UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1]];
                        [ticket_background_view addSubview:qrCodeView];
                    } else {
                        qrCodeView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, qwidth, qheight)];
                        qrCodeView.image = [UIImage mdQRCodeForString:@"轉贈待接收"
                                                                 size:qwidth
                                                            fillColor:[UIColor lightGrayColor]];
                        [ticket_background_view addSubview:qrCodeView];
                    }
                } else {
                    qrCodeView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, qwidth, qheight)];
                    qrCodeView.image = [UIImage mdQRCodeForString:@"已使用"
                                                             size:qwidth
                                                        fillColor:[UIColor lightGrayColor]];
                    width = qrCodeView.frame.size.width;
                    height = 50.0f;
                    pos_x = 0;
                    pos_y = (qrCodeView.frame.size.height - height) / 2;
                    admissionStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
                    admissionStatusLabel.backgroundColor = [UIColor clearColor];
                    [admissionStatusLabel setText:[NSString stringWithFormat:@"已使用\n%@",_ticketId] lineSpacing:CELL_TEXT_LINE_SPACE];
                    admissionStatusLabel.font = [UIFont systemFontOfSize:TICKET_NORMAL_FONT_SIZE + 4];
                    admissionStatusLabel.textAlignment = NSTextAlignmentCenter;
                    admissionStatusLabel.textColor = [UIColor colorWithRed:(0xA0/255.0) green:(0x40/255.0) blue:(0x40/255.0) alpha:1];
                    admissionStatusLabel.backgroundColor = [UIColor whiteColor];
                    admissionStatusLabel.numberOfLines = 0;
                    [qrCodeView addSubview:admissionStatusLabel];
                    [ticket_background_view addSubview:qrCodeView];

                }
        }
    } else {
        [qrCodeView removeFromSuperview];
        qrCodeView = nil;
        NSInteger qwidth = (ticket_background_view.frame.size.width * 2) / 6;
        NSInteger qheight = qwidth;
        pos_x = (ticket_background_view.frame.size.width - qwidth) / 2;
        pos_y = productNameLabel.frame.origin.y + productNameLabel.frame.size.height + CELL_SPACING + 20;
        if([_admissionStatus isEqualToString:@"1"]) { //已使用
            qrCodeView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, qwidth, qheight)];
            qrCodeView.image = [UIImage mdQRCodeForString:@"已使用"
                                                     size:qwidth
                                                fillColor:[UIColor lightGrayColor]];
            width = qrCodeView.frame.size.width;
            height = 50.0f;
            pos_x = 0;
            pos_y = (qrCodeView.frame.size.height - height) / 2;
            admissionStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            admissionStatusLabel.backgroundColor = [UIColor clearColor];
            [admissionStatusLabel setText:[NSString stringWithFormat:@"已使用\n%@",_ticketId] lineSpacing:CELL_TEXT_LINE_SPACE];
            admissionStatusLabel.font = [UIFont systemFontOfSize:TICKET_NORMAL_FONT_SIZE + 4];
            admissionStatusLabel.textAlignment = NSTextAlignmentCenter;
            admissionStatusLabel.textColor = [UIColor colorWithRed:(0xA0/255.0) green:(0x40/255.0) blue:(0x40/255.0) alpha:1];
            admissionStatusLabel.backgroundColor = [UIColor whiteColor];
            admissionStatusLabel.numberOfLines = 0;
            [qrCodeView addSubview:admissionStatusLabel];
            [ticket_background_view addSubview:qrCodeView];
        } else { //未使用
            qrCodeView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x, pos_y, qwidth, qheight)];
            qrCodeView.image = [UIImage mdQRCodeForString:[[Tools getInstance] getQRCodeStringWithPerformanceNo:_performanceNo andTicketID:_ticketId]
                                                     size:qwidth
                                                fillColor:[UIColor colorWithRed:(0x49/255.0) green:(0x49/255.0) blue:(0x49/255.0) alpha:1]];
            [ticket_background_view addSubview:qrCodeView];
        }
    }
}

//虛擬票券
- (void)createVirtualTicket {
}

//退票
- (void)pressReturnTicket {
    NSArray *firstSplit = [_orderNumber componentsSeparatedByString:@"-"];
    NSString *no = @"";
    if([firstSplit count] > 1)
        no = [firstSplit objectAtIndex:0];
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:no, @"order_number",_orderNumber,@"oi", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnTicket" object:self userInfo:dic];
}

//轉贈
- (void)pressSendTicket {
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:_performanceNo, @"performance_no",_ticketId, @"ticket_id",_is_seasonticket,@"is_seasonticket", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SendTicket" object:self userInfo:dic];
}

//取回票券
- (void)pressGetBackTicket {
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:_performanceNo, @"performance_no",_ticketId, @"ticket_id", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RetrieveTicket" object:self userInfo:dic];
}

//退回票券
- (void)pressGoBackTicket {
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:_performanceNo, @"performance_no",_ticketId, @"ticket_id", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GoBackTicket" object:self userInfo:dic];
}

//接收票券
- (void)pressAcceptTicket {
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:_performanceNo, @"performance_no",_ticketId, @"ticket_id", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AcceptTicket" object:self userInfo:dic];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect {
    // Drawing code
 
//}




@end
