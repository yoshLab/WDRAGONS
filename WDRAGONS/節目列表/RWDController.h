//
//  RWDController.h
//  brothers
//
//  Created by 陳威宇 on 2020/1/13.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "ConstantData.h"
#import "Tools.h"
#import "MBProgressHUD.h"
#import "AESCipher.h"
#import "AFNetworking.h"
#import "CustomIOSAlertView.h"
#import "MessageAlertView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>
#import <GoogleSignIn/GoogleSignIn.h>

#import <AuthenticationServices/AuthenticationServices.h>

NS_ASSUME_NONNULL_BEGIN

@interface RWDController : UIViewController <WKNavigationDelegate,WKUIDelegate,CustomIOSAlertViewDelegate,UITextFieldDelegate,FBSDKLoginButtonDelegate,GIDSignInDelegate,ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding> {
    float                   pos_x;
    float                   pos_y;
    float                   width;
    float                   height;
    NSTimer                 *timer;
    MBProgressHUD           *waitingView;
    BOOL                    isLoading;
    NSString                *backUrl;
    CustomIOSAlertView      *loginAlertView;
    UITextField             *accountTextField;
    UITextField             *passwordTextField;
    NSString                *loginStatus;
    NSInteger               showAlertType;
    
    UIImageView             *errorImageView;
    UILabel                 *errorMessageLabel;
    UIButton                *errorBtn;

    FBSDKLoginButton        *fsignInButton;

    GIDSignInButton         *gsignInButton;
    GIDSignIn               *signIn;

    
    NSString                *apple_user_id;
    NSString                *google_user_id;
    NSString                *fb_user_id;

 
}

@property (strong, nonatomic)AFHTTPSessionManager *manager;
@property (strong, nonatomic)WKWebView *homePageWebView;
@property (strong, nonatomic)NSString *currentNonce;

@end

NS_ASSUME_NONNULL_END
