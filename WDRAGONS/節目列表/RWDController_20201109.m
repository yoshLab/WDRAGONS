//
//  RWDController.m
//  brothers
//
//  Created by 陳威宇 on 2020/1/13.
//  Copyright © 2020 陳威宇. All rights reserved.
//

#import "RWDController.h"
#import <CommonCrypto/CommonDigest.h>

@interface RWDController ()

@end

@implementation RWDController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [GIDSignIn sharedInstance].presentingViewController = self;

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshHomePage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshHomePage) name:@"RefreshHomePage" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"GoBackWebPage_homepage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBackWebPage) name:@"GoBackWebPage_homepage" object:nil];
    self.view.backgroundColor = [UIColor colorWithRed:(248.0f / 255.0f) green:(248.0f / 255.0f) blue:(248.0f / 255.0f) alpha:1];
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        //CGFloat topPadding = window.safeAreaInsets.top;
        CGFloat bottomPadding = window.safeAreaInsets.bottom;
        pos_x = 0;
        pos_y = DEVICE_HEIGHT - bottomPadding;
        width = DEVICE_WIDTH;
        height = bottomPadding;
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        view.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:view];
    }
    [self initWebView];

}

//畫面出現前
- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self.tabBarController.tabBar invalidateIntrinsicContentSize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_1" object:nil userInfo:nil];
    [errorImageView removeFromSuperview];
    [errorMessageLabel removeFromSuperview];
    [errorBtn removeFromSuperview];
    errorBtn = nil;
    errorImageView = nil;
    errorMessageLabel = nil;
    [_homePageWebView removeFromSuperview];
    [self.view addSubview:_homePageWebView];
    if([AppDelegate sharedAppDelegate].reloadRWD == NO) {
        [self loadWebView];
    } else {
      [AppDelegate sharedAppDelegate].reloadRWD = NO;
        //[self refreshHomePage];
    }

}

//離開畫面前
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_homePageWebView stopLoading];
    [timer invalidate];
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_1" object:nil userInfo:nil];
    [self closeLoginView];
}

- (void)refreshHomePage {
    //[self loadWebView];
    [_homePageWebView reload];
    
}

- (void)goBackWebPage {
    if([_homePageWebView canGoBack]) {
        [_homePageWebView goBack];
    }
}

- (void)initWebView {
    isLoading = NO;
    [_homePageWebView removeFromSuperview];
    _homePageWebView = nil;
    pos_x = 0;
    pos_y = 20;
    float width = DEVICE_WIDTH;
    float height = DEVICE_HEIGHT - STATUS_BAR_HEIGHT - TAB_BAR_HEIGHT;
    CGRect rectStatus = [[UIApplication sharedApplication] statusBarFrame];
    if(rectStatus.size.height >= 44) {
        height = DEVICE_HEIGHT - STATUS_BAR_HEIGHT_X - TAB_BAR_HEIGHT - SAFE_AREA;
        pos_y = STATUS_BAR_HEIGHT_X;
    }
    _homePageWebView = [[WKWebView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    
    if (@available(iOS 11.0, *)) {
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    //[_homePageWebView setDataDetectorTypes:UIDataDetectorTypeLink];
    _homePageWebView.tag = 1;
    _homePageWebView.navigationDelegate = self;
    _homePageWebView.UIDelegate = self;
    _homePageWebView.scrollView.bounces = NO;
    _homePageWebView.scrollView.showsVerticalScrollIndicator = NO;
    _homePageWebView.scrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:_homePageWebView];
}

- (void)showErrorView {
    [AppDelegate sharedAppDelegate].error_reload = YES;
    [_homePageWebView removeFromSuperview];
    [errorImageView removeFromSuperview];
    [errorMessageLabel removeFromSuperview];
    [errorBtn removeFromSuperview];
    errorBtn = nil;
    errorImageView = nil;
    errorMessageLabel = nil;
    width = DEVICE_WIDTH - 40;
    height = width;
    pos_x = 20;
    pos_y = (DEVICE_HEIGHT - height) / 2;
    
    
    errorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    errorImageView.image = [UIImage imageNamed:@"CF_1"];
    [self.view addSubview:errorImageView];

    pos_x = 0;
    pos_y = errorImageView.frame.origin.y + (errorImageView.frame.size.height / 2) + 40;
    width = DEVICE_WIDTH;
    height = 20;
    errorMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    errorMessageLabel.backgroundColor = [UIColor clearColor];
    errorMessageLabel.font = [UIFont systemFontOfSize:18];
    errorMessageLabel.textAlignment = NSTextAlignmentCenter;
    errorMessageLabel.textColor = [UIColor colorWithRed:(0x33/255.0) green:(0x33/255.0) blue:(0x33/255.0) alpha:1.0f];
    errorMessageLabel.text = @"系統連線異常";
    errorMessageLabel.numberOfLines = 0;
    [self.view addSubview:errorMessageLabel];
    
    pos_x = errorImageView.frame.origin.x;
    pos_y = errorMessageLabel.frame.origin.y + errorMessageLabel.frame.size.height + 10;
    width = errorImageView.frame.size.width;
    height = 20;
    errorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    errorBtn.tag = 1;
    errorBtn.frame = CGRectMake(pos_x, pos_y, width, height);
    [errorBtn addTarget:self action:@selector(pressErrorBtn) forControlEvents:UIControlEventTouchUpInside];
    [errorBtn setTitle:@"回首頁" forState:UIControlStateNormal];
    errorBtn.backgroundColor =  [UIColor clearColor];
    [errorBtn setTitleColor:[UIColor colorWithRed:(0x33/255.0) green:(0x33/255.0) blue:(0x33/255.0) alpha:1.0f] forState:UIControlStateNormal];
    errorBtn.titleLabel.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:errorBtn];

    
}

- (void)pressErrorBtn {
    [errorImageView removeFromSuperview];
    [errorMessageLabel removeFromSuperview];
    [errorBtn removeFromSuperview];
    errorBtn = nil;
    errorImageView = nil;
    errorMessageLabel = nil;
    [self.view addSubview:_homePageWebView];
    
    
    [self loadWebView];
}



- (void)loadWebView {
    [AppDelegate sharedAppDelegate].is_home_loading = YES;
    //NSString *urlString = HOME_PAGE_URL;
    NSString *urlString = [AppDelegate sharedAppDelegate].rwd_url;
    NSMutableDictionary *dict = [[Tools getInstance] getUserInfo];
    NSString *isLogin = [dict objectForKey:ISLOGIN];
    NSString *account = [dict objectForKey:ACCOUNT];
    NSString *password = [dict objectForKey:PWD];
    NSString *auth_type = [dict objectForKey:AUTH_TYPE];
    NSString *user_id = [dict objectForKey:USER_ID];

    if([isLogin isEqualToString:@"Y"]) {
        NSString *u = aesEncryptString(account, AES_KEY);
        NSString *t = aesEncryptString(password, AES_KEY);
        NSString *at = aesEncryptString(auth_type, AES_KEY);
        NSString *g = aesEncryptString(user_id, AES_KEY);
        
        
        if([auth_type isEqualToString:@"fb"]) {
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"_DV", @"IAMAPP",
                                             @"at" , at,
                                             @"g"  , g,
                                             @"av" , TICKET_VERSION,
                                             @"mtp", @"ios",
                                             nil];
            [self webViewWithPost:_homePageWebView urlString:urlString params:webViewParams];

        } else if([auth_type isEqualToString:@"google"]) {
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"_DV", @"IAMAPP",
                                             @"at" , at,
                                             @"g"  , g,
                                             @"av" , TICKET_VERSION,
                                             @"mtp", @"ios",
                                             nil];
            [self webViewWithPost:_homePageWebView urlString:urlString params:webViewParams];
            
        } else if([auth_type isEqualToString:@"apple"]) {
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"_DV", @"IAMAPP",
                                             @"at" , at,
                                             @"g"  , g,
                                             @"av" , TICKET_VERSION,
                                             @"mtp", @"ios",
                                             nil];
            [self webViewWithPost:_homePageWebView urlString:urlString params:webViewParams];
        } else {
            NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                             @"_DV", @"IAMAPP",
                                             @"u" , u,
                                             @"t" , t,
                                             @"at" , at,
                                             @"av" , TICKET_VERSION,
                                             @"mtp", @"ios",
                                             nil];
            [self webViewWithPost:_homePageWebView urlString:urlString params:webViewParams];
        }
    } else {
        NSMutableArray *webViewParams = [NSMutableArray arrayWithObjects:
                                         @"_DV", @"IAMAPP",
                                         @"av" , TICKET_VERSION,
                                         @"mtp", @"ios",
                                         nil];
        [self webViewWithPost:_homePageWebView urlString:urlString params:webViewParams];
    }
}

- (void)webViewWithPost:(WKWebView *)webView urlString:(NSString *)urlString params:(NSMutableArray *)params {
    NSMutableString *s = [NSMutableString stringWithCapacity:0];
    [s appendString: [NSString stringWithFormat:@"<html><body onload=\"document.forms[0].submit()\">"
                      "<form method=\"post\" action=\"%@\">", urlString]];
    if([params count] % 2 == 1) { NSLog(@"WebViewWithPost error: params don't seem right"); return; }
    for (int i=0; i < [params count] / 2; i++) {
        [s appendString: [NSString stringWithFormat:@"<input type=\"hidden\" name=\"%@\" value=\"%@\" >\n", [params objectAtIndex:i*2], [params objectAtIndex:(i*2)+1]]];
    }
    [s appendString: @"</input></form></body></html>"];
    //NSLog(@"%@", s);
    [webView loadHTMLString:s baseURL:nil];
    [timer invalidate];
    timer= nil;
    timer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(timeout) userInfo:nil repeats:NO];
}

- (void)parser_cmd:(NSString *)cmd_url {
    @try {
        NSArray *firstSplit = [cmd_url componentsSeparatedByString:@"APPACTION?"];
        if([firstSplit count] >= 2) {
            //
            NSArray *secondSplit = [[firstSplit objectAtIndex:1] componentsSeparatedByString:@"ACTION_LOGIN"];
            if([secondSplit count] >= 2) {
                NSArray *thirdSplit = [[secondSplit objectAtIndex:1] componentsSeparatedByString:@"BACKURL="];
                if([thirdSplit count] >= 2) {
                    backUrl = [thirdSplit objectAtIndex:1];
                    [AppDelegate sharedAppDelegate].rwd_url = backUrl;
                    if([backUrl length] == 0) { //回登入頁
                        [self closeLoadingView];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoButton5" object:nil userInfo:nil];
                    } else {
                        [self showLoginView];   //登入rwd
                    }
                }
                return;
            }
            //
            secondSplit = [[firstSplit objectAtIndex:1] componentsSeparatedByString:@"AUTO_LOGOUT"];
            if([secondSplit count] >= 2) {
                NSArray *thirdSplit = [[secondSplit objectAtIndex:1] componentsSeparatedByString:@"BACKURL="];
                if([thirdSplit count] >= 2) {
                    [[Tools getInstance] setLogOut];
                    [AppDelegate sharedAppDelegate].rwd_url = backUrl;
                    [self loadWebView];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_4" object:nil userInfo:nil];
                    [self showAlertMessage:@"訊息" message:@"會員帳號已登出！" tag:0];
                    //backUrl = [thirdSplit objectAtIndex:1];
                    //[self showLogoutAlertMessage];
                }
                return;
            }
            //
            secondSplit = [[firstSplit objectAtIndex:1] componentsSeparatedByString:@"RENEWTICKET"];
            if([secondSplit count] >= 2) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoButton4" object:nil userInfo:nil];
                return;
            }
            //
            secondSplit = [[firstSplit objectAtIndex:1] componentsSeparatedByString:@"AUTO_LOGIN"];
            if([secondSplit count] >= 2) {
                NSArray *thirdSplit = [[secondSplit objectAtIndex:1] componentsSeparatedByString:@"BACKURL="];
                if([thirdSplit count] >= 2) {
                    backUrl = [thirdSplit objectAtIndex:1];
                    if([backUrl length] == 0) { //回登入頁
                        [self closeLoadingView];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"GotoButton5" object:nil userInfo:nil];
                    } else {
                        [self showLoginView];   //登入rwd
                    }
                }
                return;
            }
            //
            secondSplit = [[firstSplit objectAtIndex:1] componentsSeparatedByString:@"LOGOUT"];
            if([secondSplit count] >= 2) {
                NSArray *thirdSplit = [[secondSplit objectAtIndex:1] componentsSeparatedByString:@"BACKURL="];
                if([thirdSplit count] >= 2) {
                    [[Tools getInstance] setLogOut];
                    [self loadWebView];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"Disable_TAB_4" object:nil userInfo:nil];
                    [self showAlertMessage:@"訊息" message:@"會員帳號已登出！" tag:0];
                    //backUrl = [thirdSplit objectAtIndex:1];
                    //[self showLogoutAlertMessage];
                }
                return;
            }
            secondSplit = [[firstSplit objectAtIndex:1] componentsSeparatedByString:@"OPENBROWSER"];
            if([secondSplit count] >= 2) {
                NSArray *thirdSplit = [[secondSplit objectAtIndex:1] componentsSeparatedByString:@"URL="];
                if([thirdSplit count] >= 2) {
                    NSString *urlStr = [thirdSplit objectAtIndex:1];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr] options:@{} completionHandler:nil];
                }
                return;
            }
        }
    } @catch (NSException *exception) {
    }
}

- (void)showBindAccountView:(NSString *)msg {
    loginAlertView = nil;
    accountTextField = nil;
    passwordTextField = nil;
    loginAlertView = [[CustomIOSAlertView alloc] init];
    [loginAlertView setButtonTitles:[NSArray arrayWithObjects:@"取消", @"綁定", nil]];
    [loginAlertView setDelegate:self];
    loginAlertView.tag = 40;
    float alert_view_width = DEVICE_WIDTH - 60;
    UIView *containerView = [[UIView alloc] init];
    pos_x = 20;
    pos_y = 5;
    height = 40;
    width = alert_view_width - (pos_x * 2);
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [titleLabel setText:@"會員綁定"];
    [titleLabel setTextColor:[UIColor messageAlertViewTitleColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:ALERT_VIEW_TITLE_FONT_SIZE]];
    [containerView addSubview:titleLabel];
    pos_x = 0;
    pos_y = titleLabel.frame.origin.y + titleLabel.frame.size.height;
    width = alert_view_width;
    height = 2;
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    [separator setBackgroundColor:[UIColor messageAlertViewTitleColor]];
    [containerView addSubview:separator];
    pos_x = titleLabel.frame.origin.x;
    pos_y = separator.frame.origin.y + separator.frame.size.height + 5;
    width = titleLabel.frame.size.width;
    height = 60;
    UILabel *accountLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
    //NSString *msg = @"該社群帳號尚未綁定，欲綁定社群帳號請輸入TixFun的登入帳號及密碼。";
    [accountLabel setTextColor:[UIColor blackColor]];
    [accountLabel setTextAlignment:NSTextAlignmentLeft];
    [accountLabel setFont:[UIFont systemFontOfSize:18]];
    height = [[Tools getInstance] text:msg heightWithFontSize:18 width:width lineSpacing:CELL_TEXT_LINE_SPACE];
    [accountLabel setText:msg];
    [accountLabel setNumberOfLines:0];
    [accountLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [containerView addSubview:accountLabel];
    pos_x = 10;
    pos_y = accountLabel.frame.origin.y + accountLabel.frame.size.height + 10;
    width = alert_view_width - (pos_x * 2);
    height = 40;
    accountTextField = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
    [accountTextField setPlaceholder:@"帳號"];
    [accountTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [accountTextField setKeyboardType:UIKeyboardTypeEmailAddress];
    [accountTextField setDelegate:self];
    [accountTextField addTarget:self action:@selector(doEditFieldDone:) forControlEvents:UIControlEventEditingDidEndOnExit];
    passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(accountTextField.frame.origin.x, accountTextField.frame.origin.y + accountTextField.frame.size.height + 10, accountTextField.frame.size.width, accountTextField.frame.size.height)];
    [passwordTextField setPlaceholder:@"密碼"];
    [passwordTextField setBorderStyle:UITextBorderStyleRoundedRect];
    [passwordTextField setSecureTextEntry:YES];
    passwordTextField.returnKeyType = UIReturnKeyNext;
    [passwordTextField setDelegate:self];
    [passwordTextField addTarget:self action:@selector(doEditFieldDone:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [containerView addSubview:accountTextField];
    [containerView addSubview:passwordTextField];
    width = DEVICE_WIDTH - 60;
    height = passwordTextField.frame.origin.y + passwordTextField.frame.size.height + 20;
    [containerView setFrame:CGRectMake(0, 0, width, height)];
    [loginAlertView setContainerView:containerView];
    [loginAlertView show];
}


- (void)showLoginView {
    if(loginAlertView == nil) {
        loginAlertView = [[CustomIOSAlertView alloc] init];
        [loginAlertView setButtonTitles:[NSArray arrayWithObjects:@"取消", @"確定", nil]];
        [loginAlertView setDelegate:self];
        float alert_view_width = DEVICE_WIDTH - 60;
        UIView *containerView = [[UIView alloc] init];
        pos_x = 20;
        pos_y = 5;
        height = 40;
        width = alert_view_width - (pos_x * 2);
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        [titleLabel setText:@"會員登入"];
        [titleLabel setTextColor:[UIColor messageAlertViewTitleColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setFont:[UIFont boldSystemFontOfSize:ALERT_VIEW_TITLE_FONT_SIZE]];
        [containerView addSubview:titleLabel];
        pos_x = 0;
        pos_y = titleLabel.frame.origin.y + titleLabel.frame.size.height;
        width = alert_view_width;
        height = 2;
        UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        [separator setBackgroundColor:[UIColor messageAlertViewTitleColor]];
        [containerView addSubview:separator];
        pos_x = titleLabel.frame.origin.x;
        pos_y = separator.frame.origin.y + separator.frame.size.height + 5;
        width = titleLabel.frame.size.width;
        height = 30;
        UILabel *accountLabel = [[UILabel alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
        [accountLabel setText:@"請使用會員登入"];
        [accountLabel setTextColor:[UIColor messageAlertViewTitleColor]];
        [accountLabel setNumberOfLines:0];
        [accountLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [containerView addSubview:accountLabel];
        pos_x = 10;
        pos_y = accountLabel.frame.origin.y + accountLabel.frame.size.height + 10;
        width = alert_view_width - (pos_x * 2);
        height = 40;
        accountTextField = [[UITextField alloc] initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
        accountTextField.textColor = [UIColor blackColor];
        accountTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"帳號" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        accountTextField.backgroundColor = [UIColor whiteColor];
        [accountTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [accountTextField setKeyboardType:UIKeyboardTypeEmailAddress];
        [accountTextField setDelegate:self];
        [accountTextField addTarget:self action:@selector(doEditFieldDone:) forControlEvents:UIControlEventEditingDidEndOnExit];
        passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(accountTextField.frame.origin.x, accountTextField.frame.origin.y + accountTextField.frame.size.height + 10, accountTextField.frame.size.width, accountTextField.frame.size.height)];
        passwordTextField.textColor = [UIColor blackColor];
        passwordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"密碼" attributes:@{NSForegroundColorAttributeName: [UIColor lightGrayColor]}];
        passwordTextField.backgroundColor = [UIColor whiteColor];
        [passwordTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [passwordTextField setSecureTextEntry:YES];
        passwordTextField.returnKeyType = UIReturnKeyNext;
        [passwordTextField setDelegate:self];
        [passwordTextField addTarget:self action:@selector(doEditFieldDone:) forControlEvents:UIControlEventEditingDidEndOnExit];
        [containerView addSubview:accountTextField];
        [containerView addSubview:passwordTextField];
        pos_x = 10;
        pos_y = passwordTextField.frame.origin.y + passwordTextField.frame.size.height + 10;
        width = passwordTextField.frame.size.width;
        if(@available(iOS 13.0, *)) {
            height = 44;
            fsignInButton = [[FBSDKLoginButton alloc]  initWithFrame:CGRectMake(pos_x, pos_y, width, height)];
            [fsignInButton setAttributedTitle:[[NSAttributedString alloc] initWithString:@"使用 facebook 登入"]
                                    forState:UIControlStateNormal];
            fsignInButton.permissions = @[@"public_profile", @"email"];
            fsignInButton.delegate = self;
            [containerView addSubview:fsignInButton];
            //google login
            [GIDSignInButton class];
            signIn = [GIDSignIn sharedInstance];
            signIn.shouldFetchBasicProfile = YES;
            signIn.delegate = self;
            [[GIDSignIn sharedInstance] signOut];
            pos_x = fsignInButton.frame.origin.x;
            pos_y = fsignInButton.frame.origin.y + fsignInButton.frame.size.height + 5;
            height = fsignInButton.frame.size.height;
            width = fsignInButton.frame.size.width;
            gsignInButton = [[GIDSignInButton alloc] initWithFrame:CGRectMake(pos_x,pos_y,width,height)];
            [gsignInButton setStyle:kGIDSignInButtonStyleWide];
            [containerView addSubview:gsignInButton];
            //apple id
            pos_x = gsignInButton.frame.origin.x;
            pos_y = gsignInButton.frame.origin.y + gsignInButton.frame.size.height + 5;
            height = gsignInButton.frame.size.height;
            width = gsignInButton.frame.size.width;
            ASAuthorizationAppleIDButton *appleIDButton = [ASAuthorizationAppleIDButton new];
            appleIDButton.frame =  CGRectMake(pos_x, pos_y, width, height);
            [containerView addSubview:appleIDButton];
            //[appleIDButton addTarget:self action:@selector(handleAuthrization:) forControlEvents:UIControlEventTouchUpInside];
            [appleIDButton addTarget:self action:@selector(startSignInWithAppleFlow) forControlEvents:UIControlEventTouchUpInside];
            pos_x = appleIDButton.frame.origin.x;
            pos_y = appleIDButton.frame.origin.y + appleIDButton.frame.size.height + 15;
        } else {
            // Fallback on earlier versions
            //pos_x = 10;
            //pos_y = passwordTextField.frame.origin.y + passwordTextField.frame.size.height + 10;
        }
          width = accountTextField.frame.size.width / 2;
          height = 30;
          UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
          registerBtn.tag = 7;
          registerBtn.frame = CGRectMake(pos_x, pos_y, width, height);
          [registerBtn setTitle:@"註冊新會員" forState:UIControlStateNormal];
          [registerBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
          registerBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
          [registerBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
          registerBtn.layer.borderWidth = 0;
          registerBtn.backgroundColor = [UIColor clearColor];
          [registerBtn addTarget:self action:@selector(pressRegisterBtn) forControlEvents:UIControlEventTouchUpInside];
          registerBtn.enabled = YES;
          [containerView addSubview:registerBtn];
          pos_x = registerBtn.frame.origin.x + registerBtn.frame.size.width;
          pos_y = registerBtn.frame.origin.y;
          width = registerBtn.frame.size.width;
          height = registerBtn.frame.size.height;
          UIButton *forgetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
          forgetBtn.tag = 8;
          forgetBtn.frame = CGRectMake(pos_x, pos_y, width, height);
          [forgetBtn setTitle:@"忘記密碼" forState:UIControlStateNormal];
          [forgetBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
          forgetBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
          [forgetBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
          forgetBtn.layer.borderWidth = 0;
          forgetBtn.backgroundColor = [UIColor clearColor];
          [forgetBtn addTarget:self action:@selector(pressForgetBtn) forControlEvents:UIControlEventTouchUpInside];
          forgetBtn.enabled = YES;
          [containerView addSubview:forgetBtn];
          width = DEVICE_WIDTH - 60;
          height = registerBtn.frame.origin.y + registerBtn.frame.size.height + 20;
          [containerView setFrame:CGRectMake(0, 0, width, height)];
          loginAlertView.tag = 30;
          [loginAlertView setContainerView:containerView];
          [loginAlertView show];
        
        
       // [self startSignInWithAppleFlow];
     }
      
}

- (void)pressRegisterBtn {
    [self closeLoginView];
    [AppDelegate sharedAppDelegate].rwd_url = REGISTER_URL;
    [self loadWebView];
    
}

- (void)pressForgetBtn {
    [self closeLoginView];
    [AppDelegate sharedAppDelegate].rwd_url = FORGET_URL;
    [self loadWebView];

    
}

- (void)showLogoutAlertMessage {
    [self closeLoadingView];
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setDelegate:self];
    [alertView setTag:10];
    [[MessageAlertView getInstance] showAlertView:alertView withTitleMessage:@"登出" andContentMessage:@"是否登出" buttonsTitleArray:[NSArray arrayWithObjects:@"取消",@"確定", nil]];
}

//顯示正在登入訊息
- (void)showLoadingView:(NSString *)message {
     dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if(!self->waitingView) {
                self->waitingView = [MBProgressHUD showHUDAddedTo:[AppDelegate sharedAppDelegate].window animated:YES];
                self->waitingView.mode = MBProgressHUDModeIndeterminate;
                self->waitingView.label.text = message;
            } else {
               // self->waitingView.label.text = message;
            }
        });
    });
}

- (void)closeLoadingView {
    isLoading = NO;
    [waitingView hideAnimated:YES];
    waitingView = nil;
}

- (void)timeout{
    [AppDelegate sharedAppDelegate].is_home_loading = NO;
    [_homePageWebView stopLoading];
    [self closeLoadingView];
    [timer invalidate];
    timer= nil;
    [self showErrorView];
}



- (void)closeLoginView {
    
    [loginAlertView close];
    [accountTextField removeFromSuperview];
    accountTextField = nil;
    [passwordTextField removeFromSuperview];
    passwordTextField = nil;
    [loginAlertView removeFromSuperview];
    loginAlertView = nil;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    [self showLoadingView:@""];
    [timer invalidate];
    timer= nil;
    timer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(timeout) userInfo:nil repeats:NO];
    
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
    
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    [AppDelegate sharedAppDelegate].is_home_loading = NO;
     [timer invalidate];
    [self closeLoadingView];

}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    if([error code] == NSURLErrorCancelled) {
        [self closeLoadingView];
        return;
    }
}

// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    [AppDelegate sharedAppDelegate].is_home_loading = NO;
    //NSLog(@"webView.URL = %@",webView.URL);

}
// 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation{
    
}
// 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
   // NSLog(@"%@",navigationResponse.response.URL.absoluteString);
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    //不允许跳转
    //decisionHandler(WKNavigationResponsePolicyCancel);
}
// 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    //NSLog(@"URL=%@",navigationAction.request.URL.absoluteString);
    NSString *requestString = [navigationAction.request.URL.absoluteString uppercaseString];
    if([requestString rangeOfString:@"STATICXX.FACEBOOK.COM"].location != NSNotFound) {
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    if([requestString rangeOfString:@"FACEBOOK.COM"].location != NSNotFound) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL options:@{} completionHandler:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    if([requestString rangeOfString:@"LINE.NAVER.JP"].location != NSNotFound) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL options:@{} completionHandler:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }

    
/*
    if([requestString rangeOfString:@"SHADOW.UTIKI.COM.TW"].location == NSNotFound && [requestString rangeOfString:@"TICKET.COM.TW"].location == NSNotFound && [requestString rangeOfString:@"ABOUT:BLANK"].location == NSNotFound && [requestString rangeOfString:@"YOUTUBE.COM"].location == NSNotFound && [requestString rangeOfString:@"FACEBOOK.COM"].location != NSNotFound) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL options:@{} completionHandler:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
*/
    if([requestString rangeOfString:@"APPACTION?"].location != NSNotFound) {
        [AppDelegate sharedAppDelegate].is_home_loading = NO;
        [webView stopLoading];
        [self closeLoadingView];
        [self parser_cmd:navigationAction.request.URL.absoluteString];
        //不允许跳转iiuytruhyewqdfd
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    
    decisionHandler(WKNavigationActionPolicyAllow);
}
#pragma mark - WKUIDelegate
// 创建一个新的WebView
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    

    if (navigationAction.request.URL) {
        NSURL *url = navigationAction.request.URL;
        NSString *urlPath = url.absoluteString;
        if ([urlPath rangeOfString:@"https://"].location != NSNotFound || [urlPath rangeOfString:@"http://"].location != NSNotFound) {
            NSString *str = [urlPath uppercaseString];
            if([str rangeOfString:@".PDF"].location != NSNotFound) {
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
            } else {
                [webView loadRequest:[NSURLRequest requestWithURL:url]];

            }
        }
    }
    // --- 解决Crash: reason: 'Returned WKWebView was not created with the given configuration.'
    return nil;

    
    
   // return [[WKWebView alloc]init];
}
// 输入框
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * __nullable result))completionHandler{
    completionHandler(@"http");
}
// 确认框
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler{
    completionHandler(YES);
}
// 警告框
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    //NSLog(@"%@",message);
    [self showAlertMessage:@"提示訊息" message:message tag:0];
    completionHandler();
}

- (void)showAlertMessage:(NSString *)title message:(NSString *)message tag:(NSInteger)tag {
    [self closeLoadingView];
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setDelegate:self];
    [alertView setTag:tag];
    [[MessageAlertView getInstance] showAlertView:alertView withTitleMessage:title andContentMessage:message buttonsTitleArray:[NSArray arrayWithObjects:@"確定", nil]];
}

#pragma CustomIOS7AlertViewDelegate
- (void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSInteger tag = [alertView tag];
    
    switch (tag) {
        case 10:  //登出
            if(buttonIndex == 1) {
                [AppDelegate sharedAppDelegate].rwd_url = backUrl;
                //[self logOut];
            }
            break;
        case 30: //登入
            if(buttonIndex == 1) {
                if([self checkField] == YES) {
                    [[Tools getInstance] setUserData:@"web" key:AUTH_TYPE];
                    [self checkAccountFromServer_web];
                } else
                    [self showAlertMessage:@"錯誤訊息" message:@"帳號或密碼不得為空！" tag:0];
            } else {
            }
            [loginAlertView close];
            loginAlertView = nil;
            break;
        case 40: //綁定
            if(buttonIndex == 1) {
                if([self checkField] == YES) {
                    [[Tools getInstance] setUserData:@"web" key:AUTH_TYPE];
                    [self bindAccountFromServer_apple:apple_user_id];
                } else
                    [self showAlertMessage:@"錯誤訊息" message:@"帳號或密碼不得為空！" tag:0];
            } else {
            }
            [loginAlertView close];
            loginAlertView = nil;
            break;
    }
    [(CustomIOSAlertView *)alertView close];
}

- (BOOL)checkField {
    
    //accountTextField.text = @"weiyu.chen@aceraeb.com";
    //accountTextField.text = @"cillinchen@gmail.com";
    //passwordTextField.text = @"123456";
    //return YES;
    
    NSString *u = [accountTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *p = [passwordTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if(u.length > 0 && p.length > 0)
        return YES;
    return NO;
}

- (void)checkAccountFromServer_fb:(NSString *)user_id {
    loginStatus = @"";
    [self showLoadingView:@""];
     NSString *urlString =[NSString stringWithFormat:@"%@/DI0102_", SERVER_URL];
    //[_manager invalidateSessionCancelingTasks:YES];
    
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //_manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:15];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    [params setObject:aesEncryptString(@"fb", AES_KEY) forKey:@"authtype"];
    [params setObject:aesEncryptString(user_id, AES_KEY) forKey:@"gid"];
    [params setObject:aesEncryptString([AppDelegate sharedAppDelegate].FCM_token, AES_KEY) forKey:@"registration_ids"];

    
    [_manager POST:urlString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self closeLoadingView];
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status = [dd objectForKey:@"status"];
        NSString *token = [dd objectForKey:@"interface_token_id"];
        NSString *name = [dd objectForKey:@"name"];
        NSString *account = [dd objectForKey:@"account"];
        NSString *message = [dd objectForKey:@"message"];

        if([status isEqualToString:@"S000"] == YES) { //登入成功
            [[Tools getInstance] setUserData:@"Y" key:ISLOGIN];
            [[Tools getInstance] setUserData:token key:TOKENID];
            [[Tools getInstance] setUserData:name key:NAME];
            [[Tools getInstance] setUserData:account key:ACCOUNT];
            [[Tools getInstance] setUserData:user_id key:USER_ID];
            [[Tools getInstance] setUserData:@"fb" key:AUTH_TYPE];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_4" object:nil userInfo:nil];
            [self loadWebView];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showErrorView];
    }];
}

- (void)checkAccountFromServer_google:(NSString *)user_id {
    loginStatus = @"";
    [self showLoadingView:@""];
     NSString *urlString =[NSString stringWithFormat:@"%@/DI0102_", SERVER_URL];
    //[_manager invalidateSessionCancelingTasks:YES];
    
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //_manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:15];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    [params setObject:aesEncryptString(@"google", AES_KEY) forKey:@"authtype"];
    [params setObject:aesEncryptString(user_id, AES_KEY) forKey:@"gid"];
    [params setObject:aesEncryptString([AppDelegate sharedAppDelegate].FCM_token, AES_KEY) forKey:@"registration_ids"];
    
    [_manager POST:urlString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self closeLoadingView];
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status = [dd objectForKey:@"status"];
        NSString *token = [dd objectForKey:@"interface_token_id"];
        NSString *name = [dd objectForKey:@"name"];
        NSString *account = [dd objectForKey:@"account"];
        NSString *message = [dd objectForKey:@"message"];
        if([status isEqualToString:@"S000"] == YES) { //登入成功
            [[Tools getInstance] setUserData:@"Y" key:ISLOGIN];
            [[Tools getInstance] setUserData:token key:TOKENID];
            [[Tools getInstance] setUserData:name key:NAME];
            [[Tools getInstance] setUserData:account key:ACCOUNT];
            [[Tools getInstance] setUserData:user_id key:USER_ID];
            [[Tools getInstance] setUserData:@"google" key:AUTH_TYPE];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_4" object:nil userInfo:nil];
            [self loadWebView];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showErrorView];
    }];
}

- (void)bindAccountFromServer_apple:(NSString *)user_id {
    loginStatus = @"";
    [self showLoadingView:@""];
    NSString *u = [accountTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *urlString =[NSString stringWithFormat:@"%@/DI0103_", SERVER_URL];
    
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //_manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:15];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    [params setObject:aesEncryptString(u, AES_KEY) forKey:@"account"];
    [params setObject:aesEncryptString(passwordTextField.text, AES_KEY) forKey:@"password"];
    [params setObject:aesEncryptString(@"apple", AES_KEY) forKey:@"authtype"];
    [params setObject:aesEncryptString(user_id, AES_KEY) forKey:@"gid"];
    [params setObject:aesEncryptString([AppDelegate sharedAppDelegate].FCM_token, AES_KEY) forKey:@"registration_ids"];

    
    [_manager POST:urlString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self closeLoadingView];
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status = [dd objectForKey:@"status"];
        NSString *token = [dd objectForKey:@"interface_token_id"];
        NSString *name = [dd objectForKey:@"name"];
        NSString *account = [dd objectForKey:@"account"];
        NSString *message = [dd objectForKey:@"message"];
        if([status isEqualToString:@"S000"] == YES) { //登入成功
            [[Tools getInstance] setUserData:@"Y" key:ISLOGIN];
            [[Tools getInstance] setUserData:token key:TOKENID];
            [[Tools getInstance] setUserData:name key:NAME];
            [[Tools getInstance] setUserData:account key:ACCOUNT];
            [[Tools getInstance] setUserData:self->passwordTextField.text key:PWD];
            [[Tools getInstance] setUserData:@"web" key:AUTH_TYPE];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_4" object:nil userInfo:nil];
            [self loadWebView];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showErrorView];
    }];

    
    
}

- (void)checkAccountFromServer_apple:(NSString *)user_id {
    loginStatus = @"";
    [self showLoadingView:@""];
     NSString *urlString =[NSString stringWithFormat:@"%@/DI0102_", SERVER_URL];
    //[_manager invalidateSessionCancelingTasks:YES];
    
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //_manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:15];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    [params setObject:aesEncryptString(@"apple", AES_KEY) forKey:@"authtype"];
    [params setObject:aesEncryptString(user_id, AES_KEY) forKey:@"gid"];
    [params setObject:aesEncryptString([AppDelegate sharedAppDelegate].FCM_token, AES_KEY) forKey:@"registration_ids"];
    
    [_manager POST:urlString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self closeLoadingView];
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status = [dd objectForKey:@"status"];
        NSString *token = [dd objectForKey:@"interface_token_id"];
        NSString *name = [dd objectForKey:@"name"];
        NSString *account = [dd objectForKey:@"account"];
        NSString *message = [dd objectForKey:@"message"];
        
        if([status isEqualToString:@"S000"] == YES) { //登入成功
            [[Tools getInstance] setUserData:@"Y" key:ISLOGIN];
            [[Tools getInstance] setUserData:token key:TOKENID];
            [[Tools getInstance] setUserData:name key:NAME];
            [[Tools getInstance] setUserData:account key:ACCOUNT];
            [[Tools getInstance] setUserData:user_id key:USER_ID];
            [[Tools getInstance] setUserData:@"apple" key:AUTH_TYPE];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_4" object:nil userInfo:nil];
            [self loadWebView];
        }else if([status isEqualToString:@"F006-4"] == YES) {
            [self showBindAccountView:message];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showErrorView];
    }];
}


- (void)checkAccountFromServer_web {
    loginStatus = @"";
    [self showLoadingView:@""];
    NSString *u = [accountTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
     NSString *urlString =[NSString stringWithFormat:@"%@/DI0102_", SERVER_URL];
    //[_manager invalidateSessionCancelingTasks:YES];
    
    [_manager invalidateSessionCancelingTasks:YES resetSession:YES];
    
    _manager = nil;
    _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    //_manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager.requestSerializer setTimeoutInterval:15];  //Time out after 10 seconds
    _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:aesEncryptString(BF_CODE, AES_KEY)  forKey:@"bf_code"];
    [params setObject:aesEncryptString(PLATFORM_CODE, AES_KEY)  forKey:@"platform_code"];
    [params setObject:aesEncryptString(TICKET_VERSION, AES_KEY)  forKey:@"app_version"];
    [params setObject:aesEncryptString(@"1.0", AES_KEY)  forKey:@"api_ver"];
    [params setObject:aesEncryptString(@"ios", AES_KEY)  forKey:@"mobile_type"];
    [params setObject:aesEncryptString(SYSTEM_VERSION, AES_KEY)  forKey:@"os_version"];
    NSString *udid = [[Tools getInstance] getRequest_id];
    [params setObject:aesEncryptString(udid, AES_KEY) forKey:@"request_id"];
    [params setObject:aesEncryptString(u, AES_KEY) forKey:@"account"];
    [params setObject:aesEncryptString(passwordTextField.text, AES_KEY) forKey:@"password"];
    [params setObject:aesEncryptString(@"web", AES_KEY) forKey:@"authtype"];
    [params setObject:aesEncryptString([AppDelegate sharedAppDelegate].FCM_token, AES_KEY) forKey:@"registration_ids"];

    
    [_manager POST:urlString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self closeLoadingView];
        NSDictionary *dd = (NSDictionary *)responseObject;
        NSString *status = [dd objectForKey:@"status"];
        NSString *token = [dd objectForKey:@"interface_token_id"];
        NSString *name = [dd objectForKey:@"name"];
        NSString *account = [dd objectForKey:@"account"];
        NSString *message = [dd objectForKey:@"message"];
        if([status isEqualToString:@"S000"] == YES) { //登入成功
            [[Tools getInstance] setUserData:@"Y" key:ISLOGIN];
            [[Tools getInstance] setUserData:token key:TOKENID];
            [[Tools getInstance] setUserData:name key:NAME];
            [[Tools getInstance] setUserData:account key:ACCOUNT];
            [[Tools getInstance] setUserData:self->passwordTextField.text key:PWD];
            [[Tools getInstance] setUserData:@"web" key:AUTH_TYPE];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Enable_TAB_4" object:nil userInfo:nil];
            [self loadWebView];
        } else {
            [self showAlertMessage:@"錯誤訊息" message:message tag:0];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self closeLoadingView];
        [self showErrorView];
    }];
}

- (IBAction)doEditFieldDone:(id)sender {
    [sender resignFirstResponder];
}

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error {
    
    if (error) {
        // Process error
    }
    else if (result.isCancelled) {
        // Handle cancellations
    }
    else {
        // Navigate to other view
        FBSDKAccessToken *token = [FBSDKAccessToken currentAccessToken];
        NSString *userID = token.userID;
        //NSString *appID = token.appID;
        //[[Tools getInstance] setUserData:@"fb" key:AUTH_TYPE];
        //[[Tools getInstance] setUserData:userID key:USER_ID];
/*
        if ([FBSDKAccessToken currentAccessToken]) {
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me?fields=id,name,email" parameters:nil]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 
                 NSDictionary *dd = (NSDictionary *)result;

                 if (!error) {
                     NSLog(@"fetched user------------------------------------------------------------:%@", result);
                 }
             }];
        }
*/
        NSLog(@"Login");
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        [loginAlertView close];
        loginAlertView = nil;
        [self checkAccountFromServer_fb:userID];
    }
    
}


#pragma mark - GIDSignInDelegate
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    if (error) {
        //   _signInAuthStatus.text = [NSString stringWithFormat:@"Status: Authentication error: %@", error];
        return;
    } else {
        [[GIDSignIn sharedInstance] signOut];
        
        //[[Tools getInstance] setUserData:@"google" key:AUTH_TYPE];
        //[[Tools getInstance] setUserData:user.userID key:USER_ID];
        [loginAlertView close];
        loginAlertView = nil;
        //[AppDelegate sharedAppDelegate].loginType = @"google";
        //[AppDelegate sharedAppDelegate].gid = user.userID;
        //isFbLogin = YES;
        //[self getSystemTime:2];
    }
    [[GIDSignIn sharedInstance] disconnect];
    [self checkAccountFromServer_google:user.userID];

}


- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    // [myActivityIndicator stopAnimating];
    NSLog(@"OK");
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)dealloc {

    if (@available(iOS 13.0, *)) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
    }
}


// for firebase apple sign in

- (NSString *)randomNonce:(NSInteger)length {
  NSAssert(length > 0, @"Expected nonce to have positive length");
  NSString *characterSet = @"0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._";
  NSMutableString *result = [NSMutableString string];
  NSInteger remainingLength = length;

  while (remainingLength > 0) {
    NSMutableArray *randoms = [NSMutableArray arrayWithCapacity:16];
    for (NSInteger i = 0; i < 16; i++) {
      uint8_t random = 0;
      int errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random);
      NSAssert(errorCode == errSecSuccess, @"Unable to generate nonce: OSStatus %i", errorCode);

      [randoms addObject:@(random)];
    }

    for (NSNumber *random in randoms) {
      if (remainingLength == 0) {
        break;
      }

      if (random.unsignedIntValue < characterSet.length) {
        unichar character = [characterSet characterAtIndex:random.unsignedIntValue];
        [result appendFormat:@"%C", character];
        remainingLength--;
      }
    }
  }

  return result;
}

- (void)startSignInWithAppleFlow  API_AVAILABLE(ios(13.0)) {
  NSString *nonce = [self randomNonce:32];
  self.currentNonce = nonce;
  ASAuthorizationAppleIDProvider *appleIDProvider = [[ASAuthorizationAppleIDProvider alloc] init];
  ASAuthorizationAppleIDRequest *request = [appleIDProvider createRequest];
  request.requestedScopes = @[ASAuthorizationScopeFullName, ASAuthorizationScopeEmail];
  request.nonce = [self stringBySha256HashingString:nonce];

  ASAuthorizationController *authorizationController =
      [[ASAuthorizationController alloc] initWithAuthorizationRequests:@[request]];
  authorizationController.delegate = self;
  authorizationController.presentationContextProvider = self;
  [authorizationController performRequests];
}

- (NSString *)stringBySha256HashingString:(NSString *)input {
  const char *string = [input UTF8String];
  unsigned char result[CC_SHA256_DIGEST_LENGTH];
  CC_SHA256(string, (CC_LONG)strlen(string), result);

  NSMutableString *hashed = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
  for (NSInteger i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
    [hashed appendFormat:@"%02x", result[i]];
  }
  return hashed;
}

- (void)authorizationController:(ASAuthorizationController *)controller
   didCompleteWithAuthorization:(ASAuthorization *)authorization API_AVAILABLE(ios(13.0)) {
  if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]]) {
    ASAuthorizationAppleIDCredential *appleIDCredential = authorization.credential;
    NSString *rawNonce = self.currentNonce;
    //NSAssert(rawNonce != nil, @"Invalid state: A login callback was received, but no login request was sent.");

    if (appleIDCredential.identityToken == nil) {
      NSLog(@"Unable to fetch identity token.");
      return;
    }

    NSString *idToken = [[NSString alloc] initWithData:appleIDCredential.identityToken
                                              encoding:NSUTF8StringEncoding];
    if (idToken == nil) {
      NSLog(@"Unable to serialize id token from data: %@", appleIDCredential.identityToken);
    }

    // Initialize a Firebase credential.
    FIROAuthCredential *credential = [FIROAuthProvider credentialWithProviderID:@"apple.com"
                                                                        IDToken:idToken
                                                                       rawNonce:rawNonce];

    // Sign in with Firebase.
    [[FIRAuth auth] signInWithCredential:credential
                              completion:^(FIRAuthDataResult * _Nullable authResult,
                                           NSError * _Nullable error) {
      if (error != nil) {
        // Error. If error.code == FIRAuthErrorCodeMissingOrInvalidNonce,
        // make sure you're sending the SHA256-hashed nonce as a hex string
        // with your request to Apple.
        return;
      }
      // Sign-in succeeded!
        FIRUser *user = authResult.user;
        self->apple_user_id = user.uid;
        [self checkAccountFromServer_apple:user.uid];
        [self closeLoginView];
    }];
  }
}

- (void)authorizationController:(ASAuthorizationController *)controller
           didCompleteWithError:(NSError *)error API_AVAILABLE(ios(13.0)) {
  NSLog(@"Sign in with Apple errored: %@", error);
}

@end
