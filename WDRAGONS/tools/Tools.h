//
//  Tools.h
//  CustomerServices
//
//  Created by 陳威宇 on 2017/8/3.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CustomIOSAlertView.h"
#import "ConstantData.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "UILabel+Utils.h"
#import <sys/sysctl.h>
#import "AESCipher.h"

@interface Tools : NSObject
+ (Tools *)getInstance;

- (void)setUserData:(NSString *)value key:(NSString *)key;
- (NSString *)getUserData:(NSString *)key;
- (NSMutableDictionary *)getUserInfo;
- (CGSize)getTextSize:(UILabel *)label maxSize:(CGSize)maxSize;
- (CGSize)getTextSize:(NSString *)text withFont:(UIFont *)font maxSize:(CGSize)maxSize;
- (NSString *)getRequest_id;
- (void)setLogOut;
- (NSString *)getUserAccount;

//- (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font width:(NSInteger)width;
//- (CustomIOSAlertView *)showAlertMessage:(NSString *)title message:(NSString *)message color:(UIColor *)color tag:(NSInteger)tag buttonNumber:(NSInteger)buttonNumber;
//- (NSString *)getDateString:(NSString *)dateFormatStr toIndex:(int)index;
- (CGFloat)text:(NSString*)text heightWithFontSize:(CGFloat)fontSize width:(CGFloat)width lineSpacing:(CGFloat)lineSpacing;
- (NSString *)getQRCodeStringWithPerformanceNo:(NSString *)performanceNo andTicketID:(NSString *)ticketID;
//- (NSString *)getCheckSum:(NSString *)systemTime;
//- (UIImage *)generateBarcode:(NSString*)dataString frame:(CGRect)frame;
//- (NSString *)getaesEncryptString:(NSString *)string;
//- (NSString *)getCheckSum_1:(NSString *)systemTime;

@end
