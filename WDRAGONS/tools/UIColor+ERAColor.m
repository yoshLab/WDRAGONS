//
//  UIColor+ERAColor.m
//  UDNTicket
//
//  Created by Weiyu Chen on 2018/11/23.
//  Copyright (c) 2018年 Weiyu Chen. All rights reserved.
//

#import "UIColor+ERAColor.h"

@implementation UIColor (ERAColor)

+ (UIColor *)messageAlertViewTitleColor {
    return [UIColor colorWithRed:(0x0e/255.0) green:(0x22/255.0) blue:(0x40/255.0) alpha:1.0f];
}

+ (UIColor *)blueButtonBackgroundColor {
    return [UIColor colorWithRed:(0x3e/255.0) green:(0x7e/255.0) blue:(0xe8/255.0) alpha:1.0f];
}

+ (UIColor *)backgroundGrayColor {
    return [UIColor colorWithRed:(0x83/255.0) green:(0x83/255.0) blue:(0x83/255.0) alpha:1.0f];
}

+ (UIColor *)moreAppCellTextColor {
    return [UIColor colorWithRed:64/255.0 green:77/255.0 blue:94/255.0 alpha:1.0f];
}

@end
