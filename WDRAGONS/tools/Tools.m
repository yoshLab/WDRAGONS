//
//  Tools.m
//  CustomerServices
//
//  Created by 陳威宇 on 2017/8/3.
//  Copyright © 2017年 陳威宇. All rights reserved.
//

#import "Tools.h"

static Tools *sTools = nil;

@implementation Tools

+ (Tools *)getInstance {
    if (sTools == nil) {
        sTools  = [[Tools alloc] init];
    }
    
    return sTools;
}

/*
- (CGSize)sizeWithString:(NSString *)string font:(UIFont *)font width:(NSInteger)width {
    CGRect rect = [string boundingRectWithSize:CGSizeMake(width, 8000)//限制最大的宽度和高度
                                       options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesFontLeading  |NSStringDrawingUsesLineFragmentOrigin
                                    attributes:@{NSFontAttributeName: font}
                                       context:nil];
    return rect.size;
}


//顯示警告訊息
- (CustomIOSAlertView *)showAlertMessage:(NSString *)title message:(NSString *)message color:(UIColor *)color tag:(NSInteger)tag buttonNumber:(NSInteger)buttonNumber {
    UIView *containerAlertView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 130)];
    CGSize size = [[Tools getInstance] sizeWithString:message font:[UIFont systemFontOfSize:20] width:290];
    if(size.height < 80)
        size.height = size.height + 40;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 290 - 20, 20)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.text = title;
    [containerAlertView addSubview:titleLabel];
    UILabel *bodyMsg = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, 290 - 20, size.height)];
    bodyMsg.textAlignment = NSTextAlignmentLeft;
    bodyMsg.font = [UIFont systemFontOfSize:18];
    bodyMsg.lineBreakMode = NSLineBreakByWordWrapping;
    bodyMsg.numberOfLines = 0;
    
    bodyMsg.textColor = color;
    bodyMsg.text = message;
    [containerAlertView addSubview:bodyMsg];
    CGRect newFrame = containerAlertView.frame;
    newFrame.size.height = bodyMsg.frame.origin.y + bodyMsg.frame.size.height + 30;
    [containerAlertView setFrame:newFrame];
    CustomIOSAlertView *customAlert = [[CustomIOSAlertView alloc] init];
    if(buttonNumber == 1) {
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"確定", nil]];
    } else if(buttonNumber == 2) {
        [customAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"取消",@"確定", nil]];
    }
    [customAlert setTag:tag];
    [customAlert setContainerView:containerAlertView];
    [customAlert setUseMotionEffects:true];
    return customAlert;
}



- (NSString *)getDateString:(NSString *)dateFormatStr toIndex:(int)index {
    dateFormatStr = [dateFormatStr stringByReplacingOccurrencesOfString:@"/" withString:@""];
    dateFormatStr = [dateFormatStr stringByReplacingOccurrencesOfString:@":" withString:@""];
    dateFormatStr = [dateFormatStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    dateFormatStr = [dateFormatStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    dateFormatStr = [dateFormatStr substringToIndex:index];
    return dateFormatStr;
}



- (UIImage *)barCodeImageWithInfo:(NSString *)info {
    // 创建条形码
    CIFilter *filter = [CIFilter filterWithName:@"CIAztecCodeGenerator"];
    // 恢复滤镜的默认属性
    [filter setDefaults];
    // 将字符串转换成NSData
    NSData *data = [info dataUsingEncoding:NSUTF8StringEncoding];
    // 通过KVO设置滤镜inputMessage数据
    [filter setValue:data forKey:@"inputMessage"];
    // 获得滤镜输出的图像
    CIImage *outputImage = [filter outputImage];
    // 将CIImage 转换为UIImage
    UIImage *image = [UIImage imageWithCIImage:outputImage];
    return image;
}

- (NSString *)getCheckSum:(NSString *)systemTime {
    long checkSum = 1;
    NSString *checkNumberStr = @"269612343131";
    NSInteger length = [systemTime length];
    if(length > [checkNumberStr length]) {
        length = [checkNumberStr length];
    }
    for (int i = 0; i < length; i++) {
        char systemTimeChar   = [systemTime characterAtIndex:i];
        int systemTimeCharInt = (int)(systemTimeChar - '0');
        
        char check   = [checkNumberStr characterAtIndex:i];
        int checkInt = (int)(check - '0');
        
        int x = systemTimeCharInt * checkInt;
        if (x != 0)
            checkSum *= x;
    }
    
    NSString *checkSumStr = [NSString stringWithFormat:@"%ld", checkSum];
    checkSumStr = [checkSumStr substringFromIndex:([checkSumStr length] - 3)];
    
    return checkSumStr;
}


- (UIImage *)generateBarcode:(NSString*)dataString frame:(CGRect)frame {
    
    CIFilter *barCodeFilter = [CIFilter filterWithName:@"CICode128BarcodeGenerator"];
    [barCodeFilter setDefaults];
    NSData *barCodeData = [dataString dataUsingEncoding:NSASCIIStringEncoding];
    [barCodeFilter setValue:barCodeData forKey:@"inputMessage"];

    CIImage *outputImage = [barCodeFilter outputImage];
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:outputImage
                                       fromRect:[outputImage extent]];
    UIImage *image = [UIImage imageWithCGImage:cgImage
                                         scale:1.
                                   orientation:UIImageOrientationUp];
    
    // Resize without interpolating
    CGFloat scaleRate = frame.size.width / image.size.width;
    UIImage *resized = [self resizeImage:image
                             withQuality:kCGInterpolationNone
                                    rate:scaleRate];

    return resized;
}

- (UIImage *)resizeImage:(UIImage *)image withQuality:(CGInterpolationQuality)quality rate:(CGFloat)rate {
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}


- (NSString *)getaesEncryptString:(NSString *)string {
    NSString *unescaped = aesEncryptString(string, AES_KEY);
    NSString *unreserved = @"-._~?";
    NSMutableCharacterSet *allowed = [NSMutableCharacterSet
    [allowed addCharactersInString:unreserved];
    NSString *escapedString = [unescaped stringByAddingPercentEncodingWithAllowedCharacters:allowed];
    
    //NSString *escapedString = [unescaped stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return unescaped;
}
*/

- (void)setLogOut {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"N" forKey:ISLOGIN];
    [prefs setObject:@"" forKey:ACCOUNT];
    [prefs setObject:@"" forKey:PWD];
    [prefs setObject:@"" forKey:NAME];
    [prefs setObject:@"" forKey:TOKENID];
    [prefs setObject:@"" forKey:AUTH_TYPE];
    [prefs setObject:@"" forKey:USER_ID];
}

- (void)setUserData:(NSString *)value key:(NSString *)key {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:value forKey:key];
}

- (NSString *)getUserData:(NSString *)key {
    NSString *rtn = @"";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    rtn = [prefs stringForKey:key];
    return rtn;
}

- (NSString *)getRequest_id {
    NSDate *datenow = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:datenow];
    NSDate *localeDate = [datenow dateByAddingTimeInterval:interval];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[localeDate timeIntervalSince1970]];
    NSString *rtn = @"";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    rtn = [prefs stringForKey:UDID];
    NSString *udid = [NSString stringWithFormat:@"%@_%@",rtn,timeSp];
    return udid;
}

- (NSString *)getUserAccount {
    NSString *rtn = @"";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    rtn = [prefs stringForKey:ACCOUNT];
    return rtn;
}

- (NSMutableDictionary *)getUserInfo {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSString *rtn = @"";
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //帳號
    rtn = [prefs stringForKey:ACCOUNT];
    [dict setValue:rtn forKey:ACCOUNT];
    //name
    rtn = [prefs stringForKey:NAME];
    [dict setValue:rtn forKey:NAME];
    //密碼
    rtn = [prefs stringForKey:PWD];
    [dict setValue:rtn forKey:PWD];
    //TOKEN ID
    rtn = [prefs stringForKey:TOKENID];
    [dict setValue:rtn forKey:TOKENID];
    //是否登入
    rtn = [prefs stringForKey:ISLOGIN];
    [dict setValue:rtn forKey:ISLOGIN];
    //登入類別
    rtn = [prefs stringForKey:AUTH_TYPE];
    [dict setValue:rtn forKey:AUTH_TYPE];
    //user id
    rtn = [prefs stringForKey:USER_ID];
    [dict setValue:rtn forKey:USER_ID];
    return dict;
}

- (CGSize)getTextSize:(UILabel *)label maxSize:(CGSize)maxSize {
    return [label sizeThatFits:maxSize];
}

- (CGSize)getTextSize:(NSString *)text withFont:(UIFont *)font maxSize:(CGSize)maxSize {
    NSString *version = [[UIDevice currentDevice] systemVersion];
    if ([version floatValue] >= 7.0)
        return [text boundingRectWithSize:maxSize
                                  options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                               attributes:@{NSFontAttributeName:font}
                                  context:nil].size;
    else
        return [text sizeWithFont:font
                constrainedToSize:maxSize
                    lineBreakMode:NSLineBreakByWordWrapping];
}

- (CGFloat)text:(NSString*)text heightWithFontSize:(CGFloat)fontSize width:(CGFloat)width lineSpacing:(CGFloat)lineSpacing {
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, MAXFLOAT)];
    label.font = [UIFont systemFontOfSize:fontSize];
    label.numberOfLines = 0;
    [label setText:text lineSpacing:lineSpacing];
    [label sizeToFit];
    return label.frame.size.height;
}

- (NSString *)getQRCodeStringWithPerformanceNo:(NSString *)performanceNo andTicketID:(NSString *)ticketID {
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyyMMddHHmmss"];
    NSString *nowTime = [[form stringFromDate:[NSDate date]] substringFromIndex:6];
    
    
    NSString *ticketType = @"C";
    NSString *baseString = [NSString stringWithFormat:@"%@%@%@%@", ticketType, performanceNo, ticketID, nowTime];
    
    long checkSum = 0;
    NSString *checkNumberStr = @"269612343131574385794274218567";
    for (int i = 0; i < [baseString length]; i++) {
        char systemTimeChar   = [baseString characterAtIndex:i];
        int systemTimeCharInt = (int)systemTimeChar;
        if (systemTimeCharInt >= 65 && systemTimeCharInt <= 90)
            systemTimeCharInt -= 55;
        else if (systemTimeCharInt >= 97 && systemTimeCharInt <= 122)
            systemTimeCharInt -= 87;
        else
            systemTimeCharInt -= 48;
        
        char check   = [checkNumberStr characterAtIndex:i];
        int checkInt = (int)(check - '0');
        
        int x = systemTimeCharInt * checkInt;
        checkSum += x;
    }
    checkSum %= 36;
    
    NSString *str = @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSString *checkSumStr = [NSString stringWithFormat:@"%@%@%@%@-%@%@%c", ticketType,BF_CODE, performanceNo, ticketID, nowTime,PLATFORM_CODE, [str characterAtIndex:checkSum]];
    
    return checkSumStr;
}

@end

