//
//  MessageAlertView.m
//  UDNTicket
//
//  Created by Luster NB024 on 2015/4/8.
//  Copyright (c) 2015年 Luster NB024. All rights reserved.
//
#import "MessageAlertView.h"
#import "Tools.h"

@implementation MessageAlertView

static MessageAlertView *sMessageAlertView = nil;

+ (MessageAlertView *)getInstance {
    if (sMessageAlertView == nil)
        sMessageAlertView = [[MessageAlertView alloc] init];
    return sMessageAlertView;
}

- (void)showAlertView:(CustomIOSAlertView *)alertView withTitleMessage:(NSString *)titleMessage buttonsTitleArray:(NSArray *)buttonsArray {
    
    UIView *containerView = [[UIView alloc] init];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, ALERT_VIEW_TITLE_MARGIN_TOP, ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_HEIGHT)];
    [titleLabel setText:titleMessage];
    [titleLabel setTextColor:[UIColor messageAlertViewTitleColor]];
    
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:ALERT_VIEW_TITLE_FONT_SIZE]];
    
    [containerView addSubview:titleLabel];
    [containerView setFrame:CGRectMake(0, 0, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT + 10)];
    
    [alertView setButtonTitles:buttonsArray];
    [alertView setContainerView:containerView];
    [alertView show];
}

- (void)showAlertView:(CustomIOSAlertView *)alertView withTitleMessage:(NSString *)titleMessage andContentMessage:(NSString *)contentMessage buttonsTitleArray:(NSArray *)buttonsArray {
    
    UIView *containerView = [[UIView alloc] init];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, ALERT_VIEW_TITLE_MARGIN_TOP, ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_HEIGHT)];
    [titleLabel setText:titleMessage];
    [titleLabel setTextColor:[UIColor messageAlertViewTitleColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:ALERT_VIEW_TITLE_FONT_SIZE]];
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, 2.0f)];
    [separator setBackgroundColor:[UIColor messageAlertViewTitleColor]];
    
    CGSize textSize = [[Tools getInstance] getTextSize:contentMessage withFont:[UIFont systemFontOfSize:ALERT_VIEW_MESSAGE_FONT_SIZE] maxSize:CGSizeMake(ALERT_VIEW_WIDTH, MAXFLOAT)];
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, separator.frame.origin.y + separator.frame.size.height + 5, ALERT_VIEW_WIDTH, textSize.height)];
    [messageLabel setText:contentMessage];
    [messageLabel setFont:[UIFont systemFontOfSize:ALERT_VIEW_MESSAGE_FONT_SIZE]];
    [messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [messageLabel setTextColor:[UIColor messageAlertViewTitleColor]];
    [messageLabel setNumberOfLines:0];
    
    //weiyu 修改  2015.11.12
    //[messageLabel setTextAlignment:NSTextAlignmentCenter];
    [messageLabel setTextAlignment:NSTextAlignmentLeft];
    
    if (textSize.height > ALERT_VIEW_LABEL_MAX_HEIGHT) {
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, separator.frame.origin.y + separator.frame.size.height + 5, ALERT_VIEW_WIDTH + 2 * ALERT_VIEW_LABEL_MARGIN_LEFT, ALERT_VIEW_LABEL_MAX_HEIGHT)];
        [scrollView setShowsHorizontalScrollIndicator:NO];
        [messageLabel setFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, 0, ALERT_VIEW_WIDTH, textSize.height)];
        [messageLabel setTextAlignment:NSTextAlignmentLeft];
        [scrollView addSubview:messageLabel];
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, textSize.height)];
        
        [containerView addSubview:scrollView];
        [containerView setFrame:CGRectMake(0, 0, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT + separator.frame.size.height + 5 + scrollView.frame.size.height + 10)];
    } else {
        [containerView addSubview:messageLabel];
        [containerView setFrame:CGRectMake(0, 0, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT + separator.frame.size.height + 5 + messageLabel.frame.size.height + 10)];
    }

    [containerView addSubview:titleLabel];
    [containerView addSubview:separator];
//    [containerView addSubview:messageLabel];
//    [containerView setFrame:CGRectMake(0, 0, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT + separator.frame.size.height + 5 + messageLabel.frame.size.height + 10)];
    
    [alertView setButtonTitles:buttonsArray];
    [alertView setContainerView:containerView];
    [alertView show];
}


- (void)showAlertView:(CustomIOSAlertView *)alertView withTitleMessage:(NSString *)titleMessage andContentMessage:(NSString *)contentMessage andandContentMessage:(UIColor *)contentMessageColor buttonsTitleArray:(NSArray *)buttonsArray {
    
    UIView *containerView = [[UIView alloc] init];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, ALERT_VIEW_TITLE_MARGIN_TOP, ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_HEIGHT)];
    [titleLabel setText:titleMessage];
    [titleLabel setTextColor:[UIColor messageAlertViewTitleColor]];
    
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setFont:[UIFont boldSystemFontOfSize:ALERT_VIEW_TITLE_FONT_SIZE]];
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, 0.5f)];
    [separator setBackgroundColor:[UIColor messageAlertViewTitleColor]];
    
    CGSize textSize = [[Tools getInstance] getTextSize:contentMessage withFont:[UIFont systemFontOfSize:ALERT_VIEW_MESSAGE_FONT_SIZE] maxSize:CGSizeMake(ALERT_VIEW_WIDTH, MAXFLOAT)];
    
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, separator.frame.origin.y + separator.frame.size.height + 5, ALERT_VIEW_WIDTH, textSize.height)];
    [messageLabel setText:contentMessage];
    [messageLabel setFont:[UIFont systemFontOfSize:ALERT_VIEW_MESSAGE_FONT_SIZE]];
    [messageLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [messageLabel setNumberOfLines:0];
    //weiyu 修改 2015.11.12
    //[messageLabel setTextAlignment:NSTextAlignmentCenter];
    [messageLabel setTextAlignment:NSTextAlignmentLeft];
    [messageLabel setTextColor:contentMessageColor];

    
    if (textSize.height > ALERT_VIEW_LABEL_MAX_HEIGHT) {
        UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, separator.frame.origin.y + separator.frame.size.height + 5, ALERT_VIEW_WIDTH + 2 * ALERT_VIEW_LABEL_MARGIN_LEFT, ALERT_VIEW_LABEL_MAX_HEIGHT)];
        [scrollView setShowsHorizontalScrollIndicator:NO];
        [messageLabel setFrame:CGRectMake(ALERT_VIEW_LABEL_MARGIN_LEFT, 0, ALERT_VIEW_WIDTH, textSize.height)];
        [messageLabel setTextAlignment:NSTextAlignmentLeft];
        [scrollView addSubview:messageLabel];
        [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, textSize.height)];
        
        [containerView addSubview:scrollView];
        [containerView setFrame:CGRectMake(0, 0, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT + separator.frame.size.height + 5 + scrollView.frame.size.height + 10)];
    } else {
        [containerView addSubview:messageLabel];
        [containerView setFrame:CGRectMake(0, 0, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT + separator.frame.size.height + 5 + messageLabel.frame.size.height + 10)];
    }
    
    [containerView addSubview:titleLabel];
    [containerView addSubview:separator];
    //    [containerView addSubview:messageLabel];
    //    [containerView setFrame:CGRectMake(0, 0, ALERT_VIEW_LABEL_MARGIN_LEFT * 2 + ALERT_VIEW_WIDTH, ALERT_VIEW_TITLE_MARGIN_TOP + ALERT_VIEW_TITLE_HEIGHT + separator.frame.size.height + 5 + messageLabel.frame.size.height + 10)];
    
    [alertView setButtonTitles:buttonsArray];
    [alertView setContainerView:containerView];
    [alertView show];
}
@end
