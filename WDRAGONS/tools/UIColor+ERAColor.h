//
//  UIColor+ERAColor.h
//  UDNTicket
//
//  Created by Weiyu Chen on 2018/11/23.
//  Copyright (c) 2018年 Weiyu Chen All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ERAColor)

+ (UIColor *)messageAlertViewTitleColor;
+ (UIColor *)blueButtonBackgroundColor;
+ (UIColor *)backgroundGrayColor;
+ (UIColor *)moreAppCellTextColor;

@end
