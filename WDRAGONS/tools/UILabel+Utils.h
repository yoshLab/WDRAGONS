
#import <UIKit/UIKit.h>

@interface UILabel (Utils)
- (void)setText:(NSString*)text lineSpacing:(CGFloat)lineSpacing;
@end;
