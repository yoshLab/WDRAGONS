//
//  MICheckBox.m
//  eChecker
//
//  Created by 陳 威宇 on 13/2/6.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import "MICheckBox.h"

@implementation MICheckBox
@synthesize isChecked;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.contentHorizontalAlignment =
        UIControlContentHorizontalAlignmentLeft;
        
        [self setImage:[UIImage imageNamed:
                        @"BtnUncheck.png"]
              forState:UIControlStateNormal];
        
        [self addTarget:self action:
         @selector(checkBoxClicked)
       forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}


-(IBAction) checkBoxClicked{
    if(self.isChecked ==NO){
        self.isChecked =YES;
        [self setImage:[UIImage imageNamed:
                        @"BtnCheck.png"]
              forState:UIControlStateNormal];
        
    }else{
        self.isChecked =NO;
        [self setImage:[UIImage imageNamed:
                        @"BtnUncheck.png"]
              forState:UIControlStateNormal];
        
    }
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code

    if(self.isChecked ==NO){
        [self setImage:[UIImage imageNamed:
                        @"BtnUncheck.png"]
              forState:UIControlStateNormal];
    } else {
        [self setImage:[UIImage imageNamed:
                        @"BtnCheck.png"]
              forState:UIControlStateNormal];
    }

}


@end
