//
//  MICheckBox.h
//  eChecker
//
//  Created by 陳 威宇 on 13/2/6.
//  Copyright (c) 2013年 陳 威宇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MICheckBox : UIButton {

    BOOL isChecked;

}

@property (nonatomic,assign) BOOL isChecked;

-(IBAction) checkBoxClicked;

@end
