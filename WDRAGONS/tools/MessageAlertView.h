//
//  MessageAlertView.h
//  UDNTicket
//
//  Created by Luster NB024 on 2015/4/8.
//  Copyright (c) 2015年 Luster NB024. All rights reserved.
//

#import "CustomIOSAlertView.h"
#import "UIColor+ERAColor.h"
#import "ConstantData.h"

#define IPHONE_6_ALERT_VIEW_WIDTH 260.0f
#define ALERT_VIEW_WIDTH DEVICE_WIDTH * IPHONE_6_ALERT_VIEW_WIDTH / IPHONE_6_DEVICE_WIDTH
#define ALERT_VIEW_TITLE_MARGIN_TOP 5.0f
#define ALERT_VIEW_TITLE_HEIGHT 40.0f
#define ALERT_VIEW_LABEL_MARGIN_LEFT 12.0f
#define ALERT_VIEW_TITLE_FONT_SIZE 20.0f
#define ALERT_VIEW_MESSAGE_FONT_SIZE 16.0f
#define IPHONE_6_ALERT_VIEW_LABEL_MAX_HEIGHT 400.0f
#define ALERT_VIEW_LABEL_MAX_HEIGHT DEVICE_HEIGHT * IPHONE_6_ALERT_VIEW_LABEL_MAX_HEIGHT / IPHONE_6_DEVICE_HEIGHT

@interface MessageAlertView : NSObject

+ (MessageAlertView *)getInstance;
- (void)showAlertView:(CustomIOSAlertView *)alertView withTitleMessage:(NSString *)titleMessage buttonsTitleArray:(NSArray *)buttonsArray;
- (void)showAlertView:(CustomIOSAlertView *)alertView withTitleMessage:(NSString *)titleMessage andContentMessage:(NSString *)contentMessage buttonsTitleArray:(NSArray *)buttonsArray;

- (void)showAlertView:(CustomIOSAlertView *)alertView withTitleMessage:(NSString *)titleMessage andContentMessage:(NSString *)contentMessage andandContentMessage:(UIColor *)contentMessageColor buttonsTitleArray:(NSArray *)buttonsArray;

@end
