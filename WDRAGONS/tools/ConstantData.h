//
//  ConstantData.h
//  UDNTicket
//
//  Created by Luster NB024 on 2015/4/13.
//  Copyright (c) 2015年 Luster NB024. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TEST_SERVER_URL @"https://shadow2.utiki.com.tw/WDRAGONS_CORE/Services"
#define OFFICIAL_SERVER_URL @"https://tix.wdragons.com/Services"
#define TEST_HOME_PAGE_URL @"https://shadow2.utiki.com.tw/WDRAGONS_CORE/UTK0101_"
#define OFFICIAL_HOME_PAGE_URL @"https://tix.wdragons.com/UTK0101_"

#define TEST_SERVICES_URL @"https://shadow2.utiki.com.tw/WDRAGONS_CORE/UTK0112_"
#define OFFICIAL_SERVICES_URL @"https://tix.wdragons.com/UTK0112_"

#define TEST_NEWS_URL @"https://shadow2.utiki.com.tw/WDRAGONS_CORE/UTK0105_"
#define OFFICIAL_NEWS_URL @"https://tix.wdragons.com/UTK0105_"


#define TEST_REGISTER_URL @"https://shadow2.utiki.com.tw/WDRAGONS_CORE/UTK1301_"
#define OFFICIAL_REGISTER_URL @"https://tix.wdragons.com/UTK1301_"

#define TEST_FORGET_URL @"https://shadow2.utiki.com.tw/WDRAGONS_CORE/UTK1302_"
#define OFFICIAL_FORGET_URL @"https://tix.wdragons.com/UTK1302_"

#define IS_TEST YES
#define SERVER_URL IS_TEST ? TEST_SERVER_URL : OFFICIAL_SERVER_URL
#define HOME_PAGE_URL IS_TEST ? TEST_HOME_PAGE_URL : OFFICIAL_HOME_PAGE_URL
#define SERVICES_URL IS_TEST ? TEST_SERVICES_URL : OFFICIAL_SERVICES_URL
#define NEWS_URL IS_TEST ? TEST_NEWS_URL : OFFICIAL_NEWS_URL
#define REGISTER_URL IS_TEST ? TEST_REGISTER_URL : OFFICIAL_REGISTER_URL
#define FORGET_URL IS_TEST ? TEST_FORGET_URL : OFFICIAL_FORGET_URL
#define ORDER_URL IS_TEST ? TEST_ORDER_URL : OFFICIAL_ORDER_URL

#define SHOW_VERSION IS_TEST
#define TICKET_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
#define SYSTEM_VERSION [[UIDevice currentDevice] systemVersion]

#define DEVICE_WIDTH [UIScreen mainScreen].bounds.size.width
#define DEVICE_HEIGHT [UIScreen mainScreen].bounds.size.height
#define NAVI_BAR_HEIGHT 64.0f
#define TAB_BAR_HEIGHT 49.0f
#define KEYBOARD_HEIGHT 216.0f
#define STATUS_BAR_HEIGHT_X 44.0f
#define IPHONEX_HEIGHT 812
#define IPHONE5_WIDTH 320
#define STATUS_BAR_HEIGHT 20.0f

#define HUD_SIZE CGSizeMake(84, 84)

#define TICKETS_LIST_OPTIONS_ARRAY [NSArray arrayWithObjects:@"已取票券", @"未取票券", @"票券歷程", nil]

#define TITLE_BUTTON_WIDTH 35.0f //30.0f
#define TITLE_BUTTON_HEIGHT 40.0f //30.0f
#define TITLE_BUTTON_X 5.0f
#define TITLE_BUTTON_Y (46.0f - TITLE_BUTTON_HEIGHT) / 2
#define SAFE_AREA 34.0f
#define IPHONE_6_DEVICE_HEIGHT 667.0f
#define IPHONE_6_DEVICE_WIDTH 375.0f
#define IPHONE_6_LIST_CELL_HEIGHT 60.0f
#define LIST_TEXT_SIZE 21.0f
#define LIST_CELL_HEIGHT DEVICE_HEIGHT * IPHONE_6_LIST_CELL_HEIGHT / IPHONE_6_DEVICE_HEIGHT

#define BUTTON_BORDER_WIDTH 1.0f
#define BUTTON_TEXT_FONT [UIFont systemFontOfSize:20.0f]


#define AES_KEY @"FA3B0AFFFFFEFFFFFFFEAAEF0000FFFB"


#define CELL_SPACING    12
#define CELL_TITLE_FONT_SIZE 15
#define CELL_DATE_FONT_SIZE 12
#define CELL_TEXT_LINE_SPACE 2
#define TICKET_NUMBER_WIDTH 26
#define TICKET_NUMBER_HEIGHT 26
#define TICKET_NUMBER_FONT_SIZE 16
#define TICKET_PROGRAM_NAME_FONT_SIZE 20
#define TICKET_NORMAL_FONT_SIZE 12
#define TICKET_SPACING    12

#define BF_CODE @"19"
#define PLATFORM_CODE @"02"
#define UDID @"UDID"
#define ACCOUNT @"ACCOUNT"
#define PWD @"PWD"
#define TOKENID @"TOKENID"
#define ISLOGIN @"ISLOGIN"
#define NAME @"NAME"
#define AUTH_TYPE @"AUTH_TYPE"
#define USER_ID @"USER_ID"

#define TAB_2_ID  1
#define TAB_3_ID  2
#define TAB_4_ID  3
#define TAB_5_ID  4

@interface ConstantData : NSObject


@end
